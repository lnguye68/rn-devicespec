import os
import sys
import tkinter
import tkinter.filedialog
import tkinter.messagebox
import tkinter.simpledialog
import openpyxl
import json
import collections
import shutil
import re
import time

DEF_SUPPORT_EXCEL_FORMATS = ['.xlsx', '.xlsm']

DEF_TRANSLATION_SHEET_NAME = "Translation"
DEF_DEFAULT_SHEET_NAME = "English - Global"

DEF_KEY_START_COLUMN = 1
DEF_KEY_START_ROW = 3

DEF_LANGUAGE_START_COL = 2
DEF_LANGUAGE_NAME_ROW = 1
DEF_LANGUAGE_VALUE_ROW = 2

DEF_STRINGS_XML_FILE_NAME = "strings_excel.xml"
DEF_DIMENS_XML_FILE_NAME = "dimens_excel.xml"
DEF_COLORS_XML_FILE_NAME = "colors_excel.xml"

# Currently this is not used. But you can use it in the future.
DEF_INTEGERS_XML_FILE_NAME = "integers_excel.xml"

DEF_SUPPORTED_RES_FILE_LIST = [DEF_STRINGS_XML_FILE_NAME,
                               DEF_DIMENS_XML_FILE_NAME,
                               DEF_COLORS_XML_FILE_NAME
                               # , DEF_INTEGERS_XML_FILE_NAME,
                               ]

DEF_SUPPORT_LANGUAGE_LIST_FILE = os.path.join("xlsx2Xml_ConfigurationFiles", "support_lang_list.json")
DEF_SPECIAL_CHARACTER_JSON_FILE = os.path.join("xlsx2Xml_ConfigurationFiles", "change_special_chars.json")
DEF_CHANGE_FONT_NAME_FILE = os.path.join("xlsx2Xml_ConfigurationFiles", "change_special_font_names.json")
DEF_OUTPUT_FILE_USED_FONT_TYPES_FILE = os.path.join("xlsx2Xml_ConfigurationFiles",
                                                    "[OUTPUT] Font_Types_Used_In_Excel_File.txt")

CHANGE_DP_ENABLED = False
DEF_DEVICE_DPI_VALUE = 0
DENSITY_DEFAULT = 160

DIALOG_ENABLED = False


def is_none(data):
    return data is None


def is_string_type(data_object):
    # From Python 3.x,  str -> bytes and unicode -> str
    # return isinstance(data_object, str) or isinstance(data_object, unicode)
    if isinstance(data_object, bytes):
        print("******** byte -> data_object : ", data_object)
        return True
    if isinstance(data_object, str):
        # print("******** str -> data_object : ", data_object)
        return True

    return False

    # return isinstance(data_object, bytes) or isinstance(data_object, str)


def is_empty_str(in_string):
    return not (in_string and in_string.strip())


def is_not_alphanumeric_chars(in_string):
    # Using the regular expression. "\W" is the same as [^a-zA-Z0-9]
    if re.search("\W", in_string, re.MULTILINE):
        # print("This is not alphanumeric characters  : " + in_string)
        return True
    else:
        return False


def is_supported_excel_file(excel_file_name):
    input_filename, input_file_ext = os.path.splitext(excel_file_name)
    return input_file_ext in DEF_SUPPORT_EXCEL_FORMATS


def is_json_file(json_file_name):
    input_filename, input_file_ext = os.path.splitext(json_file_name)
    return input_file_ext == ".json"


def read_json_file_data(in_json_file):
    try:
        fileobject = open(in_json_file)
        try:
            read_json_data = json.load(fileobject, object_pairs_hook=collections.OrderedDict)
        finally:
            fileobject.close()
    except Exception as ex:
        print("Error", ex)
        if DIALOG_ENABLED:
            tkinter.messagebox.showerror("Error", "Cannot open json file : " + in_json_file)
        raise SystemExit("Cannot open json file : " + in_json_file)
    return read_json_data


def get_number_format(sheet, in_row, in_column):
    return sheet.cell(row=in_row, column=in_column).number_format


def find_word(src_string, find_string):
    if src_string.find(find_string) == -1:
        return False
    else:
        return True


def delete_files_in_directory(target_dir, delete_file_list):
    # print("List of files to delete : %s" %delete_file_list)
    try:
        dir_list = os.listdir(target_dir)
        if dir_list:
            for item in dir_list:
                item_name = os.path.join(target_dir, item)
                # If this is file and included in the list of files to delete, delete this file.
                if os.path.isfile(item_name):
                    if not delete_file_list:
                        os.remove(item_name)
                    elif item in delete_file_list:
                        os.remove(item_name)

                # If this is directory, perform a recursive call
                elif os.path.isdir(item_name):
                    # Recursive call
                    delete_files_in_directory(item_name, delete_file_list)
                    # If the directory is empty, delete the directory
                    if not os.listdir(item_name):
                        os.rmdir(item_name)
        else:
            os.rmdir(target_dir)

    except Exception as ex:
        print("[Warring] ", ex)
        return False

    return True

def sys_arg_bool_check(v):
    if v.lower() in  ("yes", "y", "true", "t", "1"):
        return True
    elif v.lower() in ("no", "n", "false", "f", "0"):
        return False
    else:
        raise SystemExit("Boolean value expected for dimesnion/device type")

################################################################################################################

# This is to measure the run time.
start_time = time.time()

root = tkinter.Tk()
root.withdraw()

if len(sys.argv) > 1:
    input_excel_file = sys.argv[1]
    if is_empty_str(input_excel_file):
        raise SystemExit("No excel file chosen. Aborting app")
    if not is_supported_excel_file(input_excel_file):
        raise SystemExit("Not supported excel file")

    input_result_path = sys.argv[2]
    if is_empty_str(input_result_path):
        raise SystemExit("No destination directory chosen. Aborting app")
	
    input_change_dp = sys.argv[3]
    if not is_empty_str(input_change_dp):
        if (sys_arg_bool_check(input_change_dp)):
            CHANGE_DP_ENABLED = True
            input_device_dpi = sys.argv[4]
            if not is_empty_str(input_device_dpi):
                if (input_device_dpi.isdigit()):
                    DEF_DEVICE_DPI_VALUE = input_device_dpi
                else: 
                    raise SystemExit("Invalid DPI value")
            else:
                raise SystemExit("No device dpi was selected")
        else: 
            CHANGE_DP_ENABLED = False
    else:
        CHANGE_DP_ENABLED = False



else:
    DIALOG_ENABLED = True
    input_excel_file = tkinter.filedialog.askopenfilename(title="Choose the target excel file")
    if is_empty_str(input_excel_file):
        tkinter.messagebox.showerror("Error", "No excel file chosen. Aborting app")
        raise SystemExit("No excel file chosen. Aborting app")
    if not is_supported_excel_file(input_excel_file):
        tkinter.messagebox.showerror("Error", "Not supported excel file")
        raise SystemExit("Not supported excel file")

    input_result_path = tkinter.filedialog.askdirectory(title="Specify the path of the project", mustexist=1)

    if is_empty_str(input_result_path):
        tkinter.messagebox.showerror("Error", "No destination directory chosen. Aborting app")
        raise SystemExit("No destination directory chosen. Aborting app")

    CHANGE_DP_ENABLED = tkinter.messagebox.askyesno("PX to DP", "Convert the dimensions to DP?")
    if (CHANGE_DP_ENABLED):
        deviceDPI = tkinter.simpledialog.askstring("", "What is the DPI value of the device?")
        if (deviceDPI.isdigit()):
            DEF_DEVICE_DPI_VALUE = deviceDPI
        else: 
            raise SystemExit("Invalid DPI value")



def main():
    keys = []
    keys_for_value = []
    font_types = []

    # Verify the support language list file (json)
    support_lang_json_data = read_json_file_data(DEF_SUPPORT_LANGUAGE_LIST_FILE)
    print("The number of languages in json file : %d" % (len(support_lang_json_data)))

    # Verify and read the json file for the special characters
    json_special_char_list = read_json_file_data(DEF_SPECIAL_CHARACTER_JSON_FILE).items()
    # print("Special character list :")
    # for item in json_special_char_list:
    #     print(item[0] + " -> " + item[1])

    # Verify and read json file for change font name
    json_change_font_name_list = read_json_file_data(DEF_CHANGE_FONT_NAME_FILE).items()
    # print("font name list to change :")
    # for item in json_change_font_name_list:
    #     print(item[0] + " -> " + item[1])

    try:
        # Verify the excel file
        workbook = openpyxl.load_workbook(input_excel_file, data_only=True)

        # Time to load Excel document
        print("The Excel document was loaded :  %s seconds" % (time.time() - start_time))

        # Find & Verify the translation sheet
        if len(workbook.worksheets) < 1:
            if DIALOG_ENABLED:
                tkinter.messagebox.showerror("Error", "There is no sheets. Please check again.")
            raise SystemExit("There is no sheets. Please check again.")

        default_language_sheet = None

        # [CHANGE] 10/02/2018
        # for sheet in workbook.worksheets:
        #     if sheet.title == DEF_TRANSLATION_SHEET_NAME :
        #         language_sheet = sheet
        #         break
        for sheet in workbook.worksheets:
            if sheet.title == DEF_DEFAULT_SHEET_NAME:
                default_language_sheet = sheet
                break

        # [CHANGE] 10/02/2018
        # if is_none(language_sheet):
        #     if DIALOG_ENABLED:
        #         tkinter.messagebox.showerror("Error", "Cannot find the Translation sheet. Please check again!")
        #     raise SystemExit("Cannot find the Translation sheet. Please check again!")
        if is_none(default_language_sheet):
            if DIALOG_ENABLED:
                tkinter.messagebox.showerror("Error", "Cannot find the English - Global sheet for a default. "
                                                      "Please check again!")
            raise SystemExit("Cannot find the Translation sheet. Please check again!")

        # print("Default Sheet name : %s, Max Row : %d, Max Col : %d" % (default_language_sheet.title,
        #                                                                default_language_sheet.max_row,
        #                                                                default_language_sheet.max_column))
        # print(" Look at Max Row for each language, so you can check any missing Rows at a glance")
        # for sheet in workbook.worksheets:
        #     print("Language Sheet name : %s, Max Row : %d" % (sheet.title, sheet.max_row))

    except Exception as ex:
        print("Exception", ex)
        if DIALOG_ENABLED:
            tkinter.messagebox.showerror("Error", "Cannot open excel file")
        raise SystemExit("Cannot open excel file")

    '''
    [[ START - Delete old Android resource xml files
    '''
    if delete_files_in_directory(os.path.join(input_result_path, "res"), DEF_SUPPORTED_RES_FILE_LIST):
        print("Deleted old resource files.")
    else:
        print("[Note] Failed to delete old resource files")
    '''
    //]] END
    '''

    '''
    Read sheets : For loop for searching all sheets
    [[ START
    '''
    counted_lang = 0
    default_set = {}
    compare_set = {}
    difference_set = {}

    # sheets = workbook.get_sheet_names() // This is to get the list of sheets from array
    for current_sheet in workbook.worksheets:
        ''' 
        Read cells and write cell data to xml file
        [[ START
        '''
        # This is to for unnecessary sheets, if we don't have remove below two lines
        if current_sheet.title == DEF_TRANSLATION_SHEET_NAME:
            continue

        if not find_word(current_sheet.title, "English - Global"):
            language_start_col = 3
        else:
            language_start_col = 2

        for column_num in range(language_start_col, current_sheet.max_column, 1):
            # print( "xxxx current_sheet_max_column : ", current_sheet.max_column)
            language_value = current_sheet.cell(row=DEF_LANGUAGE_VALUE_ROW, column=column_num).value

            if is_none(language_value):
                continue
            if not is_string_type(language_value):
                language_value = str(language_value)
            if is_empty_str(language_value):
                continue
            if not find_word(language_value, "Value"):
                continue

            # Read language cells and compare with the support language json data
            language = current_sheet.cell(row=DEF_LANGUAGE_NAME_ROW, column=column_num).value
            if is_none(language):
                if DIALOG_ENABLED:
                    tkinter.messagebox.showerror("Error",
                                                 "C:R=[" + str(column_num) + ":" + str(DEF_LANGUAGE_NAME_ROW) + "]\n"
                                                                                                                "This language name cell is empty")
                raise SystemExit("[C:R]=[" + str(column_num) + ":" + str(DEF_LANGUAGE_NAME_ROW) + "]\n"
                                                                                                  "This language name cell is empty")
                # continue
            if not is_string_type(language):
                language = str(language)
            if is_empty_str(language):
                continue

            # Make sure it's a supported language
            if language not in support_lang_json_data:
                if DIALOG_ENABLED:
                    tkinter.messagebox.showerror("Error", "This " + language + " language is not supported.\n"
                                                                               "Please check the supported language json file again ")
                raise SystemExit("This " + language + " language is not supported.\n"
                                                      "Please check the supported language json file again ")

            counted_lang += 1
            print("[%d : %s] values-%s" % (counted_lang, language, support_lang_json_data[language]))

            # Create a directory to store the generated xml files
            save_dir = os.path.join(input_result_path, "res", "values-" + support_lang_json_data[language])
            if not os.path.exists(save_dir):
                os.makedirs(save_dir)

            # strings_excel.xml
            xml_header = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
            xml_header += "<resources>\n"
            string_xml_file_path = os.path.join(save_dir, DEF_STRINGS_XML_FILE_NAME)
            string_xml_file = open(string_xml_file_path, 'w')
            string_xml_file.write(xml_header)
            string_item_count = 0

            # [Portrait] dimens_excel.xml
            dimens_xml_file_path = os.path.join(save_dir, DEF_DIMENS_XML_FILE_NAME)
            dimens_xml_file = open(dimens_xml_file_path, 'w')
            dimens_xml_file.write(xml_header)
            dimens_item_count = 0

            # colors_excel.xml
            colors_xml_file_path = os.path.join(save_dir, DEF_COLORS_XML_FILE_NAME)
            colors_xml_file = open(colors_xml_file_path, 'w')
            colors_xml_file.write(xml_header)
            colors_item_count = 0

            # [Landscape] Create landscape localization directory
            save_land_dir = os.path.join(input_result_path, "res",
                                         "values-" + support_lang_json_data[language] + "-land")
            if not os.path.exists(save_land_dir):
                os.makedirs(save_land_dir)

            # [Landscape] dimens_excel.xml
            land_dimens_xml_file_path = os.path.join(save_land_dir, DEF_DIMENS_XML_FILE_NAME)
            land_dimens_xml_file = open(land_dimens_xml_file_path, 'w')
            land_dimens_xml_file.write(xml_header)
            land_dimens_item_count = 0

            '''
            Read the key values and the strings for each language, and
            Write them into an xml file in the form of an android string resource
            [[ START
            '''
            for row_num in range(DEF_KEY_START_ROW, current_sheet.max_row + 1):
                # ----------- Read Key -----------------
                key = current_sheet.cell(row=row_num, column=DEF_KEY_START_COLUMN).value
                if is_none(key):
                    continue
                if not is_string_type(key):
                    key = str(key)
                if is_empty_str(key):
                    continue

                ##### [[ START : Read and process the String value.
                value = current_sheet.cell(row=row_num, column=column_num).value

                if is_none(value) or is_empty_str(value):
                    if is_not_alphanumeric_chars(key):
                        # This key value is a comment.
                        comment_str = "\n    <!-- " + key + " -->\n"
                        string_xml_file.write(comment_str)
                        dimens_xml_file.write(comment_str)
                        land_dimens_xml_file.write(comment_str)
                        colors_xml_file.write(comment_str)
                    continue
                else:
                    if is_not_alphanumeric_chars(key):
                        # This key value contains specific characters that are not supported.
                        if DIALOG_ENABLED:
                            tkinter.messagebox.showerror("Error", "This key value is not supported: Row["
                                                         + str(row_num) + "] '" + str(
                                key) + " in sheet :" + current_sheet.title + "'. Aborting app.")
                        raise SystemExit("This key value is not supported: Row[" + str(row_num) + "] '" + str(
                            key) + " in sheet :" + current_sheet.title + "'. Aborting app.")

                # Change to lower character
                key = key.lower()

                # Check duplicate keys_for_value
                if key in keys_for_value:
                    string_xml_file.write("ERROR")
                    if DIALOG_ENABLED:
                        tkinter.messagebox.showerror("Error", "Duplicated key value: Row["
                                                     + str(row_num) + "] '" + str(key) + "'. Aborting app.")
                    raise SystemExit(
                        "Duplicated key value: Row[" + str(row_num) + "] '" + str(key) + "'. Aborting app.")

                # Store the key value to check the duplication of the key
                keys_for_value.append(key)

                # Replace special character
                for item in json_special_char_list:
                    value = value.replace(item[0], item[1])

                value = value.encode('utf-8')
                # print ("Value : " + value)

                # ------ Write key and value in the string xml file --------
                string_item_count += 1
                # string_xml_file.write("    <string name=\"" + str(key) + "\">" + str(value) + '</string>' + "\n")
                string_xml_file.write(
                    "    <string name=\"" + str(key) + "\">" + str(value, 'utf-8') + '</string>' + "\n")
                ##### ]] END : Read and process the String value.

                ##### [[ START : Read and process the Font type
                rename_key = str(key + "_font")

                # Check duplicate keys
                if rename_key in keys:
                    string_xml_file.write("ERROR")
                    if DIALOG_ENABLED:
                        tkinter.messagebox.showerror("Error", "Duplicated key value: Row["
                                                     + str(row_num) + "] '" + str(
                            rename_key) + " in sheet :" + current_sheet.title + "'. Aborting app.")
                    raise SystemExit("Duplicated key value: Row[" + str(row_num) + "] '" + str(
                        rename_key) + " in sheet :" + current_sheet.title + "'. Aborting app.")

                # Store the key value to check the duplication of the key
                keys.append(rename_key)

                font_type_text = current_sheet.cell(row=2, column=column_num + 1).value
                if is_none(font_type_text):
                    print("font type text is null")
                    continue
                if not is_string_type(font_type_text):
                    font_type_text = str(font_type_text)
                if is_empty_str(font_type_text):
                    print("font type text is empty")
                    continue
                if (not find_word(font_type_text, "Font Type")):
                    print("[ERROR] Not find font type")
                    continue

                value = current_sheet.cell(row=row_num, column=column_num + 1).value
                if not is_none(value):
                    if not is_string_type(value):
                        value = str(value)
                    if not is_empty_str(value):
                        # Replace special character
                        value = value.replace(".ttf", "")
                        value = value.replace(" ", "")
                        value = value.replace("_v1.0", "")
                        value = "fonts/" + value + ".ttf"

                        for item in json_change_font_name_list:
                            value = value.replace(item[0], item[1])

                        for item in json_special_char_list:
                            value = value.replace(item[0], item[1])
                        value = value.encode('utf-8')

                        # ------ Write key and value in the string xml file --------
                        string_item_count += 1
                        # string_xml_file.write("    <string name=\"" + str(rename_key) + "\">" + str(value) + '</string>' + "\n")
                        string_xml_file.write(
                            "    <string name=\"" + str(rename_key) + "\">" + str(value, 'utf-8') + '</string>' + "\n")

                        if not (value in font_types):
                            font_types.append(value)
                ##### ]] END : Read and process the Font type

                ##### [[ START : Read and process the Dimension value (Portrait)
                rename_key = str(key + "_font_size")

                # Check duplicate keys
                if rename_key in keys:
                    string_xml_file.write("ERROR")
                    if DIALOG_ENABLED:
                        tkinter.messagebox.showerror("Error", "Duplicated key value: Row["
                                                     + str(row_num) + "] '" + str(
                            rename_key) + " in sheet :" + current_sheet.title + "'. Aborting app.")
                    raise SystemExit(
                        "Duplicated key value: Row[" + str(row_num) + "] '" + str(
                            rename_key) + " in sheet :" + current_sheet.title + "'. Aborting app.")

                # Store the key value to check the duplication of the key
                keys.append(rename_key)

                font_size_text = current_sheet.cell(row=2, column=column_num + 2).value
                if is_none(font_size_text):
                    continue
                if not is_string_type(font_size_text):
                    font_size_text = str(font_size_text)
                if is_empty_str(font_size_text):
                    continue
                if (not find_word(font_size_text, "Portrait")):
                    print("[ERROR] Not find font size (portrait)")
                    continue

                value = current_sheet.cell(row=row_num, column=column_num + 2).value
                if not is_none(value):
                    if not is_string_type(value):
                        value = str(value)
                    if not is_empty_str(value):
                        value = value.replace(" ", "")
                        value = re.findall('\d+', value)[0]

                        if not (CHANGE_DP_ENABLED):
                            value = value + "px"
                        else:
                            value = (float(value) / (float(DEF_DEVICE_DPI_VALUE) / float(DENSITY_DEFAULT)))
                            value = ('%.2f' % value).rstrip('0').rstrip('.')
                            value = value + "dp"

                        # Remove
                        # value = value.replace("pt", "px")
                        value = value.encode('utf-8')

                        dimens_item_count += 1
                        # dimens_xml_file.write("    <dimen name=\"" + str(rename_key) + "\">" + str(value) + '</dimen>' + "\n")
                        dimens_xml_file.write(
                            "    <dimen name=\"" + str(rename_key) + "\">" + str(value, 'utf-8') + '</dimen>' + "\n")
                ##### ]] END : Read and process the Dimension value (Portrait)

                ##### [[ START : Read and process the Dimension value (Landscape)
                land_font_size_text = current_sheet.cell(row=2, column=column_num + 3).value
                if is_none(land_font_size_text):
                    continue
                if not is_string_type(land_font_size_text):
                    land_font_size_text = str(land_font_size_text)
                if is_empty_str(land_font_size_text):
                    continue
                if (not find_word(land_font_size_text, "Landscape")):
                    print("[ERROR] Not find font size (landscape)")
                    continue

                value = current_sheet.cell(row=row_num, column=column_num + 3).value
                if not is_none(value):
                    if not is_string_type(value):
                        value = str(value)
                    if not is_empty_str(value):
                        # Replace special character
                        value = value.replace(" ", "")
                        value = re.findall('\d+', value)[0]
						
                        if not (CHANGE_DP_ENABLED):
                            value = value + "px"
                        else:
                            value = (float(value) / (float(DEF_DEVICE_DPI_VALUE) / float(DENSITY_DEFAULT)))
                            value = ('%.2f' % value).rstrip('0').rstrip('.')
                            value = value + "dp"
                       
                        # Remove
                        # value = value.replace("pt", "px")
                        value = value.encode('utf-8')

                        land_dimens_item_count += 1
                        # land_dimens_xml_file.write("    <dimen name=\"" + str(rename_key) + "\">" + str(value, 'utf-8') + '</dimen>' + "\n")
                        land_dimens_xml_file.write(
                            "    <dimen name=\"" + str(rename_key) + "\">" + str(value, 'utf-8') + '</dimen>' + "\n")
                ##### ]] END : Read and process the Dimension value (Landscape)

                ##### [[ START : Read and process the Color value (Portrait & Landscape)
                rename_key = str(key + "_font_color")

                # Check duplicate keys
                if rename_key in keys:
                    string_xml_file.write("ERROR")
                    if DIALOG_ENABLED:
                        tkinter.messagebox.showerror("Error", "Duplicated key value: Row["
                                                     + str(row_num) + "] '" + str(
                            rename_key) + " in sheet :" + current_sheet.title + "'. Aborting app.")
                    raise SystemExit(
                        "Duplicated key value: Row[" + str(row_num) + "] '" + str(
                            rename_key) + " in sheet :" + current_sheet.title + "'. Aborting app.")

                # Store the key value to check the duplication of the key
                keys.append(rename_key)

                font_color_text = current_sheet.cell(row=2, column=column_num + 4).value
                if is_none(font_color_text):
                    continue
                if not is_string_type(font_color_text):
                    font_color_text = str(font_color_text)
                if is_empty_str(font_color_text):
                    continue
                if (not find_word(font_color_text, "Font color")):
                    print("[ERROR] Not find font color")
                    continue

                value = current_sheet.cell(row=row_num, column=column_num + 4).value
                if not is_none(value):
                    if not is_string_type(value):
                        value = str(value)
                    if not is_empty_str(value):
                        # Replace special character
                        value = value.replace(" ", "")
                        # value = re.sub('^#\') ^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$
                        value = value.encode('utf-8')

                        colors_item_count += 1
                        # colors_xml_file.write("    <color name=\"" + str(rename_key) + "\">" + str(value) + '</color>' + "\n")
                        colors_xml_file.write(
                            "    <color name=\"" + str(rename_key) + "\">" + str(value, 'utf-8') + '</color>' + "\n")
                ##### ]] END : Read and process the Color value (Portrait & Landscape)

            '''
            ]] END of 2nd For statement
            '''

            # Find missing key values from multi languages
            if find_word(current_sheet.title, "English - Global"):
                default_set = set(keys_for_value)
            else:
                compare_set = set(keys_for_value)

            if len(compare_set) > 0:
                difference_set = default_set - compare_set
                if len(difference_set) == 0:
                    print("Sheet name : %s keys are same as default english " % current_sheet.title)
                elif len(difference_set) > 0:
                    print("Sheet name : %s has missing keys %s " % (current_sheet.title, difference_set))

            compare_set.clear()
            difference_set.clear()

            del keys[:]
            del keys_for_value[:]

            xml_end_resources_tag = "</resources>\n"
            string_xml_file.write(xml_end_resources_tag)
            string_xml_file.close()
            if (string_item_count == 0):
                os.remove(os.path.join(save_dir, DEF_STRINGS_XML_FILE_NAME))

            dimens_xml_file.write(xml_end_resources_tag)
            dimens_xml_file.close()
            if (dimens_item_count == 0):
                os.remove(os.path.join(save_dir, DEF_DIMENS_XML_FILE_NAME))

            land_dimens_xml_file.write(xml_end_resources_tag)
            land_dimens_xml_file.close()
            if (land_dimens_item_count == 0):
                os.remove(os.path.join(save_land_dir, DEF_DIMENS_XML_FILE_NAME))

            colors_xml_file.write(xml_end_resources_tag)
            colors_xml_file.close()
            if (colors_item_count == 0):
                os.remove(os.path.join(save_dir, DEF_COLORS_XML_FILE_NAME))



            '''
            [[ START : Copy default language xml to values folder.
                       The first language in the translation sheet is the default language.
            '''
            # counted_lang -> current_sheet
            if find_word(current_sheet.title, "English - Global"):
                # [Default Language Resource] Copy the resource file to the default language directory.
                default_lang_dir = os.path.join(input_result_path, "res", "values")
                if not os.path.exists(default_lang_dir):
                    os.makedirs(default_lang_dir)

                if os.path.isfile(os.path.join(save_dir, DEF_STRINGS_XML_FILE_NAME)):
                    shutil.copy(os.path.join(save_dir, DEF_STRINGS_XML_FILE_NAME), default_lang_dir)
                if os.path.isfile(os.path.join(save_dir, DEF_DIMENS_XML_FILE_NAME)):
                    shutil.copy(os.path.join(save_dir, DEF_DIMENS_XML_FILE_NAME), default_lang_dir)
                if os.path.isfile(os.path.join(save_dir, DEF_COLORS_XML_FILE_NAME)):
                    shutil.copy(os.path.join(save_dir, DEF_COLORS_XML_FILE_NAME), default_lang_dir)

                try:
                    if not os.listdir(default_lang_dir):
                        os.rmdir(default_lang_dir)
                except WindowsError as ex:
                    print("[Warring] ", ex)

                # [Default Landscape Language Resource] Copy the landscape resource file to the landscape default language directory.
                default_lang_land_dir = default_lang_dir + "-land"
                if not os.path.exists(default_lang_land_dir):
                    os.makedirs(default_lang_land_dir)

                if os.path.isfile(os.path.join(save_land_dir, DEF_STRINGS_XML_FILE_NAME)):
                    shutil.copy(os.path.join(save_land_dir, DEF_STRINGS_XML_FILE_NAME), default_lang_land_dir)
                if os.path.isfile(os.path.join(save_land_dir, DEF_DIMENS_XML_FILE_NAME)):
                    shutil.copy(os.path.join(save_land_dir, DEF_DIMENS_XML_FILE_NAME), default_lang_land_dir)
                if os.path.isfile(os.path.join(save_land_dir, DEF_COLORS_XML_FILE_NAME)):
                    shutil.copy(os.path.join(save_land_dir, DEF_COLORS_XML_FILE_NAME), default_lang_land_dir)

                try:
                    if not os.listdir(default_lang_land_dir):
                        os.rmdir(default_lang_land_dir)
                except WindowsError as ex:
                    print("[Warring] ", ex)

                print("[Default : " + language + "] values")

            try:
                if not os.listdir(save_dir):
                    os.rmdir(save_dir)
            except WindowsError as ex:
                print("[Warring] ", ex)

            try:
                if not os.listdir(save_land_dir):
                    os.rmdir(save_land_dir)
            except WindowsError as ex:
                print("[Warring] ", ex)
            '''
            ]] END of IF statement : Copy default language xml to values folder
            '''

            if len(font_types) > 0:
                if os.path.isfile(DEF_OUTPUT_FILE_USED_FONT_TYPES_FILE):
                    os.remove(DEF_OUTPUT_FILE_USED_FONT_TYPES_FILE)
                font_types_file = open(DEF_OUTPUT_FILE_USED_FONT_TYPES_FILE, 'w')
                for font_type_str in font_types:
                    font_types_file.write(str(font_type_str, "utf-8") + "\n")
                font_types_file.close()
        '''
        ]] END of 1st For statement
        '''
    '''
    ]] END of For loop for searching all sheets
    '''

    default_set.clear()

    if counted_lang != len(support_lang_json_data):
        if DIALOG_ENABLED:
            tkinter.messagebox.showerror("Error",
                                         "The number of languages supported by Json file and Excel file is different.\n"
                                         "Please check both files.\n"
                                         "Excel : " + str(counted_lang) + ", Json : " + str(
                                             len(support_lang_json_data)))
        raise SystemExit("The number of languages supported by Json file and Excel file is different.\n"
                         "Please check both files.\n"
                         "Excel : " + str(counted_lang) + ", Json : " + str(len(support_lang_json_data)))

    # Total processing time
    print("End of process : %s seconds" % (time.time() - start_time))

    if DIALOG_ENABLED:
        tkinter.messagebox.showinfo("Done", "Success")
    else:
        print("Success")


#######################################################################################################
main()
