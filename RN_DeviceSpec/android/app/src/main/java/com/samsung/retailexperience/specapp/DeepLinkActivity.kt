package com.samsung.retailexperience.specapp

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.nfc.NdefMessage
import android.nfc.NfcAdapter
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.samsung.retailexperience.specapp.utils.AppConst
import com.tecace.retail.res.util.ResUriUtil
import com.tecace.retail.util.PreferenceUtil
import org.koin.android.ext.android.inject

class DeepLinkActivity : Activity() {
    companion object {
        private val TAG = DeepLinkActivity::class.java.simpleName
    }

    private val mSharedPreference : PreferenceUtil by inject()

    private var mNfcAdapter: NfcAdapter? = null
    private var mDeepLinkSource : String? = null
    private var mDeepLinkDestination : String? = null
    private var mLaunchScreen : String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Check for available NFC Adapter
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this)
        if (mNfcAdapter == null) {
            Toast.makeText(
                this,
                "NFC is off",
                Toast.LENGTH_LONG).show()
        }
        when {
            NfcAdapter.ACTION_NDEF_DISCOVERED == intent.action -> {
                nfcReader(intent)
            }
            Intent.ACTION_VIEW == intent.action -> {
                externalCallerReader(intent)
            }
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        // Check for available NFC Adapter
        when {
            NfcAdapter.ACTION_NDEF_DISCOVERED == intent?.action -> {
                nfcReader(intent)
            }
            Intent.ACTION_VIEW == intent?.action -> {
                externalCallerReader(intent)
            }
        }
    }

    /**
     * Parses the NDEF Message from the intent and send it to React Native module
     * Device Spec / Pop-up / Device Full Spec / Brand List / Device List / Device Compare
     */
    private fun nfcReader(intent: Intent) {
        val rawMsgs = intent.getParcelableArrayExtra(
            NfcAdapter.EXTRA_NDEF_MESSAGES)
        // only one message sent during the beam
        if (rawMsgs != null && rawMsgs.isNotEmpty()) {
            val msg = rawMsgs[0] as NdefMessage
            Log.d(TAG, "@#@#@# message = $msg")
            val url = String(msg.records[0].payload)
            Log.d(TAG, "@#@#@# url = $url")

            val uri = Uri.parse(url)
            val query = ResUriUtil.getInstance().getQuery(uri)
            val map = ResUriUtil.getInstance().getQueryMap(query)

            val externalCaller = getExternalCaller(uri)
            extractDeepLinkArguments(map)
        }
    }

    private fun externalCallerReader(intent: Intent?) {
        if (intent == null) return

        val uri = intent.data
        val query = ResUriUtil.getInstance().getQuery(uri)
        val map = ResUriUtil.getInstance().getQueryMap(query)

        var externalCaller = getExternalCaller(uri)
        if (externalCaller == null) {
            externalCaller = "UNKNOWN"
        }

        extractDeepLinkArguments(map)
    }

    private fun getExternalCaller(uri: Uri?): String? {
        return uri?.getQueryParameter("s")
    }

    private fun extractDeepLinkArguments(map: Map<String, String>?) {
        if (map == null || map.isEmpty()) return

        val keys = map.keys

        for (key in keys) {
            if (key.equals("s", ignoreCase = true)) {
                mDeepLinkSource = map[key].toString()
                Log.d(TAG, "@#@#@#@ source : " + map[key])
            }
            if (key.equals("d", ignoreCase = true)) {
                mDeepLinkDestination = map[key].toString()
                Log.d(TAG, "@#@#@#@ dest : " + map[key])
            }
        }

        sendNfcEvent()
    }

    /**
     * Send events to JavaScript module in order to navigate to certain screen
     */
    private fun sendNfcEvent() {
        mDeepLinkDestination?.let {
            mLaunchScreen = when (it) {
                "ps" -> "NATIVE_Popular Specifications"
                "cds" -> "choose_device_screen"
                "fs" -> "full_spec_screen"
                "dc" -> "Device Comparison"
                // Undefined screen name. Go to default screen - Device Spec first page
                else -> "NATIVE_Popular Specifications"
            }
        }

        mSharedPreference.setString(this, AppConst.LAUNCH_SCREEN, mLaunchScreen)

        val intent = Intent(this@DeepLinkActivity, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra(AppConst.APP_LAUNCH_TYPE, mDeepLinkSource)
        intent.putExtra(AppConst.LAUNCH_SCREEN, mLaunchScreen)
        startActivity(intent)
        overridePendingTransition(0,0)
    }
}