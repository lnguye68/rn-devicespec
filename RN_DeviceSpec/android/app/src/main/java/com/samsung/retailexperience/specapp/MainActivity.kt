package com.samsung.retailexperience.specapp

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.View
import com.facebook.react.ReactActivity
import com.samsung.retailexperience.specapp.analytics.AppAnalyticsManager
import com.samsung.retailexperience.specapp.nativemodules.analytics.AnalyticsModule
import com.samsung.retailexperience.specapp.utils.AppConst
import com.tecace.retail.util.HomeKeyWatcher
import org.koin.android.ext.android.inject


class MainActivity : ReactActivity(), HomeKeyWatcher.OnHomePressedListener {

    companion object {
        private val TAG = MainActivity::class.java.simpleName
    }

    private val mAppAnalyticsManager: AppAnalyticsManager by inject()
    private val mHomeKeyWatcher: HomeKeyWatcher by inject()

    /**
     * Returns the name of the main component registered from JavaScript. This is used to schedule
     * rendering of the component.
     */
    override fun getMainComponentName(): String? {
        return "RN_DeviceSpec"
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) {
            hideSystemUI()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        extractIntentExtras(intent)
    }

    private fun extractIntentExtras(intent: Intent?) {
        var appLaunchType : String? = null
        var nextScreenName : String? = null

        intent?.let {
            appLaunchType = it.getStringExtra(AppConst.APP_LAUNCH_TYPE)
            nextScreenName = it.getStringExtra(AppConst.LAUNCH_SCREEN)
        }

        if (appLaunchType.isNullOrEmpty())
            appLaunchType = AnalyticsModule.START_APP
        if (nextScreenName.isNullOrEmpty())
            nextScreenName = "NATIVE_Popular Specifications"

        mAppAnalyticsManager.screenChangeEvent(
                this,
                causeType = appLaunchType!!,
                currentScreen = "NATIVE",
                toScreen = nextScreenName!!
        )
        //Set up a HOME key watcher so that the app can report screen change when user taps the HOME key
        mHomeKeyWatcher.setOnHomePressedListener(this)
    }

    override fun onResume() {
        super.onResume()
        val newConfig = resources.configuration
        sendConfigurationChangedBroadcast(newConfig)
        mHomeKeyWatcher.startWatch()
    }

    override fun onPause() {
        super.onPause()
        mHomeKeyWatcher.stopWatch()
    }

    override fun onDestroy() {
        super.onDestroy()
        mHomeKeyWatcher.setOnHomePressedListener(null)
    }

    override fun onHomePressed() {
        //Report a screen change event to NATIVE when user taps the HOME key on the phone.
        mAppAnalyticsManager.getCurrentScreen()?.let {
            mAppAnalyticsManager.screenChangeEvent(
                    this, AnalyticsModule.HOME_KEY_PRESS,
                    currentScreen = it,
                    toScreen = "NATIVE")
            mAppAnalyticsManager.setCurrentScreen(it)
        }
    }

    override fun onHomeLongPressed() {
        Log.d(TAG, "##### onHomeLongPressed()")
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        mAppAnalyticsManager.userOrientationEvent(this)
        sendConfigurationChangedBroadcast(newConfig)
    }

    private fun sendConfigurationChangedBroadcast(newConfig: Configuration) {
        val intent = Intent("onConfigurationChanged")
        intent.putExtra("newConfig", newConfig)
        this.sendBroadcast(intent)
    }

    private fun hideSystemUI() {
        val decorView = window.decorView
        decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_LOW_PROFILE
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)
    }
}