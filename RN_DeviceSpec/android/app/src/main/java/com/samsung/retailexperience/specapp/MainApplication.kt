package com.samsung.retailexperience.specapp

import android.app.Application
import android.content.Context
import com.BV.LinearGradient.LinearGradientPackage
import com.cmcewen.blurview.BlurViewPackage
import com.facebook.react.*
import com.facebook.soloader.SoLoader
import com.samsung.retailexperience.specapp.analytics.AppAnalyticsManager
import com.samsung.retailexperience.specapp.di.koinAppModule
import com.samsung.retailexperience.specapp.nativemodules.analytics.AnalyticsPackage
import com.samsung.retailexperience.specapp.nativemodules.display.ScreenPackage
import com.samsung.retailexperience.specapp.nativemodules.files.FilePackage
import com.samsung.retailexperience.specapp.nativemodules.json.JsonPackage
import com.samsung.retailexperience.specapp.nativemodules.localize.LocalizePackage
import com.samsung.retailexperience.specapp.nativemodules.nfc.NfcPackage
import com.samsung.retailexperience.specapp.nativemodules.strings.StringPackage
import com.samsung.retailexperience.specapp.views.choosedevicepager.ChooseDevicePagerPackage
import com.samsung.retailexperience.specapp.views.popularfeaturespager.PopularFeaturesPagerPackage
import com.como.RNTShadowView.ShadowViewPackage
import com.samsung.retailexperience.specapp.views.devicecomparisonview.DeviceComparisonViewPackage
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import java.io.File
import java.lang.reflect.InvocationTargetException

class MainApplication : Application(), ReactApplication {

    private val mAppAnalyticsManager: AppAnalyticsManager by inject()

    private val mReactNativeHost: ReactNativeHost = object : ReactNativeHost(this) {
        override fun getUseDeveloperSupport(): Boolean {
            return BuildConfig.DEBUG
        }

        override fun getPackages(): List<ReactPackage> {
            val packages: MutableList<ReactPackage> = PackageList(this).packages
            // Packages that cannot be autolinked yet can be added manually here, for example:
            packages.add(FilePackage())
            packages.add(StringPackage())
            packages.add(JsonPackage())
            packages.add(ScreenPackage())
            packages.add(AnalyticsPackage())
            packages.add(LocalizePackage())
            packages.add(NfcPackage())
            packages.add(PopularFeaturesPagerPackage())
            packages.add(ChooseDevicePagerPackage())
            packages.add(ShadowViewPackage())
            packages.add(DeviceComparisonViewPackage())
            packages.add(LinearGradientPackage())
            return packages
        }

        override fun getJSMainModuleName(): String {
            return "index"
        }
    }

    override fun getReactNativeHost(): ReactNativeHost {
        return mReactNativeHost
    }

    override fun onCreate() {
        super.onCreate()
        initAppDirectory()

        startKoin{
            androidLogger()
            androidContext(this@MainApplication)
            modules(koinAppModule)
        }

        mAppAnalyticsManager.initAppAnalytics(this)
        mAppAnalyticsManager.initAnalyticsInfo(this)
        mAppAnalyticsManager.setAnalyticsInfo(this)

        SoLoader.init(this,  /* native exopackage */false)
        initializeFlipper(this, reactNativeHost.reactInstanceManager)
    }

    companion object {
        /**
         * Loads Flipper in React Native templates. Call this in the onCreate method with something like
         * initializeFlipper(this, getReactNativeHost().getReactInstanceManager());
         *
         * @param context
         * @param reactInstanceManager
         */
        private fun initializeFlipper(
                context: Context, reactInstanceManager: ReactInstanceManager) {
            if (BuildConfig.DEBUG) {
                try {
                    /*
         We use reflection here to pick up the class that initializes Flipper,
        since Flipper library is not available in release mode
        */
                    val aClass = Class.forName("com.samsung.retailexperience.samsung.ReactNativeFlipper")
                    aClass
                            .getMethod("initializeFlipper", Context::class.java, ReactInstanceManager::class.java)
                            .invoke(null, context, reactInstanceManager)
                } catch (e: ClassNotFoundException) {
                    e.printStackTrace()
                } catch (e: NoSuchMethodException) {
                    e.printStackTrace()
                } catch (e: IllegalAccessException) {
                    e.printStackTrace()
                } catch (e: InvocationTargetException) {
                    e.printStackTrace()
                }
            }
        }
    }

    /**
     * Initialize app's directory if it doesn't exist yet.
     */
    private fun initAppDirectory() {
        val extDir = this.getExternalFilesDir(null)?.absolutePath
        extDir?.let {
            val dir = File(it)
            if (!dir.exists()) {
                dir.mkdir()
            }
        }
    }
}
