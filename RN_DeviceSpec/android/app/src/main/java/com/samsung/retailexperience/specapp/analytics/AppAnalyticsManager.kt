package com.samsung.retailexperience.specapp.analytics

import android.app.Application
import android.content.Context

interface AppAnalyticsManager {

    companion object {
        private val TAG = AppAnalyticsManager::class.java.simpleName
    }

    fun setAnalyticsRunning(shouldRun: Boolean)

    fun isAnalyticsRunning(): Boolean

    /**
     * Initialize the analytics by setting up id,
     * configuration file, subsidiary, basic attributes, and ignored screens.
     *
     * @param app The current application
     */
    fun initAppAnalytics(app: Application)

    fun setAnalyticsInfo(ctx: Context)

    fun initAnalyticsInfo(ctx: Context)

    fun getDeviceID(app: Application): String?

    fun setCurrentScreen(currentScreen: String?)

    fun getCurrentScreen(): String?

    fun checkSubsidiary(ctx: Context)

    fun getSubsidiary(ctx: Context): String?

    fun deviceId(context: Context): String?

    fun setDeviceId(context: Context, deviceId: String?)

    fun setSessSeqNum()

    fun getChannel(context: Context): String?

    fun getChannelId(context: Context): String?

    fun clear()

    fun screenChangeEvent(context: Context, causeType: String, currentScreen: String, toScreen: String)

    fun userEvent(context: Context, userAction: String, extra: String? = null)

    fun userOrientationEvent(context: Context)
}