package com.samsung.retailexperience.specapp.analytics

import android.app.Application
import android.content.Context
import android.content.Intent
import android.nfc.NfcAdapter
import android.os.Handler
import android.os.Message
import android.provider.Settings
import android.util.Log
import com.samsung.retailexperience.specapp.BuildConfig
import com.samsung.retailexperience.specapp.R
import com.samsung.retailexperience.specapp.utils.AppConst
import com.tecace.retail.analytics.AnalyticsManager
import com.tecace.retail.analytics.RetailAnalyticsConst
import com.tecace.retail.res.Res
import com.tecace.retail.res.ResObserver
import com.tecace.retail.res.util.ResFileUtil
import com.tecace.retail.res.util.config.EnvironmentManager
import com.tecace.retail.util.Consts
import com.tecace.retail.util.PreferenceUtil
import com.tecace.retail.util.analytics.FragmentActionType
import rm_devicespec.util.Util
import rm_devicespec.util.UtilImpl
import java.util.*

/**
 * A singleton implementation for the [AnalyticsManager]
 */
class AppAnalyticsManagerImpl private constructor(): AppAnalyticsManager {

    companion object {
        private val TAG = AppAnalyticsManagerImpl::class.java.simpleName
        //        private const val MSG_CHECK_CONNECTION = 1
        private const val MSG_GET_SUBSIDIARY = 2
        private const val MSG_GET_ANALYTICS_INFO = 3
//        private const val CHECK_CONNECTION_DELAY_TIME = 3000L
        private const val ANALYTICS_MSG_DELAY = 5000L
        internal var DEBUG_SUBSIDIARY = "default"
        internal var DEBUG_CHANNEL = "default"

        @Volatile private var instance: AppAnalyticsManagerImpl? = null

        fun getInstance(): AppAnalyticsManager {
            return instance?: synchronized(this) {
               AppAnalyticsManagerImpl().also {
                   instance = it
               }
            }
        }
    }

    //region private fields

    private val mAnalyticsManager: AnalyticsManager = AnalyticsManager.getInstance()

    private lateinit var mAnalyticsHandler: Handler

    private var mUtil: Util = UtilImpl.getInstance()

    private var currentRotation = -1

    private var isEnabled = true
    //endregion


    //region super functions

    override fun setAnalyticsRunning(shouldRun: Boolean) {
        isEnabled = shouldRun
    }

    override fun isAnalyticsRunning(): Boolean {
        return isEnabled
    }

    override fun initAppAnalytics(app: Application) {
        var amazonAppId = app.getString(R.string.amazon_app_id)
        var amazonFileName = app.getString(R.string.amazon_file_name)

        mAnalyticsManager.setAppInfo(app, amazonAppId, amazonFileName)
        //This app should not let the app lifecycle helper controls the session sequence number
        //because it is a widget-based app. Set the second parameter to false will make the
        //session sequence number unchanged whenever the app enters foreground.
        //The session sequence number should be set by widget provider.
        mAnalyticsManager.createAnalyticsLifeCycleHelper(app, true)

        val subsidiary = mAnalyticsManager.subsidiary(app)
        PreferenceUtil.getInstance().setString(app, AppConst.CURRENT_SUBSIDIARY, subsidiary)

        if (BuildConfig.VZW_HUBBLE) {

            val vzwDeviceId = getDeviceID(app)

            mAnalyticsManager.region(app, "North America")
                .regionId(app, null)
                .subsidiary(app, "SEA")
                .subsidiaryId(app, null)
                .channel(app, null)
                .channelId(app, null)
                .subChannel(app, null)
                .subChannelId(app, null)
                .storeId(app, null)
                .deviceId(app, vzwDeviceId)
                .carrier(app, "VZW")
                .testMode(app, null)
                .GBM(app, null)
                .deviceType(app, "RDU")
                .shopperId(app, null)
                .country(app, null)
                .registered(app, "VZW")
        } else {
            mAnalyticsManager.region(app, null)
                .regionId(app, null)
                .subsidiary(app, null)
                .subsidiaryId(app, null)
                .channel(app, null)
                .channelId(app, null)
                .subChannel(app, null)
                .subChannelId(app, null)
                .storeId(app, null)
                .deviceId(app, null)
                .carrier(app, null)
                .testMode(app, null)
                .GBM(app, null)
                .deviceType(app, null)
                .shopperId(app, null)
                .country(app, null)
        }

        val ignoreSessionFragList = hashSetOf(RetailAnalyticsConst.NATIVE_SCREEN)
        mAnalyticsManager.setIgnoreScreens(ignoreSessionFragList as Set<String>?)

        val doNotReportFragList = hashSetOf(RetailAnalyticsConst.NATIVE_SCREEN)
        mAnalyticsManager.setDoNotReportScreens(doNotReportFragList as Set<String>?)

//        //check server connection
//        sendCheckConnection(app, serverConnectionHandler, CHECK_CONNECTION_DELAY_TIME)
    }

    /**
     *
     * @param ctx the application context
     */
    override fun initAnalyticsInfo(ctx: Context) {
        Log.d(TAG, "##### Initializing analytics info...")
        mAnalyticsHandler = AnalyticsHandler(this, ctx)
        if (BuildConfig.DEBUG) {
            setSubsidiary(ctx, DEBUG_SUBSIDIARY)
            setChannel(ctx, DEBUG_CHANNEL)
        } else {
            //Set the subsidiary parameter to null so that the analytics manager will try to obtain it from
            //the host app.
            setSubsidiary(ctx, null)
            setChannel(ctx, null)
        }
        //For debug mode, use this line and comment out the if-else statement above.
//        setSubsidiary(ctx, null)
//        setChannel(ctx, null)
    }


    /**
     * @param ctx The activity context
     */
    override fun setAnalyticsInfo(ctx: Context) {
        Log.d(TAG, "##### setAnalyticsInfo()")
        val subsidiary = mAnalyticsManager.subsidiary(ctx)

        if (subsidiary == null || subsidiary.isEmpty()) {
            mAnalyticsHandler.sendEmptyMessageDelayed(MSG_GET_ANALYTICS_INFO, ANALYTICS_MSG_DELAY)
            return
        }
        mAnalyticsManager.sendAnalyticsQueue()
    }


    /**
     * Get device ID for versions of the app that don't have Host App.
     */
    override fun getDeviceID(app: Application): String? {
//        if (BuildConfig.VZW_HUBBLE) {
//            var vzwDeviceId: String? = null
//        if (ActivityCompat.checkSelfPermission(app, android.Manifest.permission.READ_PHONE_STATE)
//            == PackageManager.PERMISSION_GRANTED) {
//            val storedID = PreferenceUtil.getInstance().getString(app, KEY_DEVICE_ANALYTIC_ID)
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                //Only way for Android Oreo and above. Device ID is restricted. See privacy changes
//                //in Android 10 changelog for details.
//                if (storedID.isNullOrEmpty()) {
//                    val id = Settings.Secure.getString(app.contentResolver, Settings.Secure.ANDROID_ID)
//                    vzwDeviceId = "VZW_$id"
//                    PreferenceUtil.getInstance().setString(app, KEY_DEVICE_ANALYTIC_ID, id)
//                } else {
//                    vzwDeviceId = "VZW_$storedID"
//                }
//            } else {
//                val telephonyManager = app.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
//                if (telephonyManager.deviceId != null) {
//                    vzwDeviceId = "VZW_" + telephonyManager.deviceId
//                } else if (Build.getSerial() != null) {
//                    vzwDeviceId = "VZW_" + Build.getSerial()
//                } else {
//                    vzwDeviceId = "VZW_$storedID"
//                }
//            }
//        }
        return null
    }

    override fun setCurrentScreen(currentScreen: String?) {
        mAnalyticsManager.screenName = currentScreen
    }

    override fun getCurrentScreen(): String? {
        return mAnalyticsManager.screenName
    }

    override fun getSubsidiary(ctx: Context): String? {
        return mAnalyticsManager.subsidiary(ctx)
    }

    /**
     * Update app environemnt and content paths if subsidiary changes.
     *
     * @see AppAnalyticsManager.checkSubsidiary
     */
    override fun checkSubsidiary(ctx: Context) {
        Log.d(TAG, "LONG: checkSubsidiary+++")
        try {
            var currentSubsidiary = PreferenceUtil.getInstance().getString(ctx, AppConst.CURRENT_SUBSIDIARY)
            if (currentSubsidiary == null) {
                currentSubsidiary = "null"
            }
            val newSubsidiary = Settings.Global.getString(ctx.contentResolver, "SRM_subsidiary")
            Log.d(TAG, "LONG: currentSubsidiary = $currentSubsidiary")
            Log.d(TAG, "LONG: newSubsidiary = $newSubsidiary")
            if (!currentSubsidiary.equals(newSubsidiary, ignoreCase = true)) {
                EnvironmentManager.getInstance().updateAppEnvironment(ctx)
                Res.setLanguage(ctx, Locale.getDefault().language, Locale.getDefault().country)
                ResObserver.getInstance()
                    .notifyChangedLanguageSetting(ResFileUtil.getInstance().getExternalFilesDir(ctx))
                PreferenceUtil.getInstance().setString(ctx, AppConst.CURRENT_SUBSIDIARY, newSubsidiary)
                mUtil.updateAppWidgetProvider(ctx, Consts.EXTRA_VALUE_CHANGED_LOCALE)
                initAnalyticsInfo(ctx.applicationContext)
                Log.d(TAG, "LONG: change language")
            }
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
        Log.d(TAG, "LONG: checkSubsidiary---")
    }

    override fun deviceId(context: Context): String? = mAnalyticsManager.deviceId(context)

    override fun setDeviceId(context: Context, deviceId: String?) {
        mAnalyticsManager.deviceId(context, deviceId)
    }

    override fun setSessSeqNum() {
        mAnalyticsManager.setSessSeqNum()
    }

    override fun getChannel(context: Context): String? = mAnalyticsManager.channel(context)

    override fun getChannelId(context: Context): String? = mAnalyticsManager.channelId(context)


    override fun clear() {
        mAnalyticsHandler.removeCallbacksAndMessages(null)
    }

    override fun screenChangeEvent(context: Context, causeType: String, currentScreen: String, toScreen: String)  {
        if (isEnabled) {
            mAnalyticsManager.screenName = currentScreen
            return mAnalyticsManager.notifyScreenChanged(causeType, toScreen)
        }
    }

    override fun userEvent(context: Context, userAction: String, extra: String?)  {
        if (isEnabled) {
            return mAnalyticsManager.notifyUserEvent(userAction, extra)
        }
    }

    override fun userOrientationEvent(context: Context) {
        if (isEnabled) {
            mAnalyticsManager.notifyOrientationUserEvent(
                FragmentActionType.DEVICE_ORIENTATION.actionType,
                mAnalyticsManager.screenName,
                mAnalyticsManager.getScreenOrientation(context)
            )
        }
    }

    //endregion

    //region analytics handler
    /**
     * @param appAnalyticsManager The app analytics manager API
     * @param ctx The application context
     */
    private class AnalyticsHandler(
        private val appAnalyticsManager: AppAnalyticsManager,
        private val ctx: Context
    ): Handler() {

        init {
            Log.d(TAG, "##### Initializing analytics handler for subsidiary...")
        }
        private var checkSubsidiaryCount = 0
        private var checkAnalyticsInfoCount = 0

        override fun handleMessage(msg: Message?) {
            when (msg?.what) {
                MSG_GET_SUBSIDIARY -> {
                    val subsidiary = appAnalyticsManager.getSubsidiary(ctx)
                    if (subsidiary == null || subsidiary.isEmpty()) {
                        if (++checkSubsidiaryCount < 5)
                            this.sendMessageDelayed(
                                this.obtainMessage(
                                    MSG_GET_SUBSIDIARY,
                                    msg.obj
                                ), 2000
                            )
                        else
                            checkSubsidiaryCount = 0
                    } else {
                        checkSubsidiaryCount = 0
                    }
                }

                MSG_GET_ANALYTICS_INFO -> {
                    val subsidiary = appAnalyticsManager.getSubsidiary(ctx)
                    if (subsidiary == null || subsidiary.isEmpty()) {
                        if (++checkAnalyticsInfoCount < 5)
                            this.sendEmptyMessageDelayed(MSG_GET_ANALYTICS_INFO, 2000)
                        else
                            checkAnalyticsInfoCount = 0
                    } else {
                        checkAnalyticsInfoCount = 0
                        appAnalyticsManager.setAnalyticsInfo(ctx)
                    }
                }
            }
        }
    }
    //endregion


    //region helper functions
    /**
     * Check if the activity is started via NFC tagging.
     */
    private fun checkIfFromNFC(ctx: Context, intent: Intent?, nameOfDestinationScreen: String?): Int {
        if (intent?.action == NfcAdapter.ACTION_NDEF_DISCOVERED) {
            var toScreenString = "Main menu"
            nameOfDestinationScreen?.let {
                toScreenString += "_" + Res.getString(ctx, nameOfDestinationScreen)
            }
            screenChangeEvent(ctx, "NFC", mAnalyticsManager.screenName, toScreen = "$toScreenString USP")
            return 1
        } else if (intent?.action == Intent.ACTION_VIEW) {
            val uri = intent.data
            val externalCaller = uri?.getQueryParameter("s")
            if (externalCaller == "NFC") {
                var toScreenString = "Main menu"
                nameOfDestinationScreen?.let {
                    toScreenString += "_" + Res.getString(ctx, nameOfDestinationScreen)
                }
                screenChangeEvent(ctx, "NFC", mAnalyticsManager.screenName, toScreen = "$toScreenString USP")
            }
            return 1
        }
        return 0
    }

    /**
     * Check if the activity is started via Bluetooth tagging.
     */
    private fun checkIfFromBluetooth(ctx: Context, intent: Intent?, nameOfDestinationScreen: String?): Int {
        if (intent?.action == Intent.ACTION_VIEW) {
            val uri = intent.data
            val externalCaller = uri?.getQueryParameter("s")
            if (externalCaller == "BT") {
                var toScreenString = "Main menu"
                nameOfDestinationScreen?.let {
                    toScreenString += "_" + Res.getString(ctx, nameOfDestinationScreen)
                }
                screenChangeEvent(ctx, "bluetooth", mAnalyticsManager.screenName, toScreen = "$toScreenString USP")
            return 1
            }
        }
        return 0
    }

    /**
     * Check if the activity is started via Bixby tagging.
     */
    private fun checkIfFromBixby(ctx: Context, intent: Intent?, nameOfDestinationScreen: String?): Int {
        if (intent?.action == Intent.ACTION_VIEW) {
            val uri = intent.data
            val externalCaller = uri?.getQueryParameter("s")
            if (externalCaller == "Bixby") {
                var toScreenString = "Main menu"
                nameOfDestinationScreen?.let {
                    toScreenString += "_" + Res.getString(ctx, nameOfDestinationScreen)
                }
                screenChangeEvent(ctx, "bixby", mAnalyticsManager.screenName, toScreen = "$toScreenString USP")
            }
            return 1
        }
        return 0
    }

    /**
     *
     * @param ctx The application context
     */
    private fun setSubsidiary(ctx: Context, subsidiary: String?) {
        Log.d(TAG, "##### setSubsidiary()")
        //for debug mode
        if (subsidiary != null && subsidiary.isNotEmpty()) {
            Log.d(TAG, "##### debug subsidiary: $subsidiary")
            mAnalyticsManager.setPrefSubsidiary(ctx, subsidiary.toLowerCase())
            mAnalyticsManager.subsidiary(ctx, subsidiary)
            return
        }
        val currentSubsidiary = mAnalyticsManager.subsidiary(ctx)
        if (currentSubsidiary == null || currentSubsidiary.isEmpty()) {
            Log.d(TAG, "##### currentSubsidiary: $currentSubsidiary")
            //check preference subsidiary value
            val prefSubsidiary = mAnalyticsManager.getPrefSubsidiary(ctx)
            if (prefSubsidiary == null || prefSubsidiary.isEmpty()) {
                mAnalyticsManager.setPrefSubsidiary(ctx, DEBUG_SUBSIDIARY)
            }

            //get the subsidiary value from Samsung Analytics
            mAnalyticsHandler.sendMessageDelayed(
                mAnalyticsHandler.obtainMessage(MSG_GET_SUBSIDIARY, prefSubsidiary),
                5000
            )
        } else {
            mAnalyticsManager.setPrefSubsidiary(ctx, currentSubsidiary.toLowerCase())
        }
    }

    private fun setChannel(ctx: Context, channel: String?) {
        Log.d(TAG, "##### setChannel()")
        //for debug mode
        if (channel != null && channel.isNotEmpty()) {
            Log.d(TAG, "##### debug channel: $channel")
            mAnalyticsManager.setPrefChannel(ctx, channel.toLowerCase())
            mAnalyticsManager.channel(ctx, channel)
            return
        }
        val currentChannel = mAnalyticsManager.channel(ctx)
        if (currentChannel == null || currentChannel.isEmpty()) {
            Log.d(TAG, "##### currentChannel: $currentChannel")
            //check preference channel value
            val prefChannel = mAnalyticsManager.getPrefChannel(ctx)
            if (prefChannel == null || prefChannel.isEmpty()) {
                mAnalyticsManager.setPrefChannel(ctx, DEBUG_CHANNEL)
            }
        } else {
            mAnalyticsManager.setPrefChannel(ctx, currentChannel.toLowerCase())
        }
    }

    /**
     * Get the screen name that was just added to the current screen name, in other words, take away the analytic route
     * information. For example, if the current screen name is "Menu_Demo_End card", the result is the string "End card"
     *
     * @return A string value for the actual screen name minus any route screen name.
     */
    private fun getLastScreen(screen: String): String {
        return if (screen.isNotEmpty()) {
            val beginIndexOfCurrentScreen = screen.lastIndexOf("_", screen.length - 1)
            screen.substring(beginIndexOfCurrentScreen + 1, screen.length)
        } else {
            ""
        }
    }
    //endregion
}