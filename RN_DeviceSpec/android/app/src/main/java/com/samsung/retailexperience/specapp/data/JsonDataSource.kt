package com.samsung.retailexperience.specapp.data

import com.samsung.retailexperience.specapp.data.model.ActivityModel

interface JsonDataSource {
    fun getActivityModel(json: String?): ActivityModel
//    fun getZoomModel(json: String?): ZoomModel
    fun getActivityModels(json: String): MutableMap<String, ActivityModel>

    fun getClassName(json: String?): String
    fun getForwardEnterAnim(json: String?): String
    fun getForwardExitAnim(json: String?): String
    fun getBackwardEnterAnim(json: String?): String
    fun getBackwardExitAnim(json: String?): String
    fun getBrightness(json: String): Int
    fun getScreenMode(json: String): String
    fun getDisplayCutoutMode(json: String): String
    fun getTimeoutTime(json: String): Long

    //region devicespec
//    fun getDeviceSpecModel(json: String): DeviceSpecModel
//    fun getDevicesModel(json: String): DevicesModel
//    fun getDevicesComparisonChart(json: String): BeyondComparisonModel
//    fun getSpecsModel(json: String): SpecsModel
    fun getJsonModelText(json: String): String
//    fun getScreenSlidePageModel(json: String): ScreenSlidePageModel.Pages
    //endregion

//    fun getWidgetModel(json: String): WidgetListModel
//    fun getSubWidgetModel(json: String): SubWidgetListModel
//
//    fun getDemoList(json: String): MainMenuModel
}