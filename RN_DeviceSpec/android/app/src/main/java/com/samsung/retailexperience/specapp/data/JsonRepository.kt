package com.samsung.retailexperience.specapp.data

import android.content.Context
import com.google.common.base.Strings
import com.samsung.retailexperience.specapp.data.model.ActivityModel
import com.tecace.retail.res.util.ResJsonUtil

class JsonRepository (private val context: Context): JsonDataSource {
    companion object {
        private val TAG = JsonRepository::class.java.simpleName

        @Synchronized
        fun getInstance(context: Context): JsonDataSource {
            return JsonRepository(context)
        }
    }

    override fun getActivityModel(json: String?): ActivityModel {
        return ResJsonUtil.getInstance().loadJsonModel<ActivityModel>(context, json, ActivityModel::class.java) ?: ActivityModel()
    }
//
//    override fun getZoomModel(json: String?): ZoomModel {
//        return ResJsonUtil.getInstance().loadJsonModel<ZoomModel>(context, json, ZoomModel::class.java) ?: ZoomModel()
//    }

    override fun getActivityModels(json: String): MutableMap<String, ActivityModel> {
        val map = mutableMapOf<String, ActivityModel>()
        val activityModel: ActivityModel = getActivityModel(json)
        map["CURRENT"] = activityModel

        if (!Strings.isNullOrEmpty(activityModel.action))        map["FORWARD"] = getActivityModel(activityModel.action)
        if (!Strings.isNullOrEmpty(activityModel.actionBackKey)) map["BACKWARD"] = getActivityModel(activityModel.actionBackKey)
        if (!Strings.isNullOrEmpty(activityModel.timeoutGoTo))   map["TIMEOUT"] = getActivityModel(activityModel.timeoutGoTo)

        return map
    }

    override fun getClassName(json: String?): String {
        return getActivityModel(json).className
    }

    override fun getForwardEnterAnim(json: String?): String {
        return getActivityModel(json).forwardEnterAnim
    }

    override fun getForwardExitAnim(json: String?): String {
        return getActivityModel(json).forwardExitAnim
    }

    override fun getBackwardEnterAnim(json: String?): String {
        return getActivityModel(json).backwardEnterAnim
    }

    override fun getBackwardExitAnim(json: String?): String {
        return getActivityModel(json).backwardExitAnim
    }

    override fun getBrightness(json: String): Int {
        return getActivityModel(json).brightness
    }

    override fun getScreenMode(json: String): String {
        return getActivityModel(json).screenMode
    }

    override fun getDisplayCutoutMode(json: String): String {
        return getActivityModel(json).displayCutoutMode
    }

    override fun getTimeoutTime(json: String): Long {
        return getActivityModel(json).timeoutTime
    }

    //region devicespec
//    override fun getDeviceSpecModel(json: String): DeviceSpecModel {
//        return ResJsonUtil.getInstance().loadJsonModel<DeviceSpecModel>(context, json, DeviceSpecModel::class.java)
//    }
//
//    override fun getDevicesModel(json: String): DevicesModel {
//        return ResJsonUtil.getInstance().loadJsonModel<DevicesModel>(context, json, DevicesModel::class.java)
//    }
//
//    override fun getDevicesComparisonChart(json: String): BeyondComparisonModel {
//        return ResJsonUtil.getInstance().loadJsonModel<BeyondComparisonModel>(context, json, BeyondComparisonModel::class.java)
//    }

//    override fun getSpecsModel(json: String): SpecsModel {
////        return ResJsonUtil.getInstance().loadJsonModel<SpecsModel>(context, json, SpecsModel::class.java)
////    }

    override fun getJsonModelText(json: String): String {
        return ResJsonUtil.getInstance().getJsonText(context, json)
    }

//    override fun getScreenSlidePageModel(json: String): ScreenSlidePageModel.Pages {
//        return ResJsonUtil.getInstance().loadJsonModel<ScreenSlidePageModel.Pages>(context, json, ScreenSlidePageModel.Pages::class.java)
//    }
//    //endregion
//
//    override fun getWidgetModel(json: String): WidgetListModel {
//        return ResJsonUtil.getInstance().loadJsonModel<WidgetListModel>(context, json, WidgetListModel::class.java)
//    }
//
//    override fun getSubWidgetModel(json: String): SubWidgetListModel {
//        return ResJsonUtil.getInstance().loadJsonModel<SubWidgetListModel>(context, json, SubWidgetListModel::class.java)
//    }
//
//    override fun getDemoList(json: String): MainMenuModel {
//        return ResJsonUtil.getInstance().loadJsonModel<MainMenuModel>(context, MainMenuModel::class.java, json)
//    }
}