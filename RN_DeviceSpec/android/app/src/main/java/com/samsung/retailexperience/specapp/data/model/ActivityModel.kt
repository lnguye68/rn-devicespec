package com.samsung.retailexperience.specapp.data.model

import android.util.Log
import com.google.gson.annotations.SerializedName

data class ActivityModel (
        @SerializedName(ID_ME)
        private var _me: String = "",

        @SerializedName(ID_NAME)
        private var _name: String = "",

        @SerializedName(ID_CLASS_NAME)
        private var _className: String = "",

        @SerializedName(ID_LAYOUT)
        private var _layout: String = "",

        @SerializedName(ID_BACKGROUND)
        private var _background: String = "",

        @SerializedName(ID_ACTION)
        private var _action: String? = null,

        @SerializedName(ID_ACTION_BACK_KEY)
        private var _actionBackKey: String? = null,

        @SerializedName(ID_END_PAGE_INFO)
        private var _endPageInfo: String? = null,

        @SerializedName(ID_FORWARD_ENTER_ANIM)
        private var _forwardEnterAnim: String = "",

        @SerializedName(ID_FORWARD_EXIT_ANIM)
        private var _forwardExitAnim: String = "",

        @SerializedName(ID_BACKWARD_ENTER_ANIM)
        private var _backwardEnterAnim: String = "",

        @SerializedName(ID_BACKWARD_EXIT_ANIM)
        private var _backwardExitAnim: String = "",

        @SerializedName(ID_PIVOT_X)
        private var _pivotX: String = "0",

        @SerializedName(ID_PIVOT_Y)
        private var _pivotY: String = "0",


        @SerializedName(ID_SPEAKER_VOLUME)
        private var _speakerVolume: String = "80",

        @SerializedName(ID_HEADSET_VOLUME)
        private var _headsetVolume: String = "35",

        @SerializedName(ID_BRIGHTNESS)
        private var _brightness: String = "80",

        @SerializedName(ID_SCREEN_MODE)
        private var _screenMode: String = "0x00",

        @SerializedName(ID_DISPLAY_CUTOUT_MODE)
        private var _displayCutoutMode: String = "1",

        @SerializedName(ID_ORIENTATION)
        private var _orientation: String = "",

        @SerializedName(ID_TIMEOUT_TIME)
        private var _timeoutTime: String = "0",

        @SerializedName(ID_TIMEOUT_GOTO)
        private var _timeoutGoTo: String? = null

): RetailModel() {

    //region me
    var me = _me
        get() = _me
    //endregion


    //region name
    var name = _name
        get() = _name
    //endregion


    //region className
    var className = _className
        get() = _className
    //endregion


    //region layout
    var layout = _layout
        get() = _layout
    //endregion


    //region background
    var background = _background
        get() = _background
    //endregion

    //region action
    var action = _action
        get() = _action
    //endregion

    //region actionBackKey
    var actionBackKey = _actionBackKey
        get() = _actionBackKey
    //endregion

    //region endPageInfo
    var endPageInfo
        get() = _endPageInfo
        set(value) { _endPageInfo = value }
    //endregion

    //region forwaredEnterAnim
    var forwardEnterAnim = _forwardEnterAnim
        get() = _forwardEnterAnim
    //endregion


    //region forwardExitAnim
    var forwardExitAnim = _forwardExitAnim
        get() = _forwardExitAnim
    //endregion


    //region backwardEnterAnim
    var backwardEnterAnim = _backwardEnterAnim
        get() = _backwardEnterAnim
    //endregion


    //region backwardExitAnim
    var backwardExitAnim = _backwardExitAnim
        get() = _backwardExitAnim
    //endregion

    //region pivotX
    var pivotX = _pivotX
        get() = _pivotX
    //endregion


    //region pivotY
    var pivotY = _pivotY
        get() = _pivotY
    //endregion


    //region volume
    var speakerVolume: Int = _speakerVolume.toInt()
        get() = _speakerVolume.toInt()

    var headsetVolume: Int = _headsetVolume.toInt()
        get() = _headsetVolume.toInt()
    //endregion

    //region brightness
    var brightness: Int = _brightness.toInt()
        get() = _brightness.toInt()
    //endregion

    //region screen mode
    var screenMode: String = _screenMode
        get() = _screenMode
    //endregion

    //region display cutout mode
    var displayCutoutMode: String = _displayCutoutMode
        get() = _displayCutoutMode
    //endregion

    //region orientation
    var orientation = _orientation
        get() = _orientation
    //endregion


    //region timeoutTime
    var timeoutTime: Long = _timeoutTime.toLong()
        get() = _timeoutTime.toLong()
    //endregion


    //region timeoutGoTo
    var timeoutGoTo = _timeoutGoTo
        get() = _timeoutGoTo
    //endregion



    override fun toString(): String {
        var sbuilder = StringBuilder()
        appendString(sbuilder, "AcitivtyModel [")
        appendString(sbuilder, "$ID_ME = $_me")
        appendString(sbuilder, "$ID_NAME = $_name")
        appendString(sbuilder, "$ID_CLASS_NAME = $_className")
        appendString(sbuilder, "$ID_LAYOUT = $_layout")
        appendString(sbuilder, "$ID_BACKGROUND = $_background")
        appendString(sbuilder, "$ID_ACTION = $_action")
        appendString(sbuilder, "$ID_ACTION_BACK_KEY = $_actionBackKey")
        appendString(sbuilder, "$ID_END_PAGE_INFO = $_endPageInfo")
        appendString(sbuilder, "$ID_FORWARD_ENTER_ANIM = $_forwardEnterAnim")
        appendString(sbuilder, "$ID_FORWARD_EXIT_ANIM = $_forwardExitAnim")
        appendString(sbuilder, "$ID_BACKWARD_ENTER_ANIM = $_backwardEnterAnim")
        appendString(sbuilder, "$ID_BACKWARD_EXIT_ANIM = $_backwardExitAnim")
        appendString(sbuilder, "$ID_PIVOT_X = $_pivotX")
        appendString(sbuilder, "$ID_PIVOT_Y = $_pivotY")

        appendString(sbuilder, "$ID_SPEAKER_VOLUME = $_speakerVolume")
        appendString(sbuilder, "$ID_HEADSET_VOLUME = $_headsetVolume")
        appendString(sbuilder, "$ID_BRIGHTNESS = $_brightness")
        appendString(sbuilder, "$ID_SCREEN_MODE = $_screenMode")
        appendString(sbuilder, "$ID_DISPLAY_CUTOUT_MODE = $_displayCutoutMode")
        appendString(sbuilder, "$ID_ORIENTATION = $_orientation")
        appendString(sbuilder, "$ID_TIMEOUT_TIME = $_timeoutTime")
        appendString(sbuilder, "$ID_TIMEOUT_GOTO = $_timeoutGoTo")

        appendString(sbuilder, "]")
        return sbuilder.toString()
    }

    override fun print() {
        Log.d(ActivityModel::class.java.name, toString())
    }

    companion object {
        private const val ID_ME = "ME"
        private const val ID_NAME = "NAME"
        private const val ID_CLASS_NAME = "CLASS_NAME"
        private const val ID_LAYOUT = "LAYOUT"
        private const val ID_BACKGROUND = "BACKGROUND"

        private const val ID_ACTION = "ACTION"
        private const val ID_ACTION_BACK_KEY = "ACTION_BACK_KEY"
        private const val ID_END_PAGE_INFO = "END_PAGE_INFO"
        private const val ID_FORWARD_ENTER_ANIM = "FORWARD_ENTER_ANIM"
        private const val ID_FORWARD_EXIT_ANIM = "FORWARD_EXIT_ANIM"
        private const val ID_BACKWARD_ENTER_ANIM = "BACKWARD_ENTER_ANIM"
        private const val ID_BACKWARD_EXIT_ANIM = "BACKWARD_EXIT_ANIM"
        private const val ID_PIVOT_X = "PIVOT_X"
        private const val ID_PIVOT_Y = "PIVOT_Y"

        private const val ID_SPEAKER_VOLUME = "SPEAKER_VOLUME"
        private const val ID_HEADSET_VOLUME = "HEADSET_VOLUME"
        private const val ID_BRIGHTNESS = "BRIGHTNESS"
        private const val ID_SCREEN_MODE = "SCREEN_MODE"
        private const val ID_DISPLAY_CUTOUT_MODE = "DISPLAY_CUTOUT_MODE"
        private const val ID_ORIENTATION = "ORIENTATION"
        private const val ID_TIMEOUT_TIME = "TIMEOUT_TIME"
        private const val ID_TIMEOUT_GOTO = "TIMEOUT_GOTO"
    }
}