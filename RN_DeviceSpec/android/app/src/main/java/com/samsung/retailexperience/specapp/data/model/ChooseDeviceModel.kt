package com.samsung.retailexperience.specapp.data.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ChooseDeviceModel (
    var name: String
)
