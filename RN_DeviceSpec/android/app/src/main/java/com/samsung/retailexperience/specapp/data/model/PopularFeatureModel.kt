package com.samsung.retailexperience.specapp.data.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PopularFeatureModel(
    var image: String,
    var title: String,
    var subtitle: String,
    var description: String
)