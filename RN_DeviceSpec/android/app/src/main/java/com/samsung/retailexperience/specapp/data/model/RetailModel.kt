package com.samsung.retailexperience.specapp.data.model

import java.io.Serializable

abstract class RetailModel: Serializable {
    fun appendString(builder: StringBuilder, value: String) {
        builder.append(value + System.lineSeparator())
    }

    abstract fun print()
}