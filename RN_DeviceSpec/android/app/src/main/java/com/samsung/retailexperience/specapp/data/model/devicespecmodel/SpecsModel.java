package com.samsung.retailexperience.specapp.data.model.devicespecmodel;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by icanmobile on 12/4/17.
 */

public class SpecsModel extends ArrayList<SpecsModel.Spec> implements Serializable, Cloneable {

    private static final String FIELD_DEVICE_SPEC_MODEL_LAYOUT = "layout";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_IMAGE = "deviceImage";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_NAME = "deviceName";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_NAME_LARGE = "deviceNameLarge";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_NAME_FONT = "deviceNameFont";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_NAME_FONT_SIZE = "deviceNameFontSize";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_NAME_FONT_COLOR = "deviceNameFontColor";

    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM = "deviceSpecItem";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_FONT = "deviceSpecItemFont";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_TEXT_SIZE = "deviceSpecItemTextSize";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_TEXT_COLOR = "deviceSpecItemTextColor";

    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC = "deviceSpec";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FONT = "deviceSpecFont";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_TEXT_SIZE = "deviceSpecTextSize";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_TEXT_COLOR = "deviceSpecTextColor";

    //region Compare Device data
    private static final String FIELD_DEVICE_SPEC_MODEL_COMPARED_DEVICE_IMAGE = "comparedDeviceImage";
    private static final String FIELD_DEVICE_SPEC_MODEL_COMPARED_DEVICE_NAME = "comparedDeviceName";
    private static final String FIELD_DEVICE_SPEC_MODEL_COMPARED_DEVICE_NAME_FONT = "comparedDeviceNameFont";
    private static final String FIELD_DEVICE_SPEC_MODEL_COMPARED_DEVICE_NAME_FONT_SIZE = "comparedDeviceNameFontSize";
    private static final String FIELD_DEVICE_SPEC_MODEL_COMPARED_DEVICE_NAME_FONT_COLOR = "comparedDeviceNameFontColor";

    private static final String FIELD_DEVICE_SPEC_MODEL_COMPARED_DEVICE_SPEC = "comparedDeviceSpec";
    private static final String FIELD_DEVICE_SPEC_MODEL_COMPARED_DEVICE_SPEC_FONT = "comparedDeviceSpecFont";
    private static final String FIELD_DEVICE_SPEC_MODEL_COMPARED_DEVICE_SPEC_TEXT_SIZE = "comparedDeviceSpecTextSize";
    private static final String FIELD_DEVICE_SPEC_MODEL_COMPARED_DEVICE_SPEC_TEXT_COLOR = "comparedDeviceSpecTextColor";

    //endregion

    private static final String FIELD_DEVICE_SPEC_MODEL_ACTION = "action";
    private static final String FIELD_DEVICE_SPEC_MODEL_COLORS = "colors";
    private static final String FIELD_DEVICE_SPEC_MODEL_COMPARED_COLORS = "comparedColors";
    private static final String FIELD_DEVICE_SPEC_MODEL_COLUMN1 = "column1";
    private static final String FIELD_DEVICE_SPEC_MODEL_COLUMN2 = "column2";
    private static final String FIELD_DEVICE_SPEC_MODEL_COLUMN3 = "column3";

    private static final String FIELD_DEVICE_SPEC_MODEL_COMPARE = "compare";
    private static final String FIELD_DEVICE_SPEC_MODEL_COMPARE_FONT = "compareFont";
    private static final String FIELD_DEVICE_SPEC_MODEL_COMPARE_TEXT_SIZE = "compareTextSize";
    private static final String FIELD_DEVICE_SPEC_MODEL_COMPARE_TEXT_COLOR = "compareTextColor";


    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_DISCLAIMER = "deviceSpecItemDisclaimer";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_DISCLAIMER_FONT = "deviceSpecItemDisclaimerFont";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_DISCLAIMER_SIZE = "deviceSpecItemDisclaimerSize";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_DISCLAIMER_COLOR = "deviceSpecItemDisclaimerColor";

    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_LEFT_ITEM = "deviceSpecLeftItem";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_LEFT_ITEM_FONT = "deviceSpecLeftItemFont";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_LEFT_ITEM_SIZE = "deviceSpecLeftItemSize";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_LEFT_ITEM_COLOR = "deviceSpecLeftItemColor";

    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_RIGHT_ITEM = "deviceSpecRightItem";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_RIGHT_ITEM_FONT = "deviceSpecRightItemFont";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_RIGHT_ITEM_SIZE = "deviceSpecRightItemSize";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_RIGHT_ITEM_COLOR = "deviceSpecRightItemColor";

    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_TOP_TITLE = "deviceSpecFullItemTopTitle";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_TOP_TITLE_FONT = "deviceSpecFullItemTopTitleFont";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_TOP_TITLE_SIZE = "deviceSpecFullItemTopTitleSize";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_TOP_TITLE_COLOR = "deviceSpecFullItemTopTitleColor";

    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_TOP = "deviceSpecFullItemTop";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_TOP_FONT = "deviceSpecFullItemTopFont";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_TOP_SIZE = "deviceSpecFullItemTopSize";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_TOP_COLOR = "deviceSpecFullItemTopColor";

    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_BOTTOM_TITLE = "deviceSpecFullItemBottomTitle";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_BOTTOM_TITLE_FONT = "deviceSpecFullItemBottomTitleFont";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_BOTTOM_TITLE_SIZE = "deviceSpecFullItemBottomTitleSize";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_BOTTOM_TITLE_COLOR = "deviceSpecFullItemBottomTitleColor";

    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_BOTTOM = "deviceSpecFullItemBottom";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_BOTTOM_FONT = "deviceSpecFullItemBottomFont";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_BOTTOM_SIZE = "deviceSpecFullItemBottomSize";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_BOTTOM_COLOR = "deviceSpecFullItemBottomColor";

    private static final String FIELD_DEVICE_SPEC_MODEL_SPEC_POSITION = "specPosition";
    private static final String FIELD_DEVICE_SPEC_MODEL_SPEC_POSITION_2 = "specPosition2";

    private static final String FIELD_DEVICE_SPEC_MODEL_SPEC_INFO_JSON = "specInfoJson";
    private static final String FIELD_DEVICE_SPEC_MODEL_SPEC_INFO_JSON_2 = "specInfoJson2";

    private static final String FIELD_DEVICE_SPEC_ITEM_ICON = "specIcon";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_SUBTITLE = "deviceSpecItemSubtitle";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_SUBTITLE_FONT = "deviceSpecItemSubtitleFont";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_SUBTITLE_SIZE = "deviceSpecItemSubtitleTextSize";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_SUBTITLE_COLOR = "deviceSpecItemSubtitleTextColor";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_SUBTITLE_2 = "deviceSpecItemSubtitle2";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_SUBTITLE_FONT_2 = "deviceSpecItemSubtitleFont2";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_SUBTITLE_SIZE_2 = "deviceSpecItemSubtitleTextSize2";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_SUBTITLE_COLOR_2 = "deviceSpecItemSubtitleTextColor2";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_2 = "deviceSpecItem2";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_FONT_2 = "deviceSpecItemFont2";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_TEXT_SIZE_2 = "deviceSpecItemTextSize2";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_TEXT_COLOR_2 = "deviceSpecItemTextColor2";

    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM = "deviceSpecFullItem";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_FONT = "deviceSpecFullItemFont";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_SIZE = "deviceSpecFullItemSize";
    private static final String FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_COLOR = "deviceSpecFullItemColor";

    private static final String FIELD_DEVICE_SPEC_MODEL_LEARN_MORE_TEXT = "learnMoreText";
    private static final String FIELD_DEVICE_SPEC_MODEL_LEARN_MORE_TEXT_FONT = "learnMoreTextFont";
    private static final String FIELD_DEVICE_SPEC_MODEL_LEARN_MORE_TEXT_FONT_SIZE= "learnMoreTextSize";
    private static final String FIELD_DEVICE_SPEC_MODEL_LEARN_MORE_TEXT_FONT_COLOR= "learnMoreTextColor";

    private static final String FIELD_DEVICE_SPEC_MODEL_LEARN_MORE_TEXT_2 = "learnMoreText2";
    private static final String FIELD_DEVICE_SPEC_MODEL_LEARN_MORE_TEXT_FONT_2 = "learnMoreTextFont2";
    private static final String FIELD_DEVICE_SPEC_MODEL_LEARN_MORE_TEXT_FONT_SIZE_2= "learnMoreTextSize2";
    private static final String FIELD_DEVICE_SPEC_MODEL_LEARN_MORE_TEXT_FONT_COLOR_2= "learnMoreTextColor2";


    private static final String FIELD_DEVICE_SPEC_MODEL_DRAW_HORIZONTAL_DIVIDER = "drawHorizontalDivider";
    private static final String FIELD_DEVICE_SPEC_MODEL_DRAW_VERTICAL_DIVIDER = "drawVerticalDivider";

    private static final String FIELD_DEVICE_SPEC_MODEL_LEFT_IMAGE = "leftImage";
    private static final String FIELD_DEVICE_SPEC_MODEL_RIGHT_IMAGE = "rightImage";



    public static class Spec implements Cloneable {
        @SerializedName(FIELD_DEVICE_SPEC_MODEL_LAYOUT)
        private String layout;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_IMAGE)
        private String deviceImage;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_NAME)
        private String deviceName;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_NAME_LARGE)
        private String deviceNameLarge;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_NAME_FONT)
        private String deviceNameFont;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_NAME_FONT_SIZE)
        private String deviceNameFontSize;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_NAME_FONT_COLOR)
        private String deviceNameFontColor;



        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM)
        private String deviceSpecItem;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_FONT)
        private String deviceSpecItemFont;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_TEXT_SIZE)
        private String deviceSpecItemTextSize;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_TEXT_COLOR)
        private String deviceSpecItemTextColor;


        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC)
        private String deviceSpec;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FONT)
        private String deviceSpecFont;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_TEXT_SIZE)
        private String deviceSpecTextSize;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_TEXT_COLOR)
        private String deviceSpecTextColor;



        @SerializedName(FIELD_DEVICE_SPEC_MODEL_COMPARED_DEVICE_IMAGE)
        private String comparedDeviceImage;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_COMPARED_DEVICE_NAME)
        private String comparedDeviceName;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_COMPARED_DEVICE_NAME_FONT)
        private String comparedDeviceNameFont;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_COMPARED_DEVICE_NAME_FONT_SIZE)
        private String comparedDeviceNameFontSize;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_COMPARED_DEVICE_NAME_FONT_COLOR)
        private String comparedDeviceNameFontColor;



        @SerializedName(FIELD_DEVICE_SPEC_MODEL_COMPARED_DEVICE_SPEC)
        private String comparedDeviceSpec;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_COMPARED_DEVICE_SPEC_FONT)
        private String comparedDeviceSpecFont;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_COMPARED_DEVICE_SPEC_TEXT_SIZE)
        private String comparedDeviceSpecTextSize;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_COMPARED_DEVICE_SPEC_TEXT_COLOR)
        private String comparedDeviceSpecTextColor;



        @SerializedName(FIELD_DEVICE_SPEC_MODEL_ACTION)
        private String action;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_COLORS)
        private ArrayList<String> colors = new ArrayList<String>();

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_COMPARED_COLORS)
        private ArrayList<String> comparedColors = new ArrayList<String>();

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_COLUMN1)
        private ArrayList<Spec> column1 = new ArrayList<>();

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_COLUMN2)
        private ArrayList<Spec> column2 = new ArrayList<>();

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_COLUMN3)
        private ArrayList<Spec> column3 = new ArrayList<>();


        @SerializedName(FIELD_DEVICE_SPEC_MODEL_COMPARE)
        private String compare;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_COMPARE_FONT)
        private String compareFont;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_COMPARE_TEXT_SIZE)
        private String compareTextSize;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_COMPARE_TEXT_COLOR)
        private String compareTextColor;



        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_DISCLAIMER)
        private String deviceSpecItemDisclaimer;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_DISCLAIMER_FONT)
        private String deviceSpecItemDisclaimerFont;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_DISCLAIMER_SIZE)
        private String deviceSpecItemDisclaimerSize;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_DISCLAIMER_COLOR)
        private String deviceSpecItemDisclaimerColor;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_LEFT_ITEM)
        private String deviceSpecLeftItem;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_LEFT_ITEM_FONT)
        private String deviceSpecLeftItemFont;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_LEFT_ITEM_SIZE)
        private String deviceSpecLeftItemSize;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_LEFT_ITEM_COLOR)
        private String deviceSpecLeftItemColor;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_RIGHT_ITEM)
        private String deviceSpecRightItem;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_RIGHT_ITEM_FONT)
        private String deviceSpecRightItemFont;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_RIGHT_ITEM_SIZE)
        private String deviceSpecRightItemSize;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_RIGHT_ITEM_COLOR)
        private String deviceSpecRightItemColor;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM)
        private String deviceSpecFullItem;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_FONT)
        private String deviceSpecFullItemFont;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_SIZE)
        private String deviceSpecFullItemSize;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_COLOR)
        private String deviceSpecFullItemColor;


        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_TOP_TITLE)
        private String deviceSpecFullItemTopTitle;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_TOP_TITLE_FONT)
        private String deviceSpecFullItemTopTitleFont;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_TOP_TITLE_SIZE)
        private String deviceSpecFullItemTopTitleSize;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_TOP_TITLE_COLOR)
        private String deviceSpecFullItemTopTitleColor;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_TOP)
        private String deviceSpecFullItemTop;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_TOP_FONT)
        private String deviceSpecFullItemTopFont;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_TOP_SIZE)
        private String deviceSpecFullItemTopSize;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_TOP_COLOR)
        private String deviceSpecFullItemTopColor;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_BOTTOM_TITLE)
        private String deviceSpecFullItemBottomTitle;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_BOTTOM_TITLE_FONT)
        private String deviceSpecFullItemBottomTitleFont;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_BOTTOM_TITLE_SIZE)
        private String deviceSpecFullItemBottomTitleSize;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_BOTTOM_TITLE_COLOR)
        private String deviceSpecFullItemBottomTitleColor;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_BOTTOM)
        private String deviceSpecFullItemBottom;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_BOTTOM_FONT)
        private String deviceSpecFullItemBottomFont;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_BOTTOM_SIZE)
        private String deviceSpecFullItemBottomSize;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_FULL_ITEM_BOTTOM_COLOR)
        private String deviceSpecFullItemBottomColor;


        @SerializedName(FIELD_DEVICE_SPEC_MODEL_SPEC_POSITION)
        private Integer specPosition;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_SPEC_POSITION_2)
        private Integer specPosition2;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_SPEC_INFO_JSON)
        private String specInfoJson;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_SPEC_INFO_JSON_2)
        private String specInfoJson2;

        @SerializedName(FIELD_DEVICE_SPEC_ITEM_ICON)
        private String specIcon;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_SUBTITLE)
        private String subtitle;
        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_SUBTITLE_FONT)
        private String subtitleFont;
        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_SUBTITLE_SIZE)
        private String subtitleTextSize;
        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_SUBTITLE_COLOR)
        private String subtitleTextColor;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_SUBTITLE_2)
        private String subtitle2;
        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_SUBTITLE_FONT_2)
        private String subtitleFont2;
        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_SUBTITLE_SIZE_2)
        private String subtitleTextSize2;
        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_SUBTITLE_COLOR_2)
        private String subtitleTextColor2;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_2)
        private String deviceSpecItem2;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_FONT_2)
        private String deviceSpecItemFont2;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_TEXT_SIZE_2)
        private String deviceSpecItemTextSize2;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DEVICE_SPEC_ITEM_TEXT_COLOR_2)
        private String deviceSpecItemTextColor2;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_LEARN_MORE_TEXT)
        private String learnMoreText;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_LEARN_MORE_TEXT_FONT)
        private String learnMoreTextFont;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_LEARN_MORE_TEXT_FONT_SIZE)
        private String learnMoreTextSize;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_LEARN_MORE_TEXT_FONT_COLOR)
        private String learnMoreTextColor;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_LEARN_MORE_TEXT_2)
        private String learnMoreText2;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_LEARN_MORE_TEXT_FONT_2)
        private String learnMoreTextFont2;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_LEARN_MORE_TEXT_FONT_SIZE_2)
        private String learnMoreTextSize2;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_LEARN_MORE_TEXT_FONT_COLOR_2)
        private String learnMoreTextColor2;


        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DRAW_HORIZONTAL_DIVIDER)
        private String drawHorizontalDivider;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_DRAW_VERTICAL_DIVIDER)
        private String drawVerticalDivider;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_LEFT_IMAGE)
        private String leftImage;

        @SerializedName(FIELD_DEVICE_SPEC_MODEL_RIGHT_IMAGE)
        private String rightImage;

        public String layout() {
            return this.layout;
        }
        public void layout(String layout) {
            this.layout = layout;
        }

        public String deviceImage() {
            return this.deviceImage;
        }
        public void deviceImage(String deviceImage) {
            this.deviceImage = deviceImage;
        }

        public String deviceName() {
            return this.deviceName;
        }
        public void deviceName(String deviceName) {
            this.deviceName = deviceName;
        }


        public String deviceNameLarge() {
            return this.deviceNameLarge;
        }
        public void deviceNameLarge(String deviceNameLarge) {
            this.deviceNameLarge = deviceNameLarge;
        }

        public String deviceNameFont() {
            return deviceNameFont;
        }

        public void deviceNameFont(String deviceNameFont) {
            this.deviceNameFont = deviceNameFont;
        }

        public String deviceNameFontSize() {
            return deviceNameFontSize;
        }

        public void deviceNameFontSize(String deviceNameFontSize) {
            this.deviceNameFontSize = deviceNameFontSize;
        }

        public String deviceNameFontColor() {
            return deviceNameFontColor;
        }

        public void deviceNameFontColor(String deviceNameFontColor) {
            this.deviceNameFontColor = deviceNameFontColor;
        }

        public String deviceSpecItem() {
            return this.deviceSpecItem;
        }
        public void deviceSpecItem(String deviceSpecItem) {
            this.deviceSpecItem = deviceSpecItem;
        }

        public String deviceSpecItemFont() {
            return this.deviceSpecItemFont;
        }
        public void deviceSpecItemFont(String deviceSpecItemFont) {
            this.deviceSpecItemFont = deviceSpecItemFont;
        }

        public String deviceSpecItemTextSize() {
            return this.deviceSpecItemTextSize;
        }
        public void deviceSpecItemTextSize(String deviceSpecItemTextSize) {
            this.deviceSpecItemTextSize = deviceSpecItemTextSize;
        }

        public String deviceSpecItemTextColor() {
            return this.deviceSpecItemTextColor;
        }
        public void deviceSpecItemTextColor(String deviceSpecItemTextColor) {
            this.deviceSpecItemTextColor = deviceSpecItemTextColor;
        }

        public String deviceSpec() {
            return this.deviceSpec;
        }
        public void deviceSpec(String deviceSpec) {
            this.deviceSpec = deviceSpec;
        }

        public String deviceSpecFont() {
            return this.deviceSpecFont;
        }
        public void deviceSpecFont(String deviceSpecFont) {
            this.deviceSpecFont = deviceSpecFont;
        }

        public String deviceSpecTextSize() {
            return this.deviceSpecTextSize;
        }
        public void deviceSpecTextSize(String deviceSpecTextSize) {
            this.deviceSpecTextSize = deviceSpecTextSize;
        }

        public String deviceSpecTextColor() {
            return this.deviceSpecTextColor;
        }
        public void deviceSpecTextColor(String deviceSpecTextColor) {
            this.deviceSpecTextColor = deviceSpecTextColor;
        }



        public String comparedDeviceImage() {
            return this.comparedDeviceImage;
        }
        public void comparedDeviceImage(String comparedDeviceImage) {
            this.comparedDeviceImage = comparedDeviceImage;
        }

        public String comparedDeviceName() {
            return this.comparedDeviceName;
        }
        public void comparedDeviceName(String comparedDeviceName) {
            this.comparedDeviceName = comparedDeviceName;
        }

        public String comparedDeviceNameFont() {
            return comparedDeviceNameFont;
        }

        public void comparedDeviceNameFont(String comparedDeviceNameFont) {
            this.comparedDeviceNameFont = comparedDeviceNameFont;
        }

        public String comparedDeviceNameFontSize() {
            return comparedDeviceNameFontSize;
        }

        public void comparedDeviceNameFontSize(String comparedDeviceNameFontSize) {
            this.comparedDeviceNameFontSize = comparedDeviceNameFontSize;
        }

        public String comparedDeviceNameFontColor() {
            return comparedDeviceNameFontColor;
        }

        public void comparedDeviceNameFontColor(String comparedDeviceNameFontColor) {
            this.comparedDeviceNameFontColor = comparedDeviceNameFontColor;
        }

        public String comparedDeviceSpec() {
            return this.comparedDeviceSpec;
        }
        public void comparedDeviceSpec(String comparedDeviceSpec) {
            this.comparedDeviceSpec = comparedDeviceSpec;
        }

        public String comparedDeviceSpecFont() {
            return this.comparedDeviceSpecFont;
        }
        public void comparedDeviceSpecFont(String comparedDeviceSpecFont) {
            this.comparedDeviceSpecFont = comparedDeviceSpecFont;
        }

        public String comparedDeviceSpecTextSize() {
            return this.comparedDeviceSpecTextSize;
        }
        public void comparedDeviceSpecTextSize(String comparedDeviceSpecTextSize) {
            this.comparedDeviceSpecTextSize = comparedDeviceSpecTextSize;
        }

        public String comparedDeviceSpecTextColor() {
            return this.comparedDeviceSpecTextColor;
        }
        public void comparedDeviceSpecTextColor(String comparedDeviceSpecTextColor) {
            this.comparedDeviceSpecTextColor = comparedDeviceSpecTextColor;
        }

        public String deviceSpecItemDisclaimer() {
            return this.deviceSpecItemDisclaimer;
        }
        public void deviceSpecItemDisclaimer(String deviceSpecItemDisclaimer) {
            this.deviceSpecItemDisclaimer = deviceSpecItemDisclaimer;
        }

        public String deviceSpecItemDisclaimerFont() {
            return this.deviceSpecItemDisclaimerFont;
        }
        public void setDeviceSpecItemFont(String deviceSpecItemDisclaimerFont) {
            this.deviceSpecItemDisclaimerFont = deviceSpecItemDisclaimerFont;
        }

        public String deviceSpecItemDisclaimerSize() {
            return this.deviceSpecItemDisclaimerSize;
        }
        public void deviceSpecItemDisclaimerSize(String deviceSpecItemDisclaimerSize) {
            this.deviceSpecItemDisclaimerSize = deviceSpecItemDisclaimerSize;
        }

        public String deviceSpecItemDisclaimerColor() {
            return this.deviceSpecItemDisclaimerColor;
        }
        public void deviceSpecItemDisclaimerColor(String deviceSpecItemDisclaimerColor) {
            this.deviceSpecItemDisclaimerColor = deviceSpecItemDisclaimerColor;
        }

        public String deviceSpecLeftItem() {
            return this.deviceSpecLeftItem;
        }
        public void deviceSpecLeftItem(String deviceSpecLeftItem) {
            this.deviceSpecLeftItem = deviceSpecLeftItem;
        }

        public String deviceSpecLeftItemFont() {
            return this.deviceSpecLeftItemFont;
        }
        public void deviceSpecLeftItemFont(String deviceSpecLeftItemFont) {
            this.deviceSpecLeftItemFont = deviceSpecLeftItemFont;
        }

        public String deviceSpecLeftItemSize() {
            return this.deviceSpecLeftItemSize;
        }
        public void deviceSpecLeftItemSize(String deviceSpecLeftItemSize) {
            this.deviceSpecLeftItemSize = deviceSpecLeftItemSize;
        }

        public String deviceSpecLeftItemColor() {
            return this.deviceSpecLeftItemColor;
        }
        public void deviceSpecLeftItemColor(String deviceSpecLeftItemColor) {
            this.deviceSpecLeftItemColor = deviceSpecLeftItemColor;
        }


        public String deviceSpecRightItem() {
            return this.deviceSpecRightItem;
        }
        public void deviceSpecRightItem(String deviceSpecRightItem) {
            this.deviceSpecRightItem = deviceSpecRightItem;
        }

        public String deviceSpecRightItemFont() {
            return this.deviceSpecRightItemFont;
        }
        public void deviceSpecRightItemFont(String deviceSpecRightItemFont) {
            this.deviceSpecRightItemFont = deviceSpecRightItemFont;
        }

        public String deviceSpecRightItemSize() {
            return this.deviceSpecRightItemSize;
        }
        public void deviceSpecRightItemSize(String deviceSpecRightItemSize) {
            this.deviceSpecRightItemSize = deviceSpecRightItemSize;
        }

        public String deviceSpecRightItemColor() {
            return this.deviceSpecRightItemColor;
        }
        public void deviceSpecRightItemColor(String deviceSpecRightItemColor) {
            this.deviceSpecRightItemColor = deviceSpecRightItemColor;
        }

        public String deviceSpecFullItemTopTitle() {
            return this.deviceSpecFullItemTopTitle;
        }
        public void deviceSpecFullItemTopTitle(String deviceSpecFullItemTopTitle) {
            this.deviceSpecFullItemTopTitle = deviceSpecFullItemTopTitle;
        }
        public String deviceSpecFullItemTopTitleFont() {
            return this.deviceSpecFullItemTopTitleFont;
        }
        public void deviceSpecFullItemTopTitleFont(String deviceSpecFullItemTopTitleFont) {
            this.deviceSpecFullItemTopTitleFont = deviceSpecFullItemTopTitleFont;
        }
        public String deviceSpecFullItemTopTitleSize() {
            return this.deviceSpecFullItemTopTitleSize;
        }
        public void deviceSpecFullItemTopTitleSize(String deviceSpecFullItemTopTitleSize) {
            this.deviceSpecFullItemTopTitleSize = deviceSpecFullItemTopTitleSize;
        }
        public String deviceSpecFullItemTopTitleColor() {
            return this.deviceSpecFullItemTopTitleColor;
        }
        public void deviceSpecFullItemTopTitleColor(String deviceSpecFullItemTopTitleColor) {
            this.deviceSpecFullItemTopTitleColor = deviceSpecFullItemTopTitleColor;

        }


        public String deviceSpecFullItemTop() {
            return this.deviceSpecFullItemTop;
        }
        public void deviceSpecFullItemTop(String deviceSpecFullItemTop) {
            this.deviceSpecFullItemTop = deviceSpecFullItemTop;
        }
        public String deviceSpecFullItemTopFont() {
            return this.deviceSpecFullItemTopFont;
        }
        public void deviceSpecFullItemTopFont(String deviceSpecFullItemTopFont) {
            this.deviceSpecFullItemTopFont = deviceSpecFullItemTopFont;
        }
        public String deviceSpecFullItemTopSize() {
            return this.deviceSpecFullItemTopSize;
        }
        public void deviceSpecFullItemTopSize(String deviceSpecFullItemTopSize) {
            this.deviceSpecFullItemTopSize = deviceSpecFullItemTopSize;
        }
        public String deviceSpecFullItemTopColor() {
            return this.deviceSpecFullItemTopColor;
        }
        public void deviceSpecFullItemTopColor(String deviceSpecFullItemTopColor) {
            this.deviceSpecFullItemTopColor = deviceSpecFullItemTopColor;
        }

        public String deviceSpecFullItemBottomTitle() {
            return this.deviceSpecFullItemBottomTitle;
        }
        public void deviceSpecFullItemBottomTitle(String deviceSpecFullItemBottomTitle) {
            this.deviceSpecFullItemBottomTitle = deviceSpecFullItemBottomTitle;
        }
        public String deviceSpecFullItemBottomTitleFont() {
            return this.deviceSpecFullItemBottomTitleFont;
        }
        public void deviceSpecFullItemBottomTitleFont(String deviceSpecFullItemBottomTitleFont) {
            this.deviceSpecFullItemBottomTitleFont = deviceSpecFullItemBottomTitleFont;
        }
        public String deviceSpecFullItemBottomTitleSize() {
            return this.deviceSpecFullItemBottomTitleSize;
        }
        public void deviceSpecFullItemBottomTitleSize(String deviceSpecFullItemBottomTitleSize) {
            this.deviceSpecFullItemBottomTitleSize = deviceSpecFullItemBottomTitleSize;
        }
        public String deviceSpecFullItemBottomTitleColor() {
            return this.deviceSpecFullItemBottomTitleColor;
        }
        public void deviceSpecFullItemBottomTitleColor(String deviceSpecFullItemBottomTitleColor) {
            this.deviceSpecFullItemBottomTitleColor = deviceSpecFullItemBottomTitleColor;
        }

        public String deviceSpecFullItemBottom() {
            return this.deviceSpecFullItemBottom;
        }
        public void deviceSpecFullItemBottom(String deviceSpecFullItemBottom) {
            this.deviceSpecFullItemBottom = deviceSpecFullItemBottom;
        }
        public String deviceSpecFullItemBottomFont() {
            return this.deviceSpecFullItemBottomFont;
        }
        public void deviceSpecFullItemBottomFont(String deviceSpecFullItemBottomFont) {
            this.deviceSpecFullItemBottomFont = deviceSpecFullItemBottomFont;
        }
        public String deviceSpecFullItemBottomSize() {
            return this.deviceSpecFullItemBottomSize;
        }
        public void deviceSpecFullItemBottomSize(String deviceSpecFullItemBottomSize) {
            this.deviceSpecFullItemBottomSize = deviceSpecFullItemBottomSize;
        }
        public String deviceSpecFullItemBottomColor() {
            return this.deviceSpecFullItemBottomColor;
        }
        public void deviceSpecFullItemBottomColor(String deviceSpecFullItemBottomColor) {
            this.deviceSpecFullItemBottomColor = deviceSpecFullItemBottomColor;
        }














        public String action() {
            return this.action;
        }
        public void action(String action) {
            this.action = action;
        }


        public ArrayList<String> colors() {
            return this.colors;
        }
        public void colors(ArrayList<String> colors) {
            this.colors = colors;
        }


        public ArrayList<String> comparedColors() {
            return this.comparedColors;
        }
        public void comparedColors(ArrayList<String> comparedColors) {
            this.comparedColors = comparedColors;
        }

        public ArrayList<Spec> column1() {
            return this.column1;
        }
        public void column1(ArrayList<Spec> column1) {
            this.column1 = column1;
        }

        public ArrayList<Spec> column2() {
            return this.column2;
        }
        public void column2(ArrayList<Spec> column2) {
            this.column2 = column2;
        }

        public ArrayList<Spec> column3() {
            return this.column3;
        }
        public void column3(ArrayList<Spec> column3) {
            this.column3 = column3;
        }

        public String compare() {
            return this.compare;
        }
        public void compare(String compare) {
            this.compare = compare;
        }

        public String compareFont() {
            return this.compareFont;
        }
        public void compareFont(String compareFont) {
            this.compareFont = compareFont;
        }

        public String compareTextSize() {
            return this.compareTextSize;
        }
        public void compareTextSize(String compareTextSize) {
            this.compareTextSize = compareTextSize;
        }

        public String compareTextColor() {
            return this.compareTextColor;
        }
        public void compareTextColor(String compareTextColor) {
            this.compareTextColor = compareTextColor;
        }


        public Integer specPosition() {
            return specPosition;
        }
        public void specPosition(Integer specPosition) {
            this.specPosition = specPosition;
        }

        public Integer specPosition2() {
            return specPosition2;
        }
        public void specPosition2(Integer specPosition) {
            this.specPosition2 = specPosition;
        }

        public String specInfoJson() {
            return specInfoJson;
        }
        public void specInfoJson(String specInfoJson) {
            this.specInfoJson = specInfoJson;
        }

        public String specInfoJson2() {
            return specInfoJson2;
        }
        public void specInfoJson2(String specInfoJson) {
            this.specInfoJson2 = specInfoJson;
        }

        public String specIcon() {
            return specIcon;
        }
        public void specIcon(String specIcon) {
            this.specIcon = specIcon;
        }

        public String getSubtitle() {
            return subtitle;
        }

        public void setSubtitle(String subtitle) {
            this.subtitle = subtitle;
        }

        public String getSubtitleFont() {
            return subtitleFont;
        }

        public void setSubtitleFont(String subtitleFont) {
            this.subtitleFont = subtitleFont;
        }

        public String getSubtitleTextSize() {
            return subtitleTextSize;
        }

        public void setSubtitleTextSize(String subtitleTextSize) {
            this.subtitleTextSize = subtitleTextSize;
        }

        public String getSubtitleTextColor() {
            return subtitleTextColor;
        }

        public void setSubtitleTextColor(String subtitleTextColor) {
            this.subtitleTextColor = subtitleTextColor;
        }

        public String getSubtitle2() {
            return subtitle2;
        }

        public void setSubtitle2(String subtitle2) {
            this.subtitle2 = subtitle2;
        }

        public String getSubtitleFont2() {
            return subtitleFont2;
        }

        public void setSubtitleFont2(String subtitleFont2) {
            this.subtitleFont2 = subtitleFont2;
        }

        public String getSubtitleTextSize2() {
            return subtitleTextSize2;
        }

        public void setSubtitleTextSize2(String subtitleTextSize2) {
            this.subtitleTextSize2 = subtitleTextSize2;
        }

        public String getSubtitleTextColor2() {
            return subtitleTextColor2;
        }

        public void setSubtitleTextColor2(String subtitleTextColor2) {
            this.subtitleTextColor2 = subtitleTextColor2;
        }

        public String getDeviceSpecItem2() {
            return deviceSpecItem2;
        }

        public void setDeviceSpecItem2(String deviceSpecItem2) {
            this.deviceSpecItem2 = deviceSpecItem2;
        }

        public String getDeviceSpecItemFont2() {
            return deviceSpecItemFont2;
        }

        public void setDeviceSpecItemFont2(String deviceSpecItemFont2) {
            this.deviceSpecItemFont2 = deviceSpecItemFont2;
        }

        public String getDeviceSpecItemTextSize2() {
            return deviceSpecItemTextSize2;
        }

        public void setDeviceSpecItemTextSize2(String deviceSpecItemTextSize2) {
            this.deviceSpecItemTextSize2 = deviceSpecItemTextSize2;
        }

        public String getDeviceSpecItemTextColor2() {
            return deviceSpecItemTextColor2;
        }

        public void setDeviceSpecItemTextColor2(String deviceSpecItemTextColor2) {
            this.deviceSpecItemTextColor2 = deviceSpecItemTextColor2;
        }

        public String deviceSpecFullItem() {
            return deviceSpecFullItem;
        }

        public void deviceSpecFullItem(String deviceSpecFullItem) {
            this.deviceSpecFullItem = deviceSpecFullItem;
        }

        public String deviceSpecFullItemFont() {
            return deviceSpecFullItemFont;
        }

        public void deviceSpecFullItemFont(String deviceSpecFullItemFont) {
            this.deviceSpecFullItemFont = deviceSpecFullItemFont;
        }

        public String deviceSpecFullItemSize() {
            return deviceSpecFullItemSize;
        }

        public void deviceSpecFullItemSize(String deviceSpecFullItemSize) {
            this.deviceSpecFullItemSize = deviceSpecFullItemSize;
        }

        public String deviceSpecFullItemColor() {
            return deviceSpecFullItemColor;
        }

        public void deviceSpecFullItemColor(String deviceSpecFullItemColor) {
            this.deviceSpecFullItemColor = deviceSpecFullItemColor;
        }

        public String learnMoreText() {
            return learnMoreText;
        }

        public void setLearnMoreText(String learnMoreText) {
            this.learnMoreText = learnMoreText;
        }

        public String learnMoreTextFont() {
            return learnMoreTextFont;
        }

        public void setLearnMoreTextFont(String learnMoreTextFont) {
            this.learnMoreTextFont = learnMoreTextFont;
        }

        public String learnMoreTextSize() {
            return learnMoreTextSize;
        }

        public void setLearnMoreTextSize(String learnMoreTextFontSize) {
            this.learnMoreTextSize = learnMoreTextFontSize;
        }

        public String learnMoreTextColor() {
            return learnMoreTextColor;
        }

        public void setLearnMoreTextColor(String learnMoreTextFontColor) {
            this.learnMoreTextColor = learnMoreTextFontColor;
        }

        public String learnMoreText2() {
            return learnMoreText2;
        }

        public void setLearnMoreText2(String learnMoreText2) {
            this.learnMoreText2 = learnMoreText2;
        }

        public String learnMoreTextFont2() {
            return learnMoreTextFont2;
        }

        public void setLearnMoreTextFont2(String learnMoreTextFont2) {
            this.learnMoreTextFont2 = learnMoreTextFont2;
        }

        public String learnMoreTextSize2() {
            return learnMoreTextSize2;
        }

        public void setLearnMoreTextSize2(String learnMoreTextSize2) {
            this.learnMoreTextSize2 = learnMoreTextSize2;
        }

        public String learnMoreTextColor2() {
            return learnMoreTextColor2;
        }

        public void setLearnMoreTextColor2(String learnMoreTextColor2) {
            this.learnMoreTextColor2 = learnMoreTextColor2;
        }

        public String drawHorizontalDivider() {
            return drawHorizontalDivider;
        }

        public void setDrawHorizontalDivider(String drawHorizontalDivider) {
            this.drawHorizontalDivider = drawHorizontalDivider;
        }

        public String drawVerticalDivider() {
            return drawVerticalDivider;
        }

        public void setDrawVerticalDivider(String drawVerticalDivider) {
            this.drawVerticalDivider = drawVerticalDivider;
        }

        public String leftImage() {
            return leftImage;
        }

        public void setLeftImage(String leftImage) {
            this.leftImage = leftImage;
        }

        public String rightImage() {
            return rightImage;
        }

        public void setRightImage(String rightImage) {
            this.rightImage = rightImage;
        }

        public String toString() {
            StringBuilder builder = new StringBuilder();
            if (this.layout != null)
                appendString(builder, "layout = " + this.layout());
            if (this.deviceImage != null)
                appendString(builder, "deviceImage = " + this.deviceImage());
            if (this.deviceName != null)
                appendString(builder, "deviceName = " + this.deviceName());

            if (this.deviceSpecItem != null)
                appendString(builder, "deviceSpecItem = " + this.deviceSpecItem);
            if (this.deviceSpecItemFont != null)
                appendString(builder, "deviceSpecItemFont = " + this.deviceSpecItemFont);
            if (this.deviceSpecItemTextSize != null)
                appendString(builder, "deviceSpecItemTextSize = " + this.deviceSpecItemTextSize);
            if (this.deviceSpecItemTextColor != null)
                appendString(builder, "deviceSpecItemTextColor = " + this.deviceSpecItemTextColor);

            if (this.deviceSpec != null)
                appendString(builder, "deviceSpec = " + this.deviceSpec());
            if (this.deviceSpecFont != null)
                appendString(builder, "deviceSpecFont = " + this.deviceSpecFont());
            if (this.deviceSpecTextSize != null)
                appendString(builder, "deviceSpecTextSize = " + this.deviceSpecTextSize());
            if (this.deviceSpecTextColor != null)
                appendString(builder, "deviceSpecTextColor = " + this.deviceSpecTextColor());



            if (this.comparedDeviceImage != null)
                appendString(builder, "comparedDeviceImage = " + this.comparedDeviceImage());
            if (this.comparedDeviceName != null)
                appendString(builder, "comparedDeviceName = " + this.comparedDeviceName());

            if (this.comparedDeviceSpec != null)
                appendString(builder, "comparedDeviceSpec = " + this.comparedDeviceSpec());
            if (this.comparedDeviceSpecFont != null)
                appendString(builder, "comparedDeviceSpecFont = " + this.comparedDeviceSpecFont());
            if (this.comparedDeviceSpecTextSize != null)
                appendString(builder, "comparedDeviceSpecTextSize = " + this.comparedDeviceSpecTextSize());
            if (this.comparedDeviceSpecTextColor != null)
                appendString(builder, "comparedDeviceSpecTextColor = " + this.comparedDeviceSpecTextColor());

            if (this.deviceSpecItemDisclaimer != null)
                appendString(builder, "deviceSpecItemDisclaimer = " + this.deviceSpecItemDisclaimer());
            if (this.deviceSpecItemDisclaimerFont != null)
                appendString(builder, "deviceSpecItemDisclaimerFont = " + this.deviceSpecItemDisclaimerFont());
            if (this.deviceSpecItemDisclaimerSize != null)
                appendString(builder, "deviceSpecItemDisclaimerSize = " + this.deviceSpecItemDisclaimerSize());
            if (this.deviceSpecItemDisclaimerColor != null)
                appendString(builder, "deviceSpecItemDisclaimerColor = " + this.deviceSpecItemDisclaimerColor());


            if (this.action != null)
                appendString(builder, "action = " + this.action());
            if (this.colors != null && this.colors().size() > 0) {
                appendString(builder, "colors = [");
                for(String color : colors)
                    appendString(builder, "" + color);
                appendString(builder, "]");
            }
            if (this.comparedColors != null && this.comparedColors().size() > 0) {
                appendString(builder, "comparedColors = [");
                for(String color : comparedColors)
                    appendString(builder, "" + color);
                appendString(builder, "]");
            }

            if (this.compare() != null)
                appendString(builder, "compare = " + this.compare());
            if (this.compareFont() != null)
                appendString(builder, "compareFont = " + this.compareFont());
            if (this.compareTextSize() != null)
                appendString(builder, "compareTextSize = " + this.compareTextSize());
            if (this.compareTextColor() != null)
                appendString(builder, "compareTextColor = " + this.compareTextColor());


            if (this.specPosition() != null)
                appendString(builder, "specPosition = " + this.specPosition());

           if (this.specInfoJson() != null)
                appendString(builder, "specInfoJson = " + this.specInfoJson());

            if (specIcon != null) {
                appendString(builder, "specIcon = " + this.specIcon());
            }
            return builder.toString();
        }

        public void appendString(StringBuilder builder, String value) {
            builder.append(value + System.getProperty("line.separator"));
        }

        public void print() {
            Log.d(Spec.class.getName(), toString());
        }

        @Override
        public Spec clone() throws CloneNotSupportedException {
            return (Spec) super.clone();
        }
    }

    /**
     * toString
     * @return string values of MainMenuModel object.
     */
    public String toString() {
        StringBuilder sbuilder = new StringBuilder();
        for(Spec spec : this) {
            appendString(sbuilder, spec.toString());
        }
        return sbuilder.toString();
    }

    public void appendString(StringBuilder builder, String value) {
        builder.append(value + System.getProperty("line.separator"));
    }

    /**
     * print
     */
    public void print() {
        Log.d(SpecsModel.class.getName(), toString());
    }

    @Override
    public SpecsModel clone() {
        return (SpecsModel) super.clone();
    }
}
