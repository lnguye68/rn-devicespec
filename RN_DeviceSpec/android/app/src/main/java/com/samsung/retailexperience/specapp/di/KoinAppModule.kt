package com.samsung.retailexperience.specapp.di

import com.samsung.retailexperience.specapp.analytics.AppAnalyticsManagerImpl
import com.samsung.retailexperience.specapp.data.JsonDataSource
import com.samsung.retailexperience.specapp.data.JsonRepository
import com.tecace.retail.util.HomeKeyWatcher
import com.tecace.retail.util.PreferenceUtil
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val koinAppModule = module {
    single<JsonDataSource> {
        JsonRepository(
            androidContext()
        )
    }

    single {
        AppAnalyticsManagerImpl.getInstance()
    }

    single {
        PreferenceUtil.getInstance()
    }

    factory { HomeKeyWatcher(androidContext()) }
}