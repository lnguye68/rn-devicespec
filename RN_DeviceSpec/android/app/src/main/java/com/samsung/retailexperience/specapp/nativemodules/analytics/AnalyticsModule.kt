package com.samsung.retailexperience.specapp.nativemodules.analytics

import android.annotation.SuppressLint
import com.facebook.react.bridge.Promise
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.bridge.ReactContextBaseJavaModule
import com.facebook.react.bridge.ReactMethod
import com.samsung.retailexperience.specapp.analytics.AppAnalyticsManager
import org.koin.core.KoinComponent
import org.koin.core.inject


class AnalyticsModule(
    val reactContext: ReactApplicationContext
): ReactContextBaseJavaModule(reactContext), KoinComponent {

    private val mAppAnalyticsManager: AppAnalyticsManager by inject()

    companion object {
        private val TAG = AnalyticsModule::class.java.simpleName
        const val START_APP = "START_APP"
        const val WIDGET = "WIDGET"
        const val NFC = "NFC"
        const val BLUE_TOOTH = "BLUE_TOOTH"
        const val BIXBY = "BIXBY"
        const val BACK_KEY_PRESS = "BACK_KEY_PRESS"
        const val BACK_ICON_PRESS = "BACK_ICON_PRESS"
        const val TIME_OUT = "TIME_OUT"
        const val VIDEO_COMPLETED = "VIDEO_COMPLETED"
        const val HOME_KEY_PRESS = "HOME_KEY_PRESS"
        const val TAP = "TAP"
        const val SWIPE = "SWIPE"
    }

    override fun getName(): String {
        return TAG
    }

    @SuppressLint("DefaultLocale")
    override fun getConstants(): Map<String, Any>? {
        val constants: MutableMap<String, Any> = HashMap()
        constants[START_APP] = START_APP
        constants[BACK_KEY_PRESS] = BACK_KEY_PRESS
        constants[TIME_OUT] = TIME_OUT
        constants[VIDEO_COMPLETED] = VIDEO_COMPLETED
        constants[HOME_KEY_PRESS] = HOME_KEY_PRESS
        constants[BACK_ICON_PRESS] = BACK_ICON_PRESS
        constants[TAP] = TAP
        constants[SWIPE] = SWIPE
        return constants
    }

    @ReactMethod
    fun sendScreenChangeEvent(causeType: String, currentScreenName: String, toScreen: String) {
        mAppAnalyticsManager.screenChangeEvent(
            context = reactContext,
            causeType = causeType,
            currentScreen = currentScreenName,
            toScreen = toScreen
        )
    }

    @ReactMethod
    fun sendUserEvent(causeType: String, extra: String?) {
        mAppAnalyticsManager.userEvent(
            context = reactContext,
            userAction = causeType,
            extra = extra
        )
    }

    @ReactMethod
    fun sendOrientationEvent() {
        mAppAnalyticsManager.userOrientationEvent(context = reactContext)
    }

    @ReactMethod
    fun getSubsidiary(promise: Promise) {
        val subsidiary = mAppAnalyticsManager.getSubsidiary(reactContext)
        promise.resolve(subsidiary)
    }

    @ReactMethod
    fun getChannel(promise: Promise) {
        val subsidiary = mAppAnalyticsManager.getChannel(reactContext)
        promise.resolve(subsidiary)
    }

}
