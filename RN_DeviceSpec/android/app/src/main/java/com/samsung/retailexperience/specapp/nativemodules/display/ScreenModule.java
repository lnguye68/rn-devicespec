package com.samsung.retailexperience.specapp.nativemodules.display;

import android.content.res.Configuration;
import android.util.Log;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.UiThreadUtil;
import com.tecace.retail.util.DisplayUtil;

public class ScreenModule extends ReactContextBaseJavaModule {
    private static final String TAG = ScreenModule.class.getSimpleName();
    private  ReactApplicationContext mReactContext;
    public ScreenModule(ReactApplicationContext reactContext) {
        super(reactContext);
        mReactContext = reactContext;
    }

    @NonNull
    @Override
    public String getName() {
        return TAG;
    }

    @ReactMethod
    public void setFullScreen(boolean isFullScreen) {
        if (isFullScreen) {
            UiThreadUtil.runOnUiThread(
                new Runnable() {
                    @Override
                    public void run() {
                        DisplayUtil.getInstance().setScreenMode(
                                getCurrentActivity(),
                                DisplayUtil.FULLSCREEN_NORMAL_MODE,
                                false,
                                0,
                                false,
                                0
                        );
                    }
                }
            );
        } else {
            UiThreadUtil.runOnUiThread(
                new Runnable() {
                    @Override
                    public void run() {
                        DisplayUtil.getInstance().setScreenMode(
                                getCurrentActivity(),
                                DisplayUtil.NON_FULLSCREEN_NORMAL_MODE,
                                false,
                                0,
                                false,
                                0
                        );
                    }
                }
            );
        }
    }

    @ReactMethod
    public void setDisplayCutout(boolean isDisplayCutoff) {
        if (isDisplayCutoff) {
            UiThreadUtil.runOnUiThread(
                new Runnable() {
                    @Override
                    public void run() {
                        DisplayUtil.getInstance().setDisplayCutoutMode(
                                getCurrentActivity(),
                                DisplayUtil.DISPLAY_CUTOUT_SHORT_EDGES
                        );
                    }
                }
            );
        } else {
            UiThreadUtil.runOnUiThread(
                new Runnable() {
                    @Override
                    public void run() {
                        DisplayUtil.getInstance().setDisplayCutoutMode(
                                getCurrentActivity(),
                                DisplayUtil.DISPLAY_CUTOUT_DEFAULT
                        );
                    }
                }
            );
        }
    }

    @ReactMethod
    public void getOrientation(Promise promise) {
        try {
            int orientation = getCurrentActivity().getResources().getConfiguration().orientation;
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                promise.resolve("land");
            } else {
                promise.resolve("port");
            }
        } catch (Exception e) {
            Log.e("ScreenModule", e.toString());
            promise.reject(e);
        }
    }
}
