package com.samsung.retailexperience.specapp.nativemodules.files;

import android.util.Log;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.tecace.retail.res.util.ResFileUtil;

public class FileModule extends ReactContextBaseJavaModule {
    private static final String TAG = FileModule.class.getSimpleName();

    private static ReactApplicationContext reactContext;

    public FileModule(ReactApplicationContext context) {
        reactContext = context;
    }

    @NonNull
    @Override
    public String getName() {
        return TAG;
    }

    @ReactMethod
    public void getFilePath(String fileName, Callback success, Callback error) {
        String filePath = ResFileUtil.getInstance().getExternalResourcePathWithLocale(
                reactContext,
                fileName
        );

        Log.d(TAG, "filePath = " + filePath);

        if (filePath == null || filePath.isEmpty()) {
            error.invoke("File is not found in external folder.");
        } else {
            success.invoke(filePath);
        }
    }
}
