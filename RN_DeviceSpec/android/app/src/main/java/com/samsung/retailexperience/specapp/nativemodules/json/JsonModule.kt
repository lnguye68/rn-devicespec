package com.samsung.retailexperience.specapp.nativemodules.json

import android.util.Log
import com.facebook.react.bridge.*
import com.samsung.retailexperience.specapp.data.JsonDataSource
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.koin.core.KoinComponent
import org.koin.core.inject


class JsonModule(reactContext: ReactApplicationContext): ReactContextBaseJavaModule(reactContext), KoinComponent {

    private val jsonDataSource: JsonDataSource by inject()

    companion object {
        private val TAG = JsonModule::class.java.simpleName
    }

    override fun getName(): String {
        return TAG
    }

    @ReactMethod
    fun getJsonModelText(jsonPath: String, success: Callback, error: Callback) {
        Log.d(TAG, "Getting model for path: $jsonPath")
        val modelText: String? = jsonDataSource.getJsonModelText(jsonPath)
        if (modelText.isNullOrEmpty()) {
            error.invoke("Could not load json text with path: $jsonPath")
        } else {
            Log.d(TAG, "model text: $modelText")
//            val jsonObject = JSONObject(modelText)
//            Log.d(TAG, "json object: $jsonObject")
//            val convertedJsonArray = convertJsonToMap(jsonObject)
            success.invoke(modelText)
        }
    }

    @ReactMethod
    fun getJsonModelTextAsync(jsonPath: String, promise: Promise) {
        Log.d(TAG, "Getting model for path: $jsonPath")
        val modelText: String? = jsonDataSource.getJsonModelText(jsonPath)
        if (modelText.isNullOrEmpty()) {
            promise.reject("Could not load json text with path: $jsonPath")
        } else {
            Log.d(TAG, "model text: $modelText")
//            val jsonObject = JSONObject(modelText)
//            Log.d(TAG, "json object: $jsonObject")
//            val convertedJsonArray = convertJsonToMap(jsonObject)
            promise.resolve(modelText)
        }
    }

    //region conversion json to array/map
    /**
     *   Source: https://gist.github.com/viperwarp/2beb6bbefcc268dee7ad
     */
    @Throws(JSONException::class)
    private fun convertJsonToMap(jsonObject: JSONObject): WritableMap? {
        val map: WritableMap = WritableNativeMap()
        val iterator = jsonObject.keys()
        while (iterator.hasNext()) {
            val key = iterator.next()
            val value = jsonObject[key]
            if (value is JSONObject) {
                map.putMap(key, convertJsonToMap(value))
            } else if (value is JSONArray) {
                map.putArray(key, convertJsonToArray(value))
            } else if (value is Boolean) {
                map.putBoolean(key, value)
            } else if (value is Int) {
                map.putInt(key, value)
            } else if (value is Double) {
                map.putDouble(key, value)
            } else if (value is String) {
                map.putString(key, value)
            } else {
                map.putString(key, value.toString())
            }
        }
        return map
    }
    /**
     *   Source: https://gist.github.com/viperwarp/2beb6bbefcc268dee7ad
     */
    @Throws(JSONException::class)
    private fun convertJsonToArray(jsonArray: JSONArray): WritableArray? {
        val array: WritableArray = WritableNativeArray()
        for (i in 0 until jsonArray.length()) {
            val value = jsonArray[i]
            if (value is JSONObject) {
                array.pushMap(convertJsonToMap(value))
            } else if (value is JSONArray) {
                array.pushArray(convertJsonToArray(value))
            } else if (value is Boolean) {
                array.pushBoolean(value)
            } else if (value is Int) {
                array.pushInt(value)
            } else if (value is Double) {
                array.pushDouble(value)
            } else if (value is String) {
                array.pushString(value)
            } else {
                array.pushString(value.toString())
            }
        }
        return array
    }
    /**
     *   Source: https://gist.github.com/viperwarp/2beb6bbefcc268dee7ad
     */
    @Throws(JSONException::class)
    private fun convertMapToJson(readableMap: ReadableMap?): JSONObject? {
        val `object` = JSONObject()
        val iterator = readableMap!!.keySetIterator()
        while (iterator.hasNextKey()) {
            val key = iterator.nextKey()
            when (readableMap.getType(key)) {
                ReadableType.Null -> `object`.put(key, JSONObject.NULL)
                ReadableType.Boolean -> `object`.put(key, readableMap.getBoolean(key))
                ReadableType.Number -> `object`.put(key, readableMap.getDouble(key))
                ReadableType.String -> `object`.put(key, readableMap.getString(key))
                ReadableType.Map -> `object`.put(key, convertMapToJson(readableMap.getMap(key)))
                ReadableType.Array -> `object`.put(key, convertArrayToJson(readableMap.getArray(key)))
            }
        }
        return `object`
    }

    @Throws(JSONException::class)
    private fun convertArrayToJson(readableArray: ReadableArray?): JSONArray? {
        val array = JSONArray()
        for (i in 0 until readableArray!!.size()) {
            when (readableArray.getType(i)) {
                ReadableType.Null -> {
                }
                ReadableType.Boolean -> array.put(readableArray.getBoolean(i))
                ReadableType.Number -> array.put(readableArray.getDouble(i))
                ReadableType.String -> array.put(readableArray.getString(i))
                ReadableType.Map -> array.put(convertMapToJson(readableArray.getMap(i)))
                ReadableType.Array -> array.put(convertArrayToJson(readableArray.getArray(i)))
            }
        }
        return array
    }
    //endregion
}