package com.samsung.retailexperience.specapp.nativemodules.localize;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import java.util.Locale;

public class LocalizeModule extends ReactContextBaseJavaModule {
    private static final String TAG = LocalizeModule.class.getSimpleName();

    private static ReactApplicationContext reactContext;

    public LocalizeModule(ReactApplicationContext context) {
        reactContext = context;
    }

    @NonNull
    @Override
    public String getName() {
        return TAG;
    }

    @ReactMethod
    public void getLanguageCountry(Promise p) {
        String language = Locale.getDefault().getLanguage();
        String country = Locale.getDefault().getCountry();
        String languageAndCountry = String.format("%s-r%s", language, country);
        p.resolve(languageAndCountry);
    }

    @ReactMethod
    public void getLanguage(Promise p) {
        String language = Locale.getDefault().getLanguage();
        p.resolve(language);
    }

}
