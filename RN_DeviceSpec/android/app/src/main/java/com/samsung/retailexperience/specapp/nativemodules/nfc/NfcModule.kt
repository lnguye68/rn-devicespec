package com.samsung.retailexperience.specapp.nativemodules.nfc

import android.util.Log
import com.facebook.react.bridge.Callback
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.bridge.ReactContextBaseJavaModule
import com.facebook.react.bridge.ReactMethod
import com.samsung.retailexperience.specapp.utils.AppConst
import com.tecace.retail.util.PreferenceUtil


class NfcModule(
        val reactContext: ReactApplicationContext
): ReactContextBaseJavaModule(reactContext) {

    companion object {
        private val TAG = NfcModule::class.java.simpleName
    }

    override fun getName(): String {
        return TAG
    }

    @ReactMethod
    fun getNFCVal(callbackFun: Callback) {
        val nfcVal = PreferenceUtil.getInstance().getString(
                reactContext,
                AppConst.LAUNCH_SCREEN)
        Log.d(TAG, "nfcVal = $nfcVal")

        if (nfcVal.isNullOrEmpty()) {
            callbackFun.invoke("")
        } else {
            callbackFun.invoke(nfcVal)
        }
        PreferenceUtil.getInstance().setString(
                reactContext,
                AppConst.LAUNCH_SCREEN,
                "")
    }
}