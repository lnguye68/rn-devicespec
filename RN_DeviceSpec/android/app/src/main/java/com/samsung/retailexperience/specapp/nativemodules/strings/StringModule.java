package com.samsung.retailexperience.specapp.nativemodules.strings;

import android.util.Log;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.tecace.retail.res.util.ResAssetUtil;
import com.tecace.retail.res.util.ResFileUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

public class StringModule extends ReactContextBaseJavaModule {
    private static final String TAG = StringModule.class.getSimpleName();

    private static ReactApplicationContext reactContext;

    public StringModule(ReactApplicationContext context) {
        reactContext = context;
    }

    @NonNull
    @Override
    public String getName() {
        return TAG;
    }

    @ReactMethod
    public void getStringContent(String fileName, Promise p) {
        String filePath = ResFileUtil.getInstance().getExternalResourcePathWithLocale(
                reactContext,
                fileName
        );

        Log.d(TAG, "filePath = " + filePath);

        try {
            if (filePath == null || filePath.isEmpty()) {
                // external strings.json file is not found, let's find it from inside.
                String stringContent = ResAssetUtil.getInstance().GetTextFromAssetWithLocale(
                        reactContext,
                        fileName
                );
                invokeReactCallbacks(stringContent, p);
            } else {
                String stringContent = getStringFromFile(filePath);
                invokeReactCallbacks(stringContent, p);
            }
        } catch (Exception e) {
            p.reject(e.getLocalizedMessage());
        }
    }

    @ReactMethod
    public void getStringContentPortrait(String fileName, Promise p) {
        String filePath = ResFileUtil.getInstance().getExternalResourcePathWithLocale(
                reactContext,
                fileName
        );

        Log.d(TAG, "filePath = " + filePath);

        try {
            if (filePath == null || filePath.isEmpty()) {
                // external strings.json file is not found, let's find it from inside.
                String stringContent = ResAssetUtil.getInstance().GetTextFromAssetWithLocaleNoOrientation(
                        reactContext,
                        fileName
                );
                invokeReactCallbacks(stringContent, p);
            } else {
                String portraitFilePath = filePath.replace("/land", "");
                String stringContent = getStringFromFile(portraitFilePath);
                invokeReactCallbacks(stringContent, p);
            }
        } catch (Exception e) {
            p.reject(e.getLocalizedMessage());
        }
    }

    @ReactMethod
    public void getDefaultStringContent(String fileName, Promise p) {
        String defaultStrings = ResAssetUtil.getInstance().GetTextFromAsset(
                reactContext,
                fileName
        );
        invokeReactCallbacks(defaultStrings, p);
    }

    @ReactMethod
    public void getDefaultLandStringContent(String fileName, Promise p) {
        String defaultLandStrings = ResAssetUtil.getInstance().GetTextFromAsset(
                reactContext,
                fileName
        );
        p.resolve(defaultLandStrings);
    }

    private void invokeReactCallbacks(String stringContent, Promise p) {
        if (stringContent == null || stringContent.isEmpty()) {
            p.reject("String content is null or empty.");
        } else {
            p.resolve(stringContent);
        }
    }

    private String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    private String getStringFromFile (String filePath) throws Exception {
        File fl = new File(filePath);
        FileInputStream fin = new FileInputStream(fl);
        String ret = convertStreamToString(fin);
        //Make sure you close all streams.
        fin.close();
        return ret;
    }
}
