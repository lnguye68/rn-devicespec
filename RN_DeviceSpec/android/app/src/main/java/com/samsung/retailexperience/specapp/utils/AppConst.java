package com.samsung.retailexperience.specapp.utils;

import com.samsung.retailexperience.specapp.BuildConfig;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by icanmobile on 11/13/17.
 */

public class AppConst {
    public static final int DEFAULT_TIMEOUT_TIME = 30000;

    public static final boolean APP_SELF_FINISH = true;

    // Added by SMHEO (03/26/2018) - SCREEN_SAVER
    // Set the screen saver (It is associated with the Attractor loop).
    // If this value is true, the application wakes up automatically
    // when the screen of the device is turned off.
    public static final boolean APP_ENABLE_SCREEN_SAVER = false;

    public static final String REMOVED_PRELOAD_CONTENT_FILE_NAME = "removedcontents.json";

    public static final String APP_CURRENT_MODEL = "CURRENT_MODEL";

    public static final ArrayList<String> fonts = new ArrayList<String>(Arrays.asList(
//            "fonts/HelveticaNeue-Light.ttf",
//            "fonts/RobotoCondensed-Bold.ttf",
//            "fonts/Roboto-Bold.ttf",
//            "fonts/Roboto-Light.ttf",
//            "fonts/Roboto-Regular.ttf",
//            "fonts/Roboto-Thin.ttf",

//            "fonts/SamsungGothicCondensed-Regular.ttf",
            "fonts/SamsungOne-400.ttf",
            "fonts/SamsungOne-500.ttf",
            "fonts/SamsungOne-600.ttf",
            "fonts/SamsungOne-700.ttf",
//            "fonts/SamsungOne-800.ttf",
//            "fonts/SamsungOneLatinWeb-600.ttf",
            "fonts/SamsungOneLatinWeb-700.ttf",

            "fonts/SamsungSharpSans-Bold.ttf",
            "fonts/SamsungSharpSans-Medium.ttf",
            "fonts/SamsungSharpSans-Regular.ttf"
//            "fonts/SECRobotoLight-Bold.ttf",
//            "fonts/SECRobotoLight-Regular.ttf"
    ));

    // environment file
    public static final String APP_CONFIG_ENV = "model/app_environment.json";

    // strings file
    public static final String APP_STRING_XML = "model/strings.xml";
    public static final String APP_STRING_EXCEL_XML = "model/strings_excel.xml";
    //endregion

    // dimension file
    public static final String APP_DIMEN_XML = "model/dimens.xml";

    // VOLUME CONSTANTS
    public static final String VOLUME_FIRST_TIME_KEY = "VOLUME_FIRST_TIME_KEY";

    public static final String PREFERENCE_SUBSIDIARY = "PREFERENCE_SUBSIDIARY";
    public static final String PREFERENCE_CARRIER = "PREFERENCE_CARRIER";

    //region devicespec
    public static final String SELECTED_DEVICE_PREFERENCE = "SELECTED_DEVICE_PREFERENCE";
    public static final String POPUP_PAGE_PREFERENCE = "POPUP_PAGE_PREFERENCE";
    //endregion
    public static final String CURRENT_SUBSIDIARY = "CURRENT_SUBSIDIARY";

    public static final String KEY_PREV_SCREEN_NAME = "key_prev_screen_name";


    // The names of the Davinci devices
    public static final String DEVICE_CODE_NAME = "Hubble";
    public static final String DEVICE_X1_LTE = "Hubble_X1_LTE";
    public static final String DEVICE_X1_5G = "Hubble_X1_5G";
    public static final String DEVICE_Y2_LTE = "Hubble_Y2_LTE";
    public static final String DEVICE_Y2_5G = "Hubble_Y2_5G";
    public static final String DEVICE_Z3_5G = "Hubble_Z3_5G";
    public static final String DEVICE_S20 = "Galaxy S20";
    public static final String DEVICE_S20_PLUS = "Galaxy S20+";
    public static final String DEVICE_S20_ULTRA = "Galaxy S20 Ultra";

    // VZW Download
//    public static final String amazon_davinci_vzw_content_url = "https://rm-davinci-file-storage.s3.us-east-2.amazonaws.com/davinci_vzw_contents_v" + BuildConfig.VERSION_NAME + ".zip";
    public static final String AMAZON_VZW_CONTENT_URL = "https://rm-hubble-storage.s3.us-east-2.amazonaws.com/" + BuildConfig.VERSION_NAME + "/rms_hash_v" + BuildConfig.VERSION_NAME + "_server.json";
    public static final String SAVED_DOWNLOAD_ID = "SAVED_RETAIL_DOWNLOAD_ID";
    public static final String SERVER_JSON_KEY = "SERVER_JSON_KEY";
    public static final String DOWNLOAD_ID_SET = "DOWNLOAD_ID_SET";
    public static final String DOWNLOAD_SIZE = "DOWNLOAD_SIZE";


    // Widget/Subsidiary
    public static boolean DID_UPDATE_WIDGET = false;
    public static boolean DID_CHECK_SUBSIDIARY = false;


    //For Hubble project Main Menu Activity
    public static final String ARG_SHOULD_PAUSE_VIDEO = "ARG_SHOULD_PAUSE_VIDEO";
    public static final String KEY_STATE_DEMO_MODEL_DATA = "KEY_STATE_DEMO_MODEL_DATA";
    public static final String KEY_STATE_VIDEO_MODEL_DATA = "KEY_STATE_VIDEO_MODEL_DATA";
    public static final String KEY_STATE_DEMO_NAME = "KEY_STATE_DEMO_NAME";
    public static final String KEY_STATE_VIDEO_POSITION = "KEY_STATE_VIDEO_POSITION";
    public static final String KEY_STATE_SHOULD_PAUSE = "KEY_STATE_SHOULD_PAUSE";

    //For mp3 file type
    public static final String MP3_TYPE = ".mp3";


    //For analytics ID for VZW version
    public static final String KEY_DEVICE_ANALYTIC_ID = "KEY_DEVICE_ANALYTIC_ID";


    public static final String LAUNCH_SCREEN = "LAUNCH_SCREEN";
    public static final String APP_LAUNCH_TYPE = "APP_LAUNCH_TYPE";
}