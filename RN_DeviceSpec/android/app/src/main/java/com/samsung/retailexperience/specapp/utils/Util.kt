package rm_devicespec.util

import android.content.BroadcastReceiver
import android.content.Context

interface Util {
    companion object {
        const val START_ACTIVITY_FROM = "START_ACTIVITY_FROM"

        enum class StartActivityFrom {
            NATIVE {
                override fun type() = "NATIVE"
            },
            WIDGET {
                override fun type() = "WIDGET"
            };

            abstract fun type(): String
        }
    }

    fun px2dp(context: Context, px: Float): Float
    fun dp2px(context: Context, dp: Float): Float

    fun getDevice(): String
    fun getRealDevice(): String

    fun updateAppWidgetProvider(context: Context, extraValue: String)

    fun getCurrentVersionCode(context: Context) : Int
}