package rm_devicespec.util

import android.appwidget.AppWidgetManager
import android.content.*
import android.content.pm.PackageManager
import android.os.Build
import android.util.DisplayMetrics
import com.samsung.retailexperience.specapp.utils.AppConst.*
import com.tecace.retail.util.Consts

class UtilImpl: Util {

    companion object {
        private val INSTANCE: UtilImpl =
            UtilImpl()
        @Synchronized
        fun getInstance(): Util {
            return INSTANCE
        }
    }

    override fun px2dp(context: Context, px: Float): Float {
        val resources = context.resources
        val metrics = resources.displayMetrics
        return px / (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    }

    override fun dp2px(context: Context, dp: Float): Float {
        val resources = context.resources
        val metrics = resources.displayMetrics
        return dp * (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    }

    override fun getDevice(): String {
        try {
            return when (Integer.parseInt(Build.MODEL.substring(4, 7))) {
                980 -> DEVICE_X1_LTE
                981 -> DEVICE_X1_5G
                985 -> DEVICE_Y2_LTE
                986 -> DEVICE_Y2_5G
                988 -> DEVICE_Z3_5G
                else -> DEVICE_X1_LTE
            }
        } catch (e: NullPointerException) {
            return DEVICE_X1_LTE
        } catch (e: NumberFormatException) {
            return DEVICE_X1_LTE
        } catch (e: StringIndexOutOfBoundsException) {
            return DEVICE_X1_LTE
        } catch (e: Exception) {
            return DEVICE_X1_LTE
        }
    }

    override fun getRealDevice(): String {
        try {
            return when (Integer.parseInt(Build.MODEL.substring(4, 7))) {
                980, 981 -> DEVICE_S20
                985, 986 -> DEVICE_S20_PLUS
                988 -> DEVICE_S20_ULTRA
                else -> DEVICE_S20
            }
        } catch (e: NullPointerException) {
            return DEVICE_S20
        } catch (e: NumberFormatException) {
            return DEVICE_S20
        } catch (e: StringIndexOutOfBoundsException) {
            return DEVICE_S20
        } catch (e: Exception) {
            return DEVICE_S20
        }
    }

    override fun updateAppWidgetProvider(context: Context, extraValue: String) {
        val i = Intent(AppWidgetManager.ACTION_APPWIDGET_UPDATE)
        i.putExtra(Consts.EXTRA_KEY_UPDATE_WIDGET, extraValue)
        context.sendBroadcast(i)
    }

    override fun getCurrentVersionCode(context: Context): Int {
        try {
            return context.packageManager.getPackageInfo(context.packageName, 0).versionCode
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
            return -1
        }

    }
}