package com.samsung.retailexperience.specapp.views.choosedevicepager

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.samsung.retailexperience.specapp.R
import com.samsung.retailexperience.specapp.data.model.ChooseDeviceModel
import kotlinx.android.synthetic.main.choose_device_pager_view_item.view.*

class ChooseDeviceAdapter(
    private var mPageMarginAndOffset: Float,
    private var mPageMarginAndOffsetVertical: Float,
    val itemOnClick : (Int) -> Unit)
    : RecyclerView.Adapter<ChooseDeviceAdapter.CustomViewHolder>() {

    private var mDataList = emptyList<ChooseDeviceModel>()
    private var mFontColor = 0
    private var mNameSize = 0f
    private var mNumSize = 0f
    private var mNamePadding = 0

    companion object {
        val MAX_VALUE = 1000
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.choose_device_pager_view_item, parent, false)
        val lp = view.layoutParams as ViewGroup.MarginLayoutParams
        lp.setMargins(
            mPageMarginAndOffset.toInt(),
            mPageMarginAndOffsetVertical.toInt(),
            mPageMarginAndOffset.toInt(),
            mPageMarginAndOffsetVertical.toInt()
        )
        view.layoutParams = lp
        return CustomViewHolder(view)
    }

    override fun getItemCount(): Int {
       return MAX_VALUE
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val realPosition = (position - 1 + this.getListSize()) % this.getListSize()

        // Device Image
        holder.device.setImageResource(R.drawable.device)
        holder.device.setOnClickListener {
            itemOnClick(realPosition)
        }

        // Device Name
        holder.name.text = mDataList[realPosition].name
        holder.name.setPadding(0,0,0, mNamePadding)
        holder.name.textSize = mNameSize
        holder.name.setTextColor(mFontColor)

        // Position Number
        holder.num.text = (realPosition + 1).toString() + "/" + this.getListSize()
        holder.num.setTextColor(mFontColor)
        holder.num.textSize = mNumSize
    }

    class CustomViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val device: ImageView = itemView.tv_device
        val name: TextView = itemView.tv_name
        val num: TextView = itemView.tv_num
    }

    fun getListSize(): Int {
        return mDataList.size
    }

    fun setDataList(dataList: List<ChooseDeviceModel>) {
        mDataList = dataList
        notifyDataSetChanged()
    }

    fun setPageMarginAndOffset(pageMarginAndOffset: Float) {
        mPageMarginAndOffset = pageMarginAndOffset
        notifyDataSetChanged()
    }

    fun setPageMarginAndOffsetVertical(pageMarginAndOffsetVertical: Float) {
        mPageMarginAndOffsetVertical = pageMarginAndOffsetVertical
        notifyDataSetChanged()
    }

    fun setFontColor(fontColor: String) {
        mFontColor = Color.parseColor(fontColor)
    }

    fun setNameSize(nameSize: Float) {
        mNameSize = nameSize
    }

    fun setNumSize(numSize: Float) {
        mNumSize = numSize
    }

    fun setNamePadding(padding: Int) {
        mNamePadding = padding
    }
}
