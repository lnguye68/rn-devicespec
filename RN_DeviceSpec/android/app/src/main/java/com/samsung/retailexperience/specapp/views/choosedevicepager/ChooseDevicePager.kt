package com.samsung.retailexperience.specapp.views.choosedevicepager

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.ViewCompat
import androidx.viewpager2.widget.CompositePageTransformer
import androidx.viewpager2.widget.ViewPager2
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.facebook.react.bridge.ReactContext
import com.facebook.react.bridge.ReadableArray
import com.facebook.react.uimanager.UIManagerModule
import com.facebook.react.uimanager.events.EventDispatcher
import com.samsung.retailexperience.specapp.R
import com.samsung.retailexperience.specapp.data.model.ChooseDeviceModel
import com.samsung.retailexperience.specapp.views.choosedevicepager.ChooseDeviceAdapter.Companion.MAX_VALUE
import com.samsung.retailexperience.specapp.views.events.PageClickedEvent
import com.samsung.retailexperience.specapp.views.events.PageSelectedEvent
import com.samsung.retailexperience.specapp.views.pagertransformers.ScalePageTransformer
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import java.io.IOException
import java.lang.reflect.Type

class ChooseDevicePager(context: Context) : FrameLayout(context) {

    companion object {
        private val TAG = ChooseDevicePager::class.java.simpleName
    }

    private var mEventDispatcher: EventDispatcher = (context as ReactContext).getNativeModule(UIManagerModule::class.java).eventDispatcher
    private val mViewPager2: ViewPager2
    private val mChooseDeviceAdapter: ChooseDeviceAdapter
    private var mPageMargin = resources.getDimensionPixelOffset(R.dimen.pageMargin).toFloat()
    private var mPeekOffset = resources.getDimensionPixelOffset(R.dimen.offset).toFloat()
    private var mPageMarginAndOffset = resources.getDimensionPixelOffset(R.dimen.pageMarginAndOffset).toFloat()
    private var mPageMarginAndOffsetVertical = resources.getDimensionPixelOffset(R.dimen.pageMarginAndOffsetVertical).toFloat()
    private var mFontColor = 0
    private var mNameSize = 0f
    private var mNumSize = 0f
    private var mNamePadding = 0
    private var mInitialPage = 0

    init {
        mEventDispatcher = (context as ReactContext).getNativeModule(UIManagerModule::class.java).eventDispatcher

        val inflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.choose_device_pager_container, this, true)
        mViewPager2 = findViewById(R.id.view_pager)
        mChooseDeviceAdapter = ChooseDeviceAdapter(mPageMarginAndOffset, mPageMarginAndOffsetVertical) { position->onClick(position)}
        updatePager()
    }

    fun setPageMargin(pageMargin: Float) {
        mPageMargin = pageMargin
    }

    fun setPeekOffset(peekOffset: Float) {
        mPeekOffset = peekOffset
    }

    fun setPageMarginAndOffset(pageMarginAndOffset: Float) {
        mPageMarginAndOffset = pageMarginAndOffset
        mChooseDeviceAdapter.setPageMarginAndOffset(pageMarginAndOffset)
    }

    fun setPageMarginAndOffsetVertical(pageMarginAndOffsetVertical: Float) {
        mPageMarginAndOffsetVertical = pageMarginAndOffsetVertical
        mChooseDeviceAdapter.setPageMarginAndOffsetVertical(pageMarginAndOffsetVertical)
    }

    fun setFontColor(fontColor: String) {
        mFontColor = Color.parseColor(fontColor)
        mChooseDeviceAdapter.setFontColor(fontColor)
    }

    fun setNameSize(nameSize: Float) {
        mNameSize = nameSize
        mChooseDeviceAdapter.setNameSize(nameSize)
    }

    fun setNumSize(numSize: Float) {
        mNumSize = numSize
        mChooseDeviceAdapter.setNumSize(numSize)
    }

    fun setNamePadding(padding: Int) {
        mNamePadding = padding
        mChooseDeviceAdapter.setNamePadding(padding)
    }

    fun setInitialPage(pagePosition: Int) {
        mInitialPage = pagePosition
    }

    fun setDataList(dataList: ReadableArray) {
        try {
            val moshi = Moshi.Builder().build()
            val type: Type = Types.newParameterizedType(
                MutableList::class.java,
                ChooseDeviceModel::class.java
            )
            val adapter = moshi.adapter<List<ChooseDeviceModel>>(type)
            val dataListModels: List<ChooseDeviceModel>? = adapter.fromJson(dataList.toString())
            dataListModels?.let {
                mChooseDeviceAdapter.setDataList(it)
                resetCurrentItem(mInitialPage)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    private fun onClick(position: Int) {
        mEventDispatcher.dispatchEvent(
            PageClickedEvent(
                this@ChooseDevicePager.id,
                position
            )
        )
    }
    private fun updatePager() {
        with(mViewPager2) {
            clipToPadding = false
            clipChildren = false
            offscreenPageLimit = 2

            setPageTransformer(CompositePageTransformer().also {
                it.addTransformer { page, position ->
                    val deviceName = page.findViewById<TextView>(R.id.tv_name)
                    val pageNum = page.findViewById<TextView>(R.id.tv_num)
                    val deviceImage = page.findViewById<ImageView>(R.id.tv_device)
                    if (position == 0f) {
                        deviceName.visibility = View.VISIBLE
                        pageNum.visibility = View.VISIBLE
                        deviceImage.colorFilter = null
                    } else {
                        deviceName.visibility = View.INVISIBLE
                        pageNum.visibility = View.INVISIBLE
                        deviceImage.setColorFilter(Color.argb(150,200,200,200))
                    }

                    val viewPager = page.parent.parent as ViewPager2
                    val offset = position * -(2 * mPeekOffset + mPageMargin)
                    if (viewPager.orientation == ViewPager2.ORIENTATION_HORIZONTAL) {
                        if (ViewCompat.getLayoutDirection(viewPager) == ViewCompat.LAYOUT_DIRECTION_RTL) {
                            page.translationX = -offset
                        } else {
                            page.translationX = offset
                        }
                    } else {
                        page.translationY = offset
                    }
                }
                it.addTransformer(ScalePageTransformer())
            })

            registerOnPageChangeCallback(object: OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)

                    val realPosition = getRealPosition(position)
                    val size: Int = mChooseDeviceAdapter.getListSize()
                    val currentPosition = (position - 1 + size) % size

                    if (size > 0 && position == 0 || position == MAX_VALUE - 1) {
                        resetCurrentItem(currentPosition)
                    }

                    mEventDispatcher.dispatchEvent(
                        PageSelectedEvent(
                            this@ChooseDevicePager.id,
                            realPosition
                        )
                    )
                }
            })

            adapter = mChooseDeviceAdapter
        }
    }

    private fun resetCurrentItem(item: Int) {
        mViewPager2.setCurrentItem(MAX_VALUE / 2 - ((MAX_VALUE / 2) % mChooseDeviceAdapter.getListSize()) + 1 + item, false)
        mChooseDeviceAdapter.notifyDataSetChanged()
    }

    private fun getRealPosition(position: Int): Int {
        return (position - 1 + mChooseDeviceAdapter.getListSize()) % mChooseDeviceAdapter.getListSize()
    }
}
