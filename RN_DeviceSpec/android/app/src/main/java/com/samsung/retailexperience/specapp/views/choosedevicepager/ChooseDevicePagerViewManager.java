package com.samsung.retailexperience.specapp.views.choosedevicepager;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.samsung.retailexperience.specapp.views.events.PageClickedEvent;
import com.samsung.retailexperience.specapp.views.events.PageSelectedEvent;

import java.util.Map;

public class ChooseDevicePagerViewManager extends SimpleViewManager<ChooseDevicePager> {

    public static final String REACT_COMPONENT_NAME = ChooseDevicePager.class.getSimpleName();

    @NonNull
    @Override
    public String getName() {
        return REACT_COMPONENT_NAME;
    }

    @NonNull
    @Override
    protected ChooseDevicePager createViewInstance(@NonNull ThemedReactContext reactContext) {
        return new ChooseDevicePager(reactContext);
    }

    @Override
    public Map getExportedCustomDirectEventTypeConstants() {
        return MapBuilder.of(
                PageSelectedEvent.EVENT_NAME, MapBuilder.of("registrationName", "onPageSelected"),
                PageClickedEvent.EVENT_NAME, MapBuilder.of("registrationName", "onPageClicked")
        );
    }

    @ReactProp(name = "pageMargin")
    public void setPageMargin(ChooseDevicePager view, Integer pageMargin) {
        view.setPageMargin(pageMargin);
    }

    @ReactProp(name = "peekOffset")
    public void setPeekOffset(ChooseDevicePager view, Integer offset) {
        view.setPeekOffset(offset);
    }

    @ReactProp(name = "pageMarginAndOffset")
    public void setPageMarginAndOffset(ChooseDevicePager view, Integer pageMarginAndOffset) {
        view.setPageMarginAndOffset(pageMarginAndOffset);
    }

    @ReactProp(name = "pageMarginAndOffsetVertical")
    public void setPageMarginAndOffsetVertical(ChooseDevicePager view, Integer pageMarginAndOffsetVertical) {
        view.setPageMarginAndOffsetVertical(pageMarginAndOffsetVertical);
    }

    @ReactProp(name = "fontColor")
    public void setFontColor(ChooseDevicePager view, String fontColor) {
        view.setFontColor(fontColor);
    }

    @ReactProp(name = "nameSize")
    public void setNameSize(ChooseDevicePager view, float nameSize) {
        view.setNameSize(nameSize);
    }

    @ReactProp(name = "numSize")
    public void setNumSize(ChooseDevicePager view, float numSize) {
        view.setNumSize(numSize);
    }

    @ReactProp(name = "padding")
    public void setNamePadding(ChooseDevicePager view, Integer padding) {
        view.setNamePadding(padding);
    }

    @ReactProp(name = "initialPage")
    public void setInitialPage(ChooseDevicePager view, Integer pagePosition) {
        view.setInitialPage(pagePosition);
    }

    @ReactProp(name = "dataList")
    public void setDataList(ChooseDevicePager view, ReadableArray dataList) {
       view.setDataList(dataList);
    }
}
