package com.samsung.retailexperience.specapp.views.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import com.samsung.retailexperience.specapp.R;
import com.tecace.retail.util.FontTypeface;

/**
 * Created by icanmobile on 5/2/17.
 */

public class CustomFontTextView extends AppCompatTextView {
    private static final String TAG = CustomFontTextView.class.getSimpleName();
    private Context context;

    public CustomFontTextView(Context context) {
        super(context);
        init(context, null, R.attr.customFontStyle);
    }

    public CustomFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs, R.attr.customFontStyle);
        init(context, attrs, R.attr.customFontStyle);
    }

    public CustomFontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs,defStyle);
    }

    private void init(Context ctx, AttributeSet attrs, int defStyle) {
        this.context = ctx;
        if (!isInEditMode()) {
            TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.CustomFontTextView, defStyle, 0);
            String customFont = a.getString(R.styleable.CustomFontTextView_customFont);
            setCustomFont(customFont);
            a.recycle();
        }
    }

    public void setCustomFont(String asset) {
        if (asset != null) {
            Typeface tf = FontTypeface.getInstance().getFont(asset);
            if (tf == null)
                tf = Typeface.createFromAsset(this.context.getAssets(), asset);
            setTypeface(tf);
        }
    }

    public void setCustomFont(Context context, String asset) {
        if (asset != null) {
            try {
                Typeface tf = Typeface.createFromAsset(context.getAssets(), asset);
                setTypeface(tf);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}