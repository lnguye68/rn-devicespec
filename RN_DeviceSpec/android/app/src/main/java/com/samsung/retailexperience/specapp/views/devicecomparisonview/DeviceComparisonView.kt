package com.samsung.retailexperience.specapp.views.devicecomparisonview

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.facebook.react.bridge.ReactContext
import com.facebook.react.uimanager.events.RCTEventEmitter
import com.samsung.retailexperience.specapp.R
import com.samsung.retailexperience.specapp.views.customviews.CustomFontTextView
import com.tecace.retail.res.Res
import kotlinx.android.synthetic.main.device_comparison_view.view.*


class DeviceComparisonView(context: Context): FrameLayout(context){

    init {
        val inflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.device_comparison_view, this, true)
    }

    fun setLeftDeviceImg(deviceImageURI: String) {
        left_device_img.setImageResource(Res.getResId(context, deviceImageURI))
    }

    fun setRightDeviceImg(deviceImageURI: String) {
        right_device_img.setImageResource(Res.getResId(context, deviceImageURI))
        x_icon.setOnClickListener {
            if (context is ReactContext) {
                (context as ReactContext).getJSModule(RCTEventEmitter::class.java)
                        .receiveEvent(id, "onClick", null)
            }
        }
    }

    fun setXIconURI(xIconURI: String?) {
        //Hide the right device
        if (xIconURI.isNullOrEmpty()) {
            right_device_img.visibility = View.INVISIBLE
            right_device_name.visibility = View.INVISIBLE
            x_icon.visibility = View.INVISIBLE
        } else {
            //Hide the left device
            left_device_img.visibility = View.INVISIBLE
            left_device_name.visibility = View.INVISIBLE
            //Set icon for the close button
            x_icon.setImageResource(Res.getResId(context, xIconURI))
        }
    }

    fun setDeviceNameColor(deviceNameColor: String?) {
        deviceNameColor?.let {
            (left_device_name as CustomFontTextView).setTextColor(Color.parseColor(deviceNameColor))
            (right_device_name as CustomFontTextView).setTextColor(Color.parseColor(deviceNameColor))
        }
    }
}