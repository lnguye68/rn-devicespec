package com.samsung.retailexperience.specapp.views.devicecomparisonview;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;

import java.util.Map;

public class DeviceComparisonViewManager extends SimpleViewManager<DeviceComparisonView> {

    private static final String REACT_COMPONENT_NAME = DeviceComparisonView.class.getSimpleName();

    @NonNull
    @Override
    public String getName() {
        return REACT_COMPONENT_NAME;
    }

    @NonNull
    @Override
    protected DeviceComparisonView createViewInstance(@NonNull ThemedReactContext reactContext) {
        return new DeviceComparisonView(reactContext);
    }

    @ReactProp(name = "leftDeviceImg")
    public void setLeftDeviceImg(DeviceComparisonView view, String deviceImageURI) {
        view.setLeftDeviceImg(deviceImageURI);
    }

    @ReactProp(name = "rightDeviceImg")
    public void setRightDeviceImg(DeviceComparisonView view, String deviceImageURI) {
        view.setRightDeviceImg(deviceImageURI);
    }

    @ReactProp(name = "xIconURI")
    public void setXIconURI(DeviceComparisonView view, String xIconURI) {
        view.setXIconURI(xIconURI);
    }

    @ReactProp(name = "deviceNameColor")
    public void setDeviceNameColor(DeviceComparisonView view, String deviceNameColor) {
        view.setDeviceNameColor(deviceNameColor);
    }

    @Nullable
    @Override
    public Map<String, Object> getExportedCustomDirectEventTypeConstants() {
        return MapBuilder.<String, Object>builder()
                .put("onClick",
                        MapBuilder.of("registrationName", "onClick"))
                .build();
    }
}
