package com.samsung.retailexperience.specapp.views.popularfeaturespager

import android.view.View
import com.samsung.retailexperience.specapp.R
import com.samsung.retailexperience.specapp.data.model.PopularFeatureModel
import com.zhpan.bannerview.BaseBannerAdapter

class PopularFeaturesAdapter
    : BaseBannerAdapter<PopularFeatureModel, PopularFeaturesViewHolder>() {

    override fun getLayoutId(viewType: Int): Int {
        return R.layout.popular_features_pager_view_item
    }

    override fun createViewHolder(itemView: View, viewType: Int): PopularFeaturesViewHolder {
        return PopularFeaturesViewHolder(itemView)
    }

    override fun onBind(holder: PopularFeaturesViewHolder, data: PopularFeatureModel, position: Int, pageSize: Int) {
        holder.bindData(data, position, pageSize)
    }
}