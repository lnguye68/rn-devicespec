package com.samsung.retailexperience.specapp.views.popularfeaturespager

import android.content.Context
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.facebook.react.bridge.ReactContext
import com.facebook.react.bridge.ReadableArray
import com.facebook.react.uimanager.UIManagerModule
import com.facebook.react.uimanager.events.EventDispatcher
import com.samsung.retailexperience.specapp.R
import com.samsung.retailexperience.specapp.data.model.PopularFeatureModel
import com.samsung.retailexperience.specapp.views.events.PageSelectedEvent
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.zhpan.bannerview.BannerViewPager
import com.zhpan.bannerview.constants.PageStyle.MULTI_PAGE_OVERLAP
import java.io.IOException
import java.lang.reflect.Type

class PopularFeaturesPager(context: Context) : FrameLayout(context) {

    companion object {
        private val TAG = PopularFeaturesPager::class.java.simpleName
    }

    private var mEventDispatcher: EventDispatcher = (context as ReactContext).getNativeModule(UIManagerModule::class.java).eventDispatcher
    private val mViewPager: BannerViewPager<PopularFeatureModel, PopularFeaturesViewHolder>
    private val mPopularFeaturesAdapter: PopularFeaturesAdapter
    private var mPageMargin = resources.getDimensionPixelOffset(R.dimen.pageMargin).toFloat()
    private var mPeekOffset = resources.getDimensionPixelOffset(R.dimen.offset).toFloat()
    private var mPageMarginAndOffset = resources.getDimensionPixelOffset(R.dimen.pageMarginAndOffset).toFloat()
    private var mPageMarginAndOffsetVertical = resources.getDimensionPixelOffset(R.dimen.pageMarginAndOffsetVertical).toFloat()
    private var mInitialPage = 0

    init {

        val inflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.popular_features_pager_container, this, true)
        mViewPager = findViewById(R.id.view_pager)
        mPopularFeaturesAdapter = PopularFeaturesAdapter()
        updatePager()
    }

    fun setPageMargin(pageMargin: Float) {
        mPageMargin = pageMargin
    }

    fun setPeekOffset(peekOffset: Float) {
        mPeekOffset = peekOffset
    }

    fun setPageMarginAndOffset(pageMarginAndOffset: Float) {
        mPageMarginAndOffset = pageMarginAndOffset
//        mPopularFeaturesAdapter.setPageMarginAndOffset(pageMarginAndOffset)
    }

    fun setPageMarginAndOffsetVertical(pageMarginAndOffsetVertical: Float) {
        mPageMarginAndOffsetVertical = pageMarginAndOffsetVertical
//        mPopularFeaturesAdapter.setPageMarginAndOffsetVertical(pageMarginAndOffsetVertical)
    }

    fun setInitialPage(pagePosition: Int) {
        mInitialPage = pagePosition
    }

    fun setDataList(dataList: ReadableArray) {
        try {
            val moshi = Moshi.Builder().build()
            val type: Type = Types.newParameterizedType(
                MutableList::class.java,
                PopularFeatureModel::class.java
            )
            val adapter = moshi.adapter<List<PopularFeatureModel>>(type)
            val dataListModels: List<PopularFeatureModel>? = adapter.fromJson(dataList.toString())
            dataListModels?.let {
                mViewPager.refreshData(it)
                mViewPager.setCurrentItem(mInitialPage, false)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
    private fun updatePager() {
        with(mViewPager) {
            adapter = mPopularFeaturesAdapter
//            addPageTransformer(PopularFeaturesTransformer())
            setIndicatorMargin(0, context.resources.getDimensionPixelSize(R.dimen.pageIndicatorMarginTop), 0, 0)
            setIndicatorSliderColor(context.getColor(R.color.pageIndicatorNormalColor), context.getColor(R.color.pageIndicatorSelectedColor))
            setIndicatorSliderRadius(context.resources.getDimensionPixelSize(R.dimen.pageIndicatorRadius))
            setPageStyle(MULTI_PAGE_OVERLAP)
            setRevealWidth(resources.getDimensionPixelSize(R.dimen.revealWidth))
//            setCanLoop(false)
            setAutoPlay(false)
            registerOnPageChangeCallback(object: OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    mEventDispatcher.dispatchEvent(
                        PageSelectedEvent(
                            this@PopularFeaturesPager.id,
                            position
                        )
                    )
                }
            })
            create()
        }
    }
}