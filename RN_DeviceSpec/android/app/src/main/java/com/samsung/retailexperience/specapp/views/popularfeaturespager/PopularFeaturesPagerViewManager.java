package com.samsung.retailexperience.specapp.views.popularfeaturespager;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.samsung.retailexperience.specapp.views.events.PageSelectedEvent;

import java.util.Map;

public class PopularFeaturesPagerViewManager extends SimpleViewManager<PopularFeaturesPager> {

    public static final String REACT_COMPONENT_NAME = PopularFeaturesPager.class.getSimpleName();

    @NonNull
    @Override
    public String getName() {
        return REACT_COMPONENT_NAME;
    }

    @NonNull
    @Override
    protected PopularFeaturesPager createViewInstance(@NonNull ThemedReactContext reactContext) {
        return new PopularFeaturesPager(reactContext);
    }

    @Override
    public Map getExportedCustomDirectEventTypeConstants() {
        return MapBuilder.of(
                PageSelectedEvent.EVENT_NAME, MapBuilder.of("registrationName", "onPageSelected")
        );
    }

    @ReactProp(name = "pageMargin")
    public void setPageMargin(PopularFeaturesPager view, Integer pageMargin) {
        view.setPageMargin(pageMargin);
    }

    @ReactProp(name = "peekOffset")
    public void setPeekOffset(PopularFeaturesPager view, Integer offset) {
        view.setPeekOffset(offset);
    }

    @ReactProp(name = "pageMarginAndOffset")
    public void setPageMarginAndOffset(PopularFeaturesPager view, Integer pageMarginAndOffset) {
        view.setPageMarginAndOffset(pageMarginAndOffset);
    }

    @ReactProp(name = "pageMarginAndOffsetVertical")
    public void setPageMarginAndOffsetVertical(PopularFeaturesPager view, Integer pageMarginAndOffsetVertical) {
        view.setPageMarginAndOffsetVertical(pageMarginAndOffsetVertical);
    }

    @ReactProp(name = "initialPage")
    public void setInitialPage(PopularFeaturesPager view, Integer pagePosition) {
        view.setInitialPage(pagePosition);
    }

    @ReactProp(name = "dataList")
    public void setDataList(PopularFeaturesPager view, ReadableArray dataList) {
       view.setDataList(dataList);
    }
}
