package com.samsung.retailexperience.specapp.views.popularfeaturespager;

import android.view.View;
import android.widget.ImageView;

import com.samsung.retailexperience.specapp.R;
import com.samsung.retailexperience.specapp.data.model.PopularFeatureModel;
import com.samsung.retailexperience.specapp.views.customviews.CustomFontTextView;
import com.squareup.picasso.Picasso;
import com.tecace.retail.res.Res;
import com.zhpan.bannerview.BaseViewHolder;

import java.io.File;

import androidx.annotation.NonNull;

public class PopularFeaturesViewHolder extends BaseViewHolder<PopularFeatureModel> {

    private View mItemView;

    public PopularFeaturesViewHolder(@NonNull View itemView) {
        super(itemView);
        mItemView = itemView;
    }

    @Override
    public void bindData(PopularFeatureModel data, int position, int pageSize) {
        ImageView image = findView(R.id.image);
        CustomFontTextView title = findView(R.id.tv_title);
        CustomFontTextView subtitle = findView(R.id.tv_subtitle);
        CustomFontTextView desc = findView(R.id.tv_description);

        Picasso.get().load(new File(data.getImage())).fit().centerInside().into(image);
        title.setText(data.getTitle());
        subtitle.setText(data.getSubtitle());
        desc.setText(data.getDescription());
    }
}
