import 'react-native-gesture-handler';
import React, {useState, useEffect, useContext} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {StringsContextProvider} from '@contexts/StringsContext';
import {ThemeContextProvider} from '@contexts/ThemeContext';
import {OrientationContextProvider} from '@contexts/OrientationContext';
import ScreenModule from '@modules/ScreenModule';
import NfcModule from '@modules/NfcModule';
import HomeScreen from '@homescreen';
import ChooseDeviceScreen from '@choosedevice';
import DeviceComparison from '@devicecomparison';
import FullSpecScreen from '@fullspec';
import {SCREEN_NAMES} from '@utils/Constants';
import {ThemeContext} from '@contexts/ThemeContext';

const Stack = createStackNavigator();

ScreenModule.setFullScreen(true);

const _renderNavigation = () => {
  const {theme, DefaultTheme} = useContext(ThemeContext);
  const [initialRoute, setInitialRoute] = useState(SCREEN_NAMES.HOME);

  useEffect(() => {
    NfcModule.getNFCVal(value => {
      if (value !== '') {
        setInitialRoute(value);
      }
    });
  }, [initialRoute]);

  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={initialRoute}
        screenOptions={{
          headerShown: false,
          cardStyle: {
            backgroundColor: DefaultTheme[theme].cardColor,
          },
        }}>
        <Stack.Screen name={SCREEN_NAMES.HOME} component={HomeScreen} />
        <Stack.Screen
          name={SCREEN_NAMES.DEVICE_COMPARISON}
          component={DeviceComparison}
        />
        <Stack.Screen
          name={SCREEN_NAMES.FULL_SPEC}
          component={FullSpecScreen}
        />
        <Stack.Screen
          name={SCREEN_NAMES.CHOOSE_DEVICE}
          component={ChooseDeviceScreen}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default function App() {
  return (
    <OrientationContextProvider>
      <StringsContextProvider>
        <ThemeContextProvider>
          <_renderNavigation />
        </ThemeContextProvider>
      </StringsContextProvider>
    </OrientationContextProvider>
  );
}
