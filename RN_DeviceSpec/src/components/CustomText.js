import React from 'react';
import {Text} from 'react-native';

export default function CustomText({data, fontColor, fontSize, lineHeight}) {
  const color = fontColor ? fontColor : data.color;
  const size = fontSize ? fontSize : data.size;

  return (
    <Text
      style={{
        fontFamily: data.font,
        color: color,
        fontSize: size,
        lineHeight: lineHeight,
      }}>
      {data.string}
    </Text>
  );
}
