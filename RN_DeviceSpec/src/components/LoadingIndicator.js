import React from 'react';
import { View, ActivityIndicator, StyleSheet } from 'react-native';


export default LoadingIndicator = ({ color }) => {

    const defaultColor = "#0000f"

    return (
        <View style={styles.loadingContainer}>
            <ActivityIndicator size="large" color={color ? color : defaultColor} />
        </View>
    );
}

const styles = StyleSheet.create({
    loadingContainer: {
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center'
    }
});