import React from 'react';
import {StyleSheet, TouchableOpacityBase} from 'react-native';
import JsonModule from '../../modules/JsonModule';
import {from, of, Subscription, Observable} from 'rxjs';
import {filter, flatMap, delay} from 'rxjs/operators';

//List of chapters
//List of active chapters
//List of inactive chapters
//Add/remove chapters
//Fire chapter start/end
//Update chapter events

class ChapterManager {
  constructor() {
    this.TAG = 'ChapterManager';
    this.chapterUri;
    this.chapterList = [];
    this.observableChapterList;
    this.loadedChapters;
    this.activeChapterList = [];
    this.inactiveChapterList = [];
    this.activeChapter;
    this.inactiveChapter;
    this.subscriptions = new Subscription();
    this.onChapterStartCallback;
    this.onChapterEndCallback;
  }

  setOnChapterStartCallback(callback) {
    this.onChapterStartCallback = callback;
  }

  setOnChapterEndCallback(callback) {
    this.onChapterEndCallback = callback;
  }

  fetchChapters(chapterUri, callback, forcedStart, eventTime) {
    if (this.chapterList.length > 0) {
      return;
    }

    JsonModule.getJsonModelText(
      chapterUri,
      value => {
        this.chapterUri = chapterUri;
        const jsonObject = JSON.parse(value);
        this.chapterList = jsonObject;
        this.observableChapterList = from(this.chapterList);
        this.updateChapterEvents(0);
        this.loadedChapters = this.chapterList;
        if (forcedStart) {
          this.reset();
          this.updateChapterEvents(eventTime);
        }
        callback(this.loadedChapters.length);
      },
      err => {
        console.log(err);
        console.log(
          this.TAG + ': Unable to load chapter with path: ' + chapterUri,
        );
      },
    );
  }

  _addChapterStartEvents(currentTimeMilli) {
    if (!this.observableChapterList) {
      console.log(this.TAG + ':_addChapterStartEvents() chapter list is null ');
      return null;
    } else {
      return this.observableChapterList
        .pipe(
          filter(chapter => {
            // console.log(this.TAG + ': currentTimeMilli ' + currentTimeMilli + ' - chapter.start ' + chapter.start + ' - chapter end ' + chapter.end);
            return (
              chapter.start >= currentTimeMilli ||
              (chapter.start < currentTimeMilli &&
                chapter.end > currentTimeMilli)
            );
          }),
          flatMap(chapter => {
            const startDelay = chapter.start - currentTimeMilli;
            if (startDelay <= 0) {
              return of(chapter);
            } else {
              return of(chapter).pipe(delay(startDelay));
            }
          }),
        )
        .subscribe(chapter => {
          this.activeChapter = chapter;
          this.activeChapterList.push(chapter);
          this.onChapterStartCallback(chapter);
          console.log(this.TAG + ': active chapter: ' + chapter.start);
        });
    }
  }

  _addChapterEndEvents(currentTimeMilli) {
    if (!this.observableChapterList) {
      console.log(this.TAG + ':_addChapterEndEvents() chapter list is null ');
      return null;
    } else {
      return this.observableChapterList
        .pipe(
          filter(chapter => {
            return chapter.end >= currentTimeMilli;
          }),
          flatMap(chapter => {
            return of(chapter).pipe(delay(chapter.end - currentTimeMilli));
          }),
        )
        .subscribe(chapter => {
          this.inactiveChapter = chapter;
          const index = this.activeChapterList.indexOf(chapter);
          if (index >= 0) {
            this.activeChapterList.splice(index, 1);
          }
          this.onChapterEndCallback(chapter);
          console.log(this.TAG + ': inactive chapter: ' + chapter.end);
        });
    }
  }

  updateChapterEvents(currentTimeMilli) {
    console.log(this.TAG + ': update chapter events at ' + currentTimeMilli);
    // if (!this.observableChapterList) {
    //   console.log(this.TAG + ': updateChapterEvents() observableChapterList is empty, return');
    //   return;
    // }
    this.subscriptions.unsubscribe();
    this.subscriptions = new Subscription();
    const chapterStartEventSubscriptions = this._addChapterStartEvents(
      currentTimeMilli,
    );
    const chapterEndEventSubscriptions = this._addChapterEndEvents(
      currentTimeMilli,
    );
    if (chapterStartEventSubscriptions) {
      this.subscriptions.add(chapterStartEventSubscriptions);
    }
    if (chapterEndEventSubscriptions) {
      this.subscriptions.add(chapterEndEventSubscriptions);
    }
  }

  reset() {
    this.clearCurrentChapters();
    this.clearEvents();
  }

  clearCurrentChapters() {
    for (let i = 0; i < this.activeChapterList.length; i++) {
      this.inactiveChapter = this.activeChapterList[i];
    }
    this.activeChapterList.splice(0, this.activeChapterList.length);
  }

  clearEvents() {
    this.activeChapter = null;
    this.inactiveChapter = null;
    this.subscriptions.unsubscribe();
  }

  getActiveChapter() {
    return this.activeChapter;
  }

  getInactiveChapter() {
    return this.inactiveChapter;
  }

  getChapterListLength() {
    return this.chapterList.length;
  }

  printList(list) {
    console.log(this.TAG + ': Chapter model:\n');
    for (let i = 0; i < list.length; i++) {
      console.log(this.TAG + ': Chapter at ' + i + ': ' + list[i].start);
      console.log(this.TAG + ': Chapter at ' + i + ': ' + list[i].end);
    }
  }
}

export default ChapterManager;
