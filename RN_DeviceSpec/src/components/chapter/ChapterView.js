import React, {useContext, useEffect, useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {StringsContext} from '../../contexts/StringsContext';
import {OrientationContext} from '../../contexts/OrientationContext';
import CustomText from '../../components/CustomText';

export default function ChapterView(props) {
  const {orientation} = useContext(OrientationContext);
  const {getString} = useContext(StringsContext);

  const chapterView = props.chapter.chapterView;
  if (chapterView.texts.length > 0) {
    switch (props.chapter.actionMessage) {
      case 'disclaimer_top':
        return (
          <View style={styles.topDisclaimer}>
            <CustomText data={getString(chapterView.texts[0].text)} />
          </View>
        );
      case 'disclaimer_mid':
        return (
          <View style={styles.midDisclaimer}>
            <CustomText data={getString(chapterView.texts[0].text)} />
          </View>
        );
      case 'disclaimer_bottom':
        return (
          <View style={styles.bottomDisclaimer}>
            <CustomText data={getString(chapterView.texts[0].text)} />
          </View>
        );
      case 'initial_title':
        return (
          <View style={styles.initialTitle}>
            <CustomText data={getString(chapterView.texts[0].text)} />
          </View>
        );
      case 'title':
        return (
          <View style={styles.title}>
            <CustomText data={getString(chapterView.texts[0].text)} />
          </View>
        );
      case 'subtitle':
        return (
          <View style={styles.subtitle}>
            <CustomText data={getString(chapterView.texts[0].text)} />
          </View>
        );
      default:
        return <View />;
    }
  }
}

const styles = StyleSheet.create({
  topDisclaimer: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
    marginBottom: 30,
  },
  midDisclaimer: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
    marginBottom: 25,
  },
  bottomDisclaimer: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
    marginBottom: 20,
  },
  initialTitle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  subtitle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 30,
  },
});
