import React, {useEffect, useState, useReducer} from 'react';
import {View, Text, StyleSheet, BackHandler} from 'react-native';
import CustomText from '../CustomText';
import ChapterManager from './ChapterManager';
import {BehaviorSubject, of} from 'rxjs';
import {CHAPTER_ACTION} from '../../utils/Constants';
import ChapterView from './ChapterView';

/** Contains all chapter views.*/
export default function ChapterViewContainer(props) {
  const ChapterInteractionStatus = {
    Ready: 'Ready',
    Notified: 'Notified',
  };

  const TAG = 'ChapterViewContainer';
  const [isLoading, setIsLoading] = useState(true);
  const [chapterManager] = useState(new ChapterManager());
  const [chapterInteractionStatus] = useState([]);
  const [views] = useState(new Map());
  // const [viewsToBeRendered] = useState([]);
  const [rendering] = useState([]);

  const _addChapterView = function(chapter) {
    if (
      chapter.action !== CHAPTER_ACTION.Custom &&
      chapter.action !== CHAPTER_ACTION.Unknown
    ) {
      console.log(
        TAG +
          ': adding chapter view for: ' +
          chapter.id +
          ' ' +
          chapter.start +
          ' ' +
          chapter.end,
      );
      const jsx = (
        <View key={chapter.id}>
          <ChapterView chapter={chapter} />
        </View>
      );
      views.set(chapter.id, jsx);
      rendering.push({id: chapter.id, jsx: jsx});
      // viewsToBeRendered.push({id: chapter.id, jsx: jsx});
      // for (let i = 0; i < viewsToBeRendered.length; i++) {
      //   console.log(TAG + ': _addChapterView: ' + viewsToBeRendered[i].id);
      //   console.log(TAG + ': _addChapterView: ' + viewsToBeRendered[i].jsx);
      // }
    }
  };

  const _removeViewFromRenderArray = function(id) {
    let index = 0;
    for (let i = 0; i < rendering.length; i++) {
      if (rendering[i].id === id) {
        index = i;
        console.log(TAG + ': jsx found at ' + index);
        break;
      }
    }
    rendering.splice(index, 1);
  };

  const _removeChapterView = function(chapter) {
    if (
      chapter.action !== CHAPTER_ACTION.Custom &&
      chapter.action !== CHAPTER_ACTION.Unknown
    ) {
      console.log(
        TAG +
          ': removing chapter view for: ' +
          chapter.id +
          ' ' +
          chapter.start +
          ' ' +
          chapter.end,
      );
      views.delete(chapter.id);
      _removeViewFromRenderArray(chapter.id);
      // for (let i = 0; i < viewsToBeRendered.length; i++) {
      //   console.log(TAG + ': _removeChapterView: ' + viewsToBeRendered[i].id);
      //   console.log(TAG + ': _removeChapterView: ' + viewsToBeRendered[i].jsx);
      // }
    }
  };

  const _onChapterStart = function(chapter) {
    if (chapter) {
      console.log(TAG + ': _onChapterStart: ' + chapter.start);
      _addChapterView(chapter);
      props.onChapterStart(chapter);
      // const iterator = views.values();
      // let result = iterator.next;
      // while (!result.done) {
      //   console.log(TAG + ': value in map: ' + result.value);
      //   result = iterator.next();
      // }
    }
  };

  const _onChapterEnd = function(chapter) {
    if (chapter) {
      console.log(TAG + ': _onChapterEnd: ' + chapter.end);
      _removeChapterView(chapter);
      props.onChapterEnd(chapter);
      // const iterator = views.values();
      // let result = iterator.next;
      // while (!result.done) {
      //   console.log(TAG + ': value in map: ' + result.value);
      //   result = iterator.next();
      // }
    }
  };

  chapterManager.setOnChapterStartCallback(_onChapterStart);
  chapterManager.setOnChapterEndCallback(_onChapterEnd);

  useEffect(() => {
    chapterManager.fetchChapters(props.chapterUri, size => {
      //Empty array
      chapterInteractionStatus.splice(0, chapterInteractionStatus.length);
      for (let i = 0; i < size; i++) {
        chapterInteractionStatus.push(ChapterInteractionStatus.Ready);
      }
    });
  }, [
    ChapterInteractionStatus.Ready,
    chapterInteractionStatus,
    chapterManager,
    props,
    props.chapterUri,
  ]);

  useEffect(() => {
    chapterManager.updateChapterEvents(props.currentTimeMilli);
  }, [chapterManager, props.currentTimeMilli, props.videoPaused]);

  useEffect(() => {
    of(props.videoPaused).subscribe(isVideoPause => {
      if (isVideoPause) {
        console.log(TAG + ': video paused.');
        chapterManager.reset();
      }
    });
  }, [
    chapterManager,
    props.currentTimeMilli,
    props.progress,
    props.videoPaused,
  ]);

  useEffect(() => {
    const backAction = () => {
      console.log(TAG + ': back button clicked.');
      chapterManager.reset();
      return false;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );
    return () => backHandler.remove();
  }, [chapterManager]);

  // const iterator = views.values();
  // let result = iterator.next;
  // while (!result.done) {
  //   if (result.value) {
  //     viewsToBeRendered.push(result.value);
  //   }
  //   result = iterator.next();
  // }
  // console.log(TAG + ': viewToBeRendered: ' + viewsToBeRendered);

  // const _render = function() {
  //   const rendering = [];
  //   for (let i = 0; i < viewsToBeRendered.length; i++) {
  //     rendering.push(viewsToBeRendered[i].jsx);
  //   }
  //   // let result = iterator.next;
  //   // while (!result.done) {
  //   //   if (result.value) {
  //   //     viewsToBeRendered.push(result.value);
  //   //   }
  //   //   result = iterator.next();
  //   // }
  //   for (let i = 0; i < rendering.length; i++) {
  //     console.log(TAG + ': rendering: ' + rendering[i].id);
  //     console.log(TAG + ': rendering: ' + rendering[i].jsx);
  //   }
  //   return rendering;
  // };

  // const _render = function() {
  //   views.forEach((val, key) => {
  //     return <View key={key}>{val}</View>
  //   });
  // };

  // const iterator = views.values();
  // let result = iterator.next;
  // while (!result.done) {
  //   if (result.value) {
  //     rendering.push(result.value);
  //   }
  //   result = iterator.next();
  // }
  // useEffect(() => {
  //   return () => rendering.splice(0, rendering.length);
  // }, [rendering]);

  return (
    <View>
      {rendering.map((val, index) => {
        console.log(`${TAG}: JSX: ${val.jsx}`);
        return <View key={index}>{val.jsx}</View>;
      })}
    </View>
  );
}
