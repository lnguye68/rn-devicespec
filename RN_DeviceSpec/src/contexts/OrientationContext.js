import React, {createContext, useState, useEffect} from 'react';
import Orientation from 'react-native-orientation';
import AnalyticsModule from '../modules/AnalyticsModule';
import {Dimensions} from 'react-native';

const OrientationContext = createContext();

/**
 * Returns true if the screen is in portrait mode
 */
const isPortrait = () => {
  const dim = Dimensions.get('screen');
  return dim.height >= dim.width;
};

/**
 * Returns true of the screen is in landscape mode
 */
const isLandscape = () => {
  const dim = Dimensions.get('screen');
  return dim.width >= dim.height;
};

const OrientationContextProvider = ({children}) => {
  const initialOrientation = Orientation.getInitialOrientation();
  const [orientation, setOrientation] = useState(initialOrientation);

  // Event Listener for orientation changes
  Dimensions.addEventListener('change', () => {
    isPortrait() ? setOrientation('PORTRAIT') : setOrientation('LANDSCAPE');
  });

  // useEffect(() => {
  //   Orientation.addOrientationListener(_handleOrientationChange);

  //   return () => {
  //     Orientation.removeOrientationListener(_handleOrientationChange);
  //   };
  // }, [orientation]);

  // function _handleOrientationChange(orientation) {
  //   AnalyticsModule.sendOrientationEvent();
  //   setOrientation(orientation);
  // }

  return (
    <OrientationContext.Provider value={{orientation}}>
      {children}
    </OrientationContext.Provider>
  );
};

export {OrientationContext, OrientationContextProvider};
