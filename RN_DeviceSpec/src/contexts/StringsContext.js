import React, {useState, createContext, useEffect} from 'react';
import {View, Text} from 'react-native';
import Orientation from 'react-native-orientation';
import * as constants from '@utils/Constants';
import FileUtil from '@utils/FileUtil';

const StringsContext = createContext();

const StringsContextProvider = ({children}) => {
  const [defaultStrings, setDefaultStrings] = useState({});
  const [defaultLandStrings, setDefaultLandStrings] = useState({});
  // NOTE: localizedStrings already handled orientation.
  const [localizedStrings, setLocalizedStrings] = useState({});
  // NOTE: defaultLocalizedStrings ignore orientation, always return the default from localized folder.
  const [defaultLocalizedStrings, setDefaultLocalizedStrings] = useState({});
  const [forceUpdateStrings, setForceUpdateStrings] = useState(true);
  const initialOrientation = Orientation.getInitialOrientation();
  const [orientation, setOrientation] = useState(initialOrientation);

  // reload strings if orientation has been changed.
  useEffect(() => {
    Orientation.addOrientationListener(_handleOrientationChange);

    return () => {
      Orientation.removeOrientationListener(_handleOrientationChange);
    };
  }, [orientation]);

  // load strings
  useEffect(() => {
    if (!forceUpdateStrings) {
      return;
    }

    async function _updateStringsStates() {
      try {
        const _defaultStrings = await FileUtil.readFile(
          constants.STRING_FILE_NAME,
        );
        const _defaultStringsLand = await FileUtil.readFile(
          constants.STRING_FILE_NAME_LAND,
        );
        const _localizedStrings = await FileUtil.readFileWithLocale(
          constants.STRING_FILE_NAME,
        );
        const _localizedStringsDefault = await FileUtil.readFileWithLocale(
          constants.STRING_FILE_NAME,
          true,
        );

        setDefaultStrings(_getParsedJson(_defaultStrings));
        setDefaultLandStrings(_getParsedJson(_defaultStringsLand));
        setLocalizedStrings(_getParsedJson(_localizedStrings));
        setDefaultLocalizedStrings(_getParsedJson(_localizedStringsDefault));
      } catch (e) {
        console.log(e);
      }
      setForceUpdateStrings(false);
    }
    _updateStringsStates();
  }, [forceUpdateStrings]);

  /*
   * Get a string based on the provided key.
   * If the string is not found from the localized strings.
   * It will look into the default strings which is located
   * in android/app/src/main/assets/strings.json
   */
  const getString = key => {
    const errMessage = {string: key + ' is not found.'};
    if (localizedStrings || defaultLocalizedStrings) {
      let string = localizedStrings[key];
      let returnString = string ? string : defaultLocalizedStrings[key];
      return returnString ? returnString : _getDefaultString(key);
    } else {
      return errMessage;
    }
  };

  function _getDefaultString(key) {
    const errMessage = {'default string': key + ' is not found.'};
    if (defaultStrings || defaultLandStrings) {
      let string = '';
      if (
        orientation === 'PORTRAIT' ||
        orientation === 'PORTRAITUPSIDEDOWN' ||
        orientation === 'UNKNOWN'
      ) {
        string = defaultStrings[key];
      } else {
        string = defaultLandStrings[key]
          ? defaultLandStrings[key]
          : defaultStrings[key];
      }
      return string ? string : errMessage;
    } else {
      return errMessage;
    }
  }

  function _getParsedJson(value) {
    return value ? JSON.parse(value) : '';
  }

  function _handleOrientationChange(orientation) {
    console.log(
      'StringContext.js --- Reloading strings. Orientation has been changed to ' +
        orientation,
    );
    setOrientation(orientation);
    setForceUpdateStrings(true);
  }

  if (Object.keys(localizedStrings).length === 0) {
    // Render a loading message if strings is null or empty.
    return (
      // <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      //   <Text>{constants.MSG_LOADING_STRINGS}</Text>
      // </View>
      <View />
    );
  } else {
    // only render if strings is not null or empty.
    return (
      <StringsContext.Provider value={{getString, setForceUpdateStrings}}>
        {children}
      </StringsContext.Provider>
    );
  }
};

export {StringsContext, StringsContextProvider};
