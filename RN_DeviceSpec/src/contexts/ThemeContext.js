import React, {createContext, useState} from 'react';
import AnalyticsModule from '@modules/AnalyticsModule';

const ThemeContext = createContext(null);
const DefaultTheme = {
  light: {
    backgroundColor: '#ffffff',
    fontColor: '#000000',
    cardColor: '#ffffff',
    expandAllTextFontColor: '#1428a0',
  },
  dark: {
    backgroundColor: '#282727',
    fontColor: '#ffffff',
    cardColor: '#434242',
    expandAllTextFontColor: '#ffffff',
  },
};

const ThemeContextProvider = ({children}) => {
  const [theme, setTheme] = useState('light');

  const toggleTheme = () => {
    if (theme === 'light') {
      AnalyticsModule.sendUserEvent(
        AnalyticsModule.TAP,
        'tap_enable_dark_theme',
      );
    } else {
      AnalyticsModule.sendUserEvent(
        AnalyticsModule.TAP,
        'tap_enable_light_theme',
      );
    }

    const newTheme = theme === 'light' ? 'dark' : 'light';
    setTheme(newTheme);
  };

  return (
    <ThemeContext.Provider value={{DefaultTheme, theme, toggleTheme}}>
      {children}
    </ThemeContext.Provider>
  );
};

export {ThemeContext, ThemeContextProvider};
