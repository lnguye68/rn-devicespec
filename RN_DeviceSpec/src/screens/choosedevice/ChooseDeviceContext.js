import React, {createContext, useState, useEffect} from 'react';
import JsonModule from '@modules/JsonModule'

export const ChooseDeviceContext = createContext();

export const ChooseDeviceProvider = ({children}) => {
  const [devices, setDevices] = useState([]);
  const [model, setModels] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  async function _fetchDevices() {
    setIsLoading(true);
    try {
      const data = await JsonModule.getJsonModelTextAsync('model/choosedevice/choose_device.json');
      setDevices(JSON.parse(data));
    } catch (e) {
      console.log(e);
    }
    setIsLoading(false);
  }

  useEffect(() => {
    _fetchDevices();
  }, [])

  return (
    <ChooseDeviceContext.Provider value={{devices, isLoading}}>
      {children}
    </ChooseDeviceContext.Provider>
  );
}
