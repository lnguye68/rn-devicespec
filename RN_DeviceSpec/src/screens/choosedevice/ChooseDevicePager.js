import React from 'react';
import {requireNativeComponent} from 'react-native';
import PropTypes from 'prop-types';

class ChooseDevicePager extends React.Component {
  constructor(props) {
    super(props);
    this._onPageSelected = this._onPageSelected.bind(this);
    this._onPageClicked = this._onPageClicked.bind(this);
  }
  _onPageSelected(event) {
    if (!this.props.onPageSelected) {
      return;
    }
    const position = event.nativeEvent.position;
    this.props.onPageSelected(position);
  }
  _onPageClicked(event) {
    if (!this.props.onPageClicked) {
      return;
    }
    const position = event.nativeEvent.position;
    this.props.onPageClicked(position);
  }
  render() {
    return <NativeChooseDevicePager {...this.props}
        dataList={this.props.dataList}
        initialPage={this.props.initialPage}
        pageMargin={this.props.pageMargin}
        peekOffset={this.props.peekOffset}
        pageMarginAndOffset={this.props.pageMarginAndOffset}
        pageMarginAndOffsetVertical={this.props.pageMarginAndOffsetVertical}
        fontColor={this.props.fontColor}
        nameSize={this.props.nameSize}
        numSize={this.props.numSize}
        padding={this.props.padding}
        onPageSelected={this._onPageSelected}
        onPageClicked={this._onPageClicked}
      />;
  }
}

ChooseDevicePager.propTypes = {
  pageMargin: PropTypes.number,
  peekOffset: PropTypes.number,
  pageMarginAndOffset: PropTypes.number,
  pageMarginAndOffsetVertical: PropTypes.number,
  initialPage: PropTypes.number,
  onPageSelected: PropTypes.func,
  onPageClicked: PropTypes.func,
  fontColor: PropTypes.string,
  nameSize: PropTypes.number,
  numSize: PropTypes.number,
  padding: PropTypes.number,
  dataList: PropTypes.array,
}

var NativeChooseDevicePager = requireNativeComponent('ChooseDevicePager', ChooseDevicePager);

export default ChooseDevicePager;
