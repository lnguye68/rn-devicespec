import React, {useState, useContext, useRef, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  PixelRatio,
  FlatList,
  BackHandler,
  TouchableWithoutFeedback,
} from 'react-native';
import {OrientationContext} from '@contexts/OrientationContext';
import {StringsContext} from '@contexts/StringsContext';
import {ChooseDeviceProvider, ChooseDeviceContext} from './ChooseDeviceContext';
import {ThemeContext} from '@contexts/ThemeContext';
import ChooseDevicePager from './ChooseDevicePager';
import LoadingIndicator from '@components/LoadingIndicator';
import ShadowView from 'react-native-simple-shadow-view';
import CustomText from '@components/CustomText';
import FastImage from 'react-native-fast-image';
import LegalScreen from '@screens/legal/LegalScreen';
import AnalyticsModule from '@modules/AnalyticsModule';
import {BorderlessButton} from 'react-native-gesture-handler';
import {isTablet} from 'react-native-device-info';
import {useNavigation, useIsFocused, useRoute} from '@react-navigation/native';
import {SCREEN_NAMES} from '@utils/Constants';

const DATA = [
  {
    id: '1',
    title: 'choose_device_brand_2',
  },
  {
    id: '2',
    title: 'choose_device_brand_3',
  },
  {
    id: '3',
    title: 'choose_device_brand_4',
  },
  {
    id: '4',
    title: 'choose_device_brand_5',
  },
];

const windowWidth =
  Dimensions.get('window').width < Dimensions.get('window').height
    ? Dimensions.get('window').width
    : Dimensions.get('window').height;
const windowHeight =
  Dimensions.get('window').height > Dimensions.get('window').width
    ? Dimensions.get('window').height
    : Dimensions.get('window').width;

const cornerBorder = isTablet() ? 15 : 15;

const _renderViewPager = () => {
  const context = useContext(ChooseDeviceContext);
  const {orientation} = useContext(OrientationContext);
  const {getString} = useContext(StringsContext);
  const {theme, DefaultTheme} = useContext(ThemeContext);
  const [position, setPosition] = useState(-1);
  const navigation = useNavigation();

  let pageMargin;
  let peekOffset;
  let pageMarginAndOffset;
  let pageMarginAndOffsetVertical;

  const _clickPosition = function(position) {
    setPosition(position);
    console.log(position);

    AnalyticsModule.sendScreenChangeEvent(
      AnalyticsModule.TAP,
      SCREEN_NAMES.CHOOSE_DEVICE /* current screen name */,
      SCREEN_NAMES.DEVICE_COMPARISON /* next screen name */,
    );

    navigation.navigate(SCREEN_NAMES.DEVICE_COMPARISON, {
      prevScreen: SCREEN_NAMES.CHOOSE_DEVICE,
    });
  };

  const _pageSelected = function(newPosition) {
    if (position < 0) {
      setPosition(newPosition)
    } else {
      setPosition(newPosition)
      newPosition += 1;
      AnalyticsModule.sendUserEvent(
        AnalyticsModule.SWIPE,
        'choose_device_carousel_page_' + newPosition + '_selected',
      );
    }
  };

  if (orientation === 'PORTRAIT') {
    pageMargin = PixelRatio.getPixelSizeForLayoutSize(
      isTablet() ? 0 : windowWidth * 0.15,
    );
    peekOffset = PixelRatio.getPixelSizeForLayoutSize(isTablet() ? 185 : 70);
    pageMarginAndOffset = PixelRatio.getPixelSizeForLayoutSize(
      isTablet() ? 85 : 25,
    );
    pageMarginAndOffsetVertical = PixelRatio.getPixelSizeForLayoutSize(15);
  } else {
    pageMargin = PixelRatio.getPixelSizeForLayoutSize(
      isTablet() ? 0 : windowHeight * 0.15,
    );
    peekOffset = PixelRatio.getPixelSizeForLayoutSize(isTablet() ? 230 : 80);
    pageMarginAndOffset = PixelRatio.getPixelSizeForLayoutSize(
      isTablet() ? 160 : 115,
    );
    pageMarginAndOffsetVertical = PixelRatio.getPixelSizeForLayoutSize(15);
  }
  return (
    <>
      {context.isLoading ? (
        <LoadingIndicator color="red" />
      ) : (
        <ShadowView
          style={[
            orientation === 'PORTRAIT'
              ? styles.shadowViewPager
              : stylesLand.shadowViewPagerLand,
            {backgroundColor: DefaultTheme[theme].cardColor},
          ]}>
          <View style={styles.pagerTitle}>
            <CustomText
              data={
                isTablet()
                  ? getString('')
                  : getString('choose_device_pager_title')
              }
              fontColor={DefaultTheme[theme].fontColor}
            />
          </View>
          <ChooseDevicePager
            style={isTablet() ? {flex: 0.9} : {flex: 1}}
            dataList={context.devices}
            pageMargin={pageMargin}
            peekOffset={peekOffset}
            pageMarginAndOffset={pageMarginAndOffset}
            pageMarginAndOffsetVertical={pageMarginAndOffsetVertical}
            initialPage={0}
            onPageSelected={_pageSelected}
            onPageClicked={_clickPosition}
            fontColor={DefaultTheme[theme].fontColor}
            nameSize={isTablet() ? 16 : 14}
            numSize={isTablet() ? 11 : 10}
            padding={isTablet() ? 55 : 3}
          />
        </ShadowView>
      )}
    </>
  );
};

const _renderSectionCards = () => {
  const {getString} = useContext(StringsContext);
  const {orientation} = useContext(OrientationContext);
  const {theme, DefaultTheme} = useContext(ThemeContext);
  const navigation = useNavigation();

  const onBrandPress = brand => {
    const brandName = getString(brand).string;
    console.log(brandName);

    AnalyticsModule.sendUserEvent(AnalyticsModule.TAP, brandName + '_selected');
  };

  const _onViewAllSpecPress = () => {
    AnalyticsModule.sendScreenChangeEvent(
      AnalyticsModule.TAP,
      SCREEN_NAMES.CHOOSE_DEVICE /* current screen name */,
      SCREEN_NAMES.FULL_SPEC /* next screen name */,
    );

    navigation.navigate(SCREEN_NAMES.FULL_SPEC, {
      prevScreen: SCREEN_NAMES.CHOOSE_DEVICE,
    });
  };

  const _onViewPopularSpecPress = () => {
    AnalyticsModule.sendScreenChangeEvent(
      AnalyticsModule.TAP,
      SCREEN_NAMES.CHOOSE_DEVICE /* current screen name */,
      SCREEN_NAMES.HOME /* next screen name */,
    );

    navigation.navigate(SCREEN_NAMES.HOME, {
      prevScreen: SCREEN_NAMES.CHOOSE_DEVICE,
    });
  };

  if (isTablet()) {
    if (orientation === 'PORTRAIT') {
      return (
        <View style={styles.sectionCards}>
          <View style={styles.allSpecsContainer}>
            <TouchableWithoutFeedback onPress={_onViewAllSpecPress}>
              <ShadowView
                style={[
                  styles.shadowViewAllSpecs,
                  {backgroundColor: DefaultTheme[theme].cardColor},
                ]}>
                <View style={{paddingStart: '7%'}}>
                  <_renderTextWithRightIcon
                    textUri={'choose_device_view_all_spec_title'}
                    iconUri={
                      theme === 'light'
                        ? 'arrow_chevron_right_black'
                        : 'arrow_chevron_right_white'
                    }
                  />
                </View>
              </ShadowView>
            </TouchableWithoutFeedback>
          </View>
          <View style={styles.popularSpecsContainer}>
            <TouchableWithoutFeedback onPress={_onViewPopularSpecPress}>
              <ShadowView
                style={[
                  styles.shadowViewPopular,
                  {backgroundColor: DefaultTheme[theme].cardColor},
                ]}>
                <FastImage
                  style={styles.leftThumbnail}
                  source={{uri: 'popular_specs_image'}}
                  resizeMode={FastImage.resizeMode.contain}
                />
                <View style={styles.viewPopularText}>
                  <_renderTextWithRightIcon
                    textUri={'choose_device_view_popular_title'}
                    iconUri={
                      theme === 'light'
                        ? 'arrow_chevron_right_black'
                        : 'arrow_chevron_right_white'
                    }
                  />
                  <CustomText
                    data={getString('choose_device_view_popular_text')}
                    fontColor={DefaultTheme[theme].fontColor}
                    fontSize={isTablet() ? 15 : null}
                  />
                </View>
              </ShadowView>
            </TouchableWithoutFeedback>
          </View>
        </View>
      );
    } else {
      return (
        <View style={stylesLand.popularSpecsContainerLand}>
          <TouchableWithoutFeedback onPress={_onViewPopularSpecPress}>
            <ShadowView
              style={[
                stylesLand.shadowViewPopularLand,
                {backgroundColor: DefaultTheme[theme].cardColor},
              ]}>
              <FastImage
                style={stylesLand.leftThumbnailLand}
                source={{uri: 'popular_specs_image'}}
                resizeMode={FastImage.resizeMode.contain}
              />
              <View style={stylesLand.viewPopularTextLand}>
                <_renderTextWithRightIcon
                  textUri={'choose_device_view_popular_title_land'}
                  iconUri={
                    theme === 'light'
                      ? 'arrow_chevron_right_black'
                      : 'arrow_chevron_right_white'
                  }
                />
                <CustomText
                  data={getString('choose_device_view_popular_text')}
                  fontColor={DefaultTheme[theme].fontColor}
                  fontSize={isTablet() ? 15 : null}
                />
              </View>
            </ShadowView>
          </TouchableWithoutFeedback>
        </View>
      );
    }
  } else {
    return (
      <View
        style={
          orientation === 'PORTRAIT'
            ? styles.sectionCards
            : stylesLand.sectionCardsLand
        }>
        <View style={styles.viewCardContainer}>
          <TouchableWithoutFeedback
            onPress={() => onBrandPress('choose_device_brand_1')}>
            <ShadowView
              style={[
                orientation === 'PORTRAIT'
                  ? styles.shadowViewCard
                  : stylesLand.shadowViewCardLand,
                {backgroundColor: DefaultTheme[theme].cardColor},
              ]}
              cornerRadius={10}>
              <CustomText
                data={getString('choose_device_brand_1')}
                fontColor={DefaultTheme[theme].fontColor}
              />
            </ShadowView>
          </TouchableWithoutFeedback>
        </View>
        <View style={styles.deviceCards}>
          <FlatList
            data={DATA}
            renderItem={({item}) => (
              <TouchableWithoutFeedback
                onPress={() => onBrandPress(item.title)}>
                <ShadowView
                  style={[
                    orientation === 'PORTRAIT'
                      ? styles.shadowViewBrand
                      : stylesLand.shadowViewBrandLand,
                    {backgroundColor: DefaultTheme[theme].cardColor},
                  ]}
                  cornerRadius={10}>
                  <CustomText
                    data={getString(item.title)}
                    fontColor={DefaultTheme[theme].fontColor}
                  />
                </ShadowView>
              </TouchableWithoutFeedback>
            )}
            keyExtractor={item => item.id}
            numColumns={2}
          />
        </View>
      </View>
    );
  }
};

const _renderTextWithRightIcon = ({textUri, iconUri, extraStyle}) => {
  const {getString} = useContext(StringsContext);
  const {theme, DefaultTheme} = useContext(ThemeContext);
  const {orientation} = useContext(OrientationContext);

  return (
    <View style={[styles.textWithRightIcon, extraStyle]}>
      <View style={orientation === 'PORTRAIT' ? {flexWrap: 'wrap'} : {flex: 1}}>
        <CustomText
          data={getString(textUri)}
          fontColor={DefaultTheme[theme].fontColor}
          fontSize={isTablet() ? 18 : null}
        />
      </View>
      <FastImage
        style={
          orientation === 'PORTRAIT'
            ? styles.chevronIcon
            : stylesLand.chevronIconLand
        }
        source={{uri: iconUri}}
        resizeMode={FastImage.resizeMode.contain}
      />
    </View>
  );
};

const _renderSectionReturn = ({onPress}) => {
  const {getString} = useContext(StringsContext);
  const {theme, DefaultTheme} = useContext(ThemeContext);
  const {orientation} = useContext(OrientationContext);

  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View
        style={
          orientation === 'PORTRAIT'
          ? styles.return
          : stylesLand.returnLand
        }>
        <FastImage
          style={styles.backIcon}
          source={{uri: theme === 'light' ? 'back_arrow' : 'back_arrow_white'}}
          resizeMode={FastImage.resizeMode.contain}
        />
        <CustomText
          data={getString('back_icon_text')}
          fontColor={DefaultTheme[theme].fontColor}
          fontSize={isTablet() ? 15 : null}
        />
      </View>
    </TouchableWithoutFeedback>
  );
};

const _renderSectionLegal = ({onPress}) => {
  const {getString} = useContext(StringsContext);
  const {theme, DefaultTheme} = useContext(ThemeContext);
  const {orientation} = useContext(OrientationContext);

  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View
        style={
          orientation === 'PORTRAIT'
            ? styles.legal
            : stylesLand.legalLand
        }>
        <CustomText
          data={getString('legal_icon_text')}
          fontColor={DefaultTheme[theme].fontColor}
          fontSize={isTablet() ? 15 : null}
        />
        <FastImage
          style={styles.plusIcon}
          source={{
            uri: theme === 'light' ? 'black_plus_icon' : 'white_plus_icon',
          }}
          resizeMode={FastImage.resizeMode.contain}
        />
      </View>
    </TouchableWithoutFeedback>
  );
};

const _renderPortrait = ({legalRef}) => {
  const {getString} = useContext(StringsContext);
  const {theme, DefaultTheme} = useContext(ThemeContext);
  const navigation = useNavigation();
  const route = useRoute();
  const {prevScreen} = route.params;

  const _goBack = () => {
    AnalyticsModule.sendScreenChangeEvent(
      AnalyticsModule.BACK_ICON_PRESS,
      SCREEN_NAMES.CHOOSE_DEVICE /* current screen name */,
      SCREEN_NAMES.HOME /* next screen name */,
    );

    navigation.navigate(SCREEN_NAMES.HOME);
  };

  const _goToLegalScreen = () => {
    AnalyticsModule.sendScreenChangeEvent(
      AnalyticsModule.TAP,
      SCREEN_NAMES.CHOOSE_DEVICE /* current screen name */,
      SCREEN_NAMES.CHOOSE_DEVICE +
        '_' +
        SCREEN_NAMES.LEGAL /* next screen name */,
    );

    legalRef.current?.open();
  };

  return (
    <View
      style={[
        styles.rootContainer,
        {backgroundColor: DefaultTheme[theme].backgroundColor},
      ]}>
      {/* section legal */}
      <LegalScreen prevScreen={SCREEN_NAMES.CHOOSE_DEVICE} ref={legalRef} />

      {/* section page title */}
      <View style={styles.sectionTitle}>
        <CustomText
          data={getString('choose_device_screen_title')}
          fontColor={DefaultTheme[theme].fontColor}
          fontSize={isTablet() ? 30 : null}
        />
      </View>

      {/* section view pager */}
      <View style={styles.sectionPager}>
        <_renderViewPager />
      </View>

      {/* section card */}
      <_renderSectionCards />

      {/* section legal */}
      <View style={styles.sectionBottom}>
        <_renderSectionReturn onPress={_goBack} />
        <_renderSectionLegal onPress={_goToLegalScreen} />
      </View>
    </View>
  );
};

const _renderLandscape = ({legalRef}) => {
  const {getString} = useContext(StringsContext);
  const {theme, DefaultTheme} = useContext(ThemeContext);
  const navigation = useNavigation();
  const route = useRoute();
  const {prevScreen} = route.params;

  const _goBack = () => {
    AnalyticsModule.sendScreenChangeEvent(
      AnalyticsModule.BACK_ICON_PRESS,
      SCREEN_NAMES.CHOOSE_DEVICE,
      SCREEN_NAMES.HOME,
    );

    navigation.navigate(SCREEN_NAMES.HOME);
  };

  const _goToLegalScreen = () => {
    AnalyticsModule.sendScreenChangeEvent(
      AnalyticsModule.TAP,
      SCREEN_NAMES.CHOOSE_DEVICE /* current screen name */,
      SCREEN_NAMES.CHOOSE_DEVICE +
        '_' +
        SCREEN_NAMES.LEGAL /* next screen name */,
    );

    legalRef.current?.open();
  };

  return (
    <View
      style={[
        stylesLand.rootContainerLand,
        {backgroundColor: DefaultTheme[theme].backgroundColor},
      ]}>
      {/* section page title */}
      <View style={stylesLand.subContainerPagerLand}>
        <View style={stylesLand.sectionTitleLand}>
          <CustomText
            data={getString('choose_device_screen_title')}
            fontColor={DefaultTheme[theme].fontColor}
            fontSize={isTablet() ? 30 : null}
          />
        </View>

        {/* section view pager */}
        <View style={stylesLand.sectionPagerLand}>
          <_renderViewPager />
        </View>

        <View style={stylesLand.sectionReturnLand}>
          <_renderSectionReturn onPress={_goBack} />
        </View>
      </View>

      <View
        style={
          isTablet()
            ? stylesLand.subContainerViewPopularLand
            : stylesLand.subContainerDevicesLand
        }>
        {/* section card */}
        <_renderSectionCards />

        {/* section legal */}
        <View style={stylesLand.sectionLegalLand}>
          <_renderSectionLegal onPress={_goToLegalScreen} />
        </View>
      </View>
    </View>
  );
};

const ChooseDeviceScreen = ({route, navigation}) => {
  const {orientation} = useContext(OrientationContext);
  const isFocused = useIsFocused();
  const {prevScreen} = route.params;
  const legalRef = useRef(null);

  useEffect(() => {
    const backAction = () => {
      AnalyticsModule.sendScreenChangeEvent(
        AnalyticsModule.BACK_KEY_PRESS,
        SCREEN_NAMES.CHOOSE_DEVICE /* current screen name */,
        prevScreen /* next screen name */,
      );
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, [prevScreen, navigation]);

  if (isFocused) {
    if (orientation === 'PORTRAIT') {
      return (
        <ChooseDeviceProvider>
          <_renderPortrait legalRef={legalRef} />
          <LegalScreen prevScreen={SCREEN_NAMES.CHOOSE_DEVICE} ref={legalRef} />
        </ChooseDeviceProvider>
      );
    } else {
      return (
        <ChooseDeviceProvider>
          <_renderLandscape legalRef={legalRef} />
          <LegalScreen prevScreen={SCREEN_NAMES.CHOOSE_DEVICE} ref={legalRef} />
        </ChooseDeviceProvider>
      );
    }
  } else {
    return <></>;
  }
};

const styles = StyleSheet.create({
  // DEFAULT/PORTRAIT STYLES
  rootContainer: {
    flex: 1,
    backgroundColor: 'white',
  },
  sectionTitle: {
    flex: 0.8,
    alignItems: isTablet() ? 'flex-start' : 'center',
    justifyContent: 'center',
    marginStart: isTablet() ? '7%' : '0%',
  },
  sectionPager: {
    flex: isTablet() ? 6.3 : 4.45,
  },
  sectionCards: {
    flex: isTablet() ? 3.6 : 4.25,
  },
  sectionBrands: {
    flex: 2.75,
  },
  sectionBottom: {
    flex: isTablet() ? 0.6 : 0.5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
  viewCardContainer: {
    flex: 3.55,
    justifyContent: 'center',
  },
  allSpecsContainer: {
    flex: 3,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  popularSpecsContainer: {
    flex: 7,
    justifyContent: 'center',
    alignItems: 'center',
  },
  shadowViewPager: {
    width: isTablet() ? '87%' : '88%',
    height: '100%',
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: cornerBorder,
    shadowColor: '#4D000000',
    shadowOpacity: 1,
    shadowRadius: 8,
    shadowOffset: {width: 0, height: 0},
  },
  deviceCards: {
    flex: 6.45,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  shadowViewCard: {
    width: '88%',
    height: '70%',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: cornerBorder,
    marginTop: '3%',
    shadowColor: '#4D000000',
    shadowOpacity: 1,
    shadowRadius: 6,
    shadowOffset: {width: 0, height: 0},
  },
  shadowViewBrand: {
    width: windowWidth * 0.41,
    height: windowHeight * 0.105,
    alignItems: 'center',
    justifyContent: 'center',
    margin: '3%',
    borderRadius: cornerBorder,
    shadowColor: '#4D000000',
    shadowOpacity: 1,
    shadowRadius: 3,
    shadowOffset: {width: 0, height: 0},
  },
  shadowViewAllSpecs: {
    width: '87%',
    height: '52%',
    borderRadius: cornerBorder,
    justifyContent: 'center',
    shadowColor: '#4D000000',
    shadowOpacity: 1,
    shadowRadius: 6,
    shadowOffset: {width: 0, height: 0},
  },
  shadowViewPopular: {
    width: '87%',
    height: '67%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: cornerBorder,
    shadowColor: '#4D000000',
    shadowOpacity: 1,
    shadowRadius: 4,
    shadowOffset: {width: 0, height: 0},
  },
  pagerTitle: {
    justifyContent: 'flex-end',
    marginTop: '2%',
    marginStart: '5%',
  },
  textWithRightIcon: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: isTablet() ? '2%' : '3%',
    paddingBottom: isTablet() ? '1%' : '3%',
  },
  viewPopularText: {
    flex: 0.7,
    marginBottom: '3%',
    marginEnd: '7%',
    alignItems: 'flex-start',
  },
  leftThumbnail: {
    flex: 0.3,
    width: '45%',
    height: '45%',
  },
  return: {
    marginStart: '5%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  legal: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  backIcon: {
    marginEnd: '7%',
    width: 12,
    height: 12,
  },
  plusIcon: {
    marginStart: '7%',
    width: 12,
    height: 12,
  },
  chevronIcon: {
    marginStart: '2%',
    width: isTablet() ? 15 : 11,
    height: isTablet() ? 15 : 11,
  },
});

const stylesLand = StyleSheet.create({
  // DEFAULT/PORTRAIT STYLES
  rootContainerLand: {
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  sectionTitleLand: {
    flex: 0.5,
    justifyContent: 'flex-end',
    marginBottom: isTablet() ? '3%' : '3%',
    marginStart: isTablet() ? '6%' : '7%',
  },
  sectionPagerLand: {
    flex: 4,
    marginBottom: '2.5%',
    marginStart: isTablet() ? '6%' : '7%',
  },
  sectionCardsLand: {
    flex: 4.9,
    marginTop: '7.5%',
  },
  sectionBrandsLand: {
    flex: 3.15,
  },
  sectionReturnLand: {
    flex: 0.5,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  sectionLegalLand: {
    flex: 0.5,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
  },
  subContainerPagerLand: {
    flex: isTablet() ? 6.6 : 5.5,
  },
  subContainerDevicesLand: {
    flex: 4.5,
  },
  subContainerViewPopularLand: {
    flex: 3.4,
  },
  popularSpecsContainerLand: {
    flex: 4.9,
    justifyContent: 'center',
    alignItems: 'center',
  },
  shadowViewPagerLand: {
    width: '100%',
    height: '97%',
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: cornerBorder,
    shadowColor: '#4D000000',
    shadowOpacity: 1,
    shadowRadius: 8,
    shadowOffset: {width: 0, height: 0},
  },
  shadowViewCardLand: {
    width: '84%',
    height: '72%',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: cornerBorder,
    marginTop: '2.5%',
    marginEnd: '1%',
    shadowColor: '#4D000000',
    shadowOpacity: 1,
    shadowRadius: 6,
    shadowOffset: {width: 0, height: 0},
  },
  shadowViewBrandLand: {
    width: windowHeight * 0.192,
    height: windowWidth * 0.19,
    alignItems: 'center',
    justifyContent: 'center',
    marginStart: '3.2%',
    marginEnd: '3.2%',
    marginTop: '4%',
    marginBottom: '3%',
    borderRadius: cornerBorder,
    shadowColor: '#4D000000',
    shadowOpacity: 1,
    shadowRadius: 3,
    shadowOffset: {width: 0, height: 0},
  },
  shadowViewPopularLand: {
    width: '75%',
    height: '80.5%',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '14%',
    marginEnd: '3%',
    borderRadius: cornerBorder,
    shadowColor: '#4D000000',
    shadowOpacity: 1,
    shadowRadius: 6,
    shadowOffset: {width: 0, height: 0},
  },
  viewPopularTextLand: {
    flex: 8,
    marginStart: '24%',
    marginEnd: '24%',
    alignItems: 'flex-start',
  },
  returnLand: {
    marginStart: isTablet() ? '6%' : '5%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  legalLand: {
    marginEnd: isTablet() ? '7%' : '3%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  leftThumbnailLand: {
    flex: 2,
    width: '45%',
    height: '45%',
    marginTop: '15%',
    marginStart: '15%',
    marginEnd: '15%',
    marginBottom: '5%',
  },
  chevronIconLand: {
    alignSelf: 'flex-end',
    marginBottom: '3.5%',
    width: isTablet() ? 15 : 11,
    height: isTablet() ? 15 : 11,
  },
});

export default ChooseDeviceScreen;
