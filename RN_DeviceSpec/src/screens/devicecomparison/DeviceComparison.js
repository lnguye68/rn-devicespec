import React, {useContext, useEffect, useState, useRef} from 'react'; // get the React object from the react module
import {
  StyleSheet,
  FlatList,
  ScrollView,
  View,
  BackHandler,
  TouchableWithoutFeedback,
} from 'react-native';
import {useNavigation, useIsFocused, useRoute} from '@react-navigation/native';
import {isTablet} from 'react-native-device-info';
import {StringsContext} from '@contexts/StringsContext';
import {ThemeContext} from '@contexts/ThemeContext';
import {OrientationContext} from '@contexts/OrientationContext';
import AnalyticsModule from '../../modules/AnalyticsModule';
import FileUtil from '@utils/FileUtil';
import {SCREEN_NAMES} from '../../utils/Constants';
import DeviceComparisonItem from './DeviceComparisonItem';
import DeviceComparisonView from './DeviceComparisonView';
import LoadingIndicator from '@components/LoadingIndicator';
import CustomText from '@components/CustomText';
import FastImage from 'react-native-fast-image';
import ShadowView from 'react-native-simple-shadow-view';
import LegalScreen from '@screens/legal/LegalScreen';
import {
  BorderlessButton,
  TouchableNativeFeedback,
} from 'react-native-gesture-handler';
import {
  DeviceComparisonContext,
  DeviceComparisonProvider,
} from './DeviceComparisonContext';

export default function DeviceComparison() {
  const TAG = 'DeviceComparison';
  const navigation = useNavigation();
  const isFocused = useIsFocused();
  const route = useRoute();
  const {prevScreen, currScreen} = route.params;
  const {orientation} = useContext(OrientationContext);
  const {getString} = useContext(StringsContext);
  const {theme, DefaultTheme} = useContext(ThemeContext);
  let [compareListItems, setCompareListItems] = useState([]);
  const [compareListItems2, setCompareListItems2] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const legalRef = useRef(null);

  // private fun getMergedSpecsFromModels(spec: SpecsModel.Spec, comparedSpecs: SpecsModel.Spec) {
  //     if (comparedSpecs.deviceImage() != null)
  //         spec.comparedDeviceImage(comparedSpecs.deviceImage())
  //     if (comparedSpecs.comparedDeviceName() != null)
  //         spec.comparedDeviceName(comparedSpecs.comparedDeviceName())
  //     if (comparedSpecs.comparedDeviceNameFont() != null)
  //         spec.comparedDeviceNameFont(comparedSpecs.comparedDeviceNameFont())
  //     if (comparedSpecs.comparedDeviceNameFontSize() != null)
  //         spec.comparedDeviceNameFontSize(comparedSpecs.comparedDeviceNameFontSize())
  //     if (comparedSpecs.comparedDeviceNameFontColor() != null)
  //         spec.comparedDeviceNameFontColor(comparedSpecs.comparedDeviceNameFontColor())
  //     if (comparedSpecs.deviceSpec() != null)
  //         spec.comparedDeviceSpec(comparedSpecs.deviceSpec())
  //     if (comparedSpecs.deviceSpecFont() != null)
  //         spec.comparedDeviceSpecFont(comparedSpecs.deviceSpecFont())
  //     if (comparedSpecs.deviceSpecTextSize() != null)
  //         spec.comparedDeviceSpecTextSize(comparedSpecs.deviceSpecTextSize())
  //     if (comparedSpecs.deviceSpecTextColor() != null)
  //         spec.comparedDeviceSpecTextColor(comparedSpecs.deviceSpecTextColor())
  //     if (comparedSpecs.colors() != null)
  //         spec.comparedColors(comparedSpecs.colors())

  //     if (spec.column1() != null && comparedSpecs.column1() != null && spec.column1().size > 0 && comparedSpecs.column1().size > 0) {
  //         val columnSize = max(spec.column1().size, comparedSpecs.column1().size)
  //         for (columnIndex in 0 until columnSize) {
  //             var specModelInColumn: SpecsModel.Spec?
  //             val comparedSpecModelInColumn: SpecsModel.Spec? = try {
  //                 comparedSpecs.column1()[columnIndex]
  //             } catch (e: IndexOutOfBoundsException) {
  //                 null
  //             }
  //             specModelInColumn = try {
  //                 spec.column1()[columnIndex]
  //             } catch (e: IndexOutOfBoundsException) {
  //                 null
  //             }
  //             //Note: At least one of specModelInColumn or comparedSpecModelInColumn will not be null.

  //             if (specModelInColumn == null && comparedSpecModelInColumn != null) {    //The compared specs column has more entries than current specs column.
  //                 //For example, in these 2 model json files, the spec's colunn1 only has 1 entry whereas the comparedSpecs's column1 has 2 entries.
  //                 //spec                                                                          comparedSpecs
  //                 //{                                                                             {
  //                 //    "layout": "@layout/device_spec_item_extra_text",                              "layout": "@layout/device_spec_item_extra_text",
  //                 //    "deviceSpecItem": "@string/specs_compare4",                                   "deviceSpecItem": "@string/specs_compare4",
  //                 //    "deviceSpecItemFont": "@string/specs_compare4_font",                          "deviceSpecItemFont": "@string/specs_compare4_font",
  //                 //    "deviceSpecItemTextSize": "@dimen/specs_compare4_font_size",                  "deviceSpecItemTextSize": "@dimen/specs_compare4_font_size",
  //                 //    "deviceSpecItemTextColor": "@color/specs_compare4_font_color",                "deviceSpecItemTextColor": "@color/specs_compare4_font_color",
  //                 //    "column1": [                                                                  "column1": [
  //                 //      {                                                                               {
  //                 //        "layout": "@layout/device_spec_subitem_start",                                    "layout": "@layout/device_spec_subitem_start",
  //                 //        "deviceSpec": "@string/specs_compare5",                                           "deviceSpec": "@string/specs_compare5",
  //                 //        "deviceSpecFont": "@string/specs_compare5_font",                                  "deviceSpecFont": "@string/specs_compare5_font",
  //                 //        "deviceSpecTextSize": "@dimen/specs_compare5_font_size",                          "deviceSpecTextSize": "@dimen/specs_compare5_font_size",
  //                 //        "deviceSpecTextColor": "@color/specs_compare5_font_color"                         "deviceSpecTextColor": "@color/specs_compare5_font_color"
  //                 //      },                                                                              },
  //                 //    ]                                                                                 {
  //                 // }                                                                                        "layout": "@layout/device_spec_subitem_end",
  //                 //                                                                                          "deviceSpec": "@string/specs_compare11",
  //                 //                                                                                          "deviceSpecFont": "@string/specs_compare11_font",
  //                 //                                                                                          "deviceSpecFont": "@string/specs_compare11_font",
  //                 //                                                                                          "deviceSpecTextSize": "@dimen/specs_compare11_font_size",
  //                 //                                                                                          "deviceSpecTextColor": "@color/specs_compare11_font_color"
  //                 //                                                                                      }
  //                 //                                                                                  ]
  //                 //                                                                              }
  //                 //Create a new specs model
  //                 specModelInColumn = SpecsModel.Spec()
  //                 //Set the layout from comparedSpecModel
  //                 specModelInColumn.layout(comparedSpecModelInColumn.layout())

  //                 if (comparedSpecModelInColumn.deviceImage() != null)
  //                     specModelInColumn.comparedDeviceImage(comparedSpecModelInColumn.deviceImage())
  //                 if (comparedSpecModelInColumn.comparedDeviceName() != null)
  //                     specModelInColumn.comparedDeviceName(comparedSpecModelInColumn.comparedDeviceName())
  //                 if (comparedSpecModelInColumn.comparedDeviceNameFont() != null)
  //                     specModelInColumn.comparedDeviceNameFont(comparedSpecModelInColumn.comparedDeviceNameFont())
  //                 if (comparedSpecModelInColumn.comparedDeviceNameFontSize() != null)
  //                     specModelInColumn.comparedDeviceNameFontSize(comparedSpecModelInColumn.comparedDeviceNameFontSize())
  //                 if (comparedSpecModelInColumn.comparedDeviceNameFontColor() != null)
  //                     specModelInColumn.comparedDeviceNameFontColor(comparedSpecModelInColumn.comparedDeviceNameFontColor())
  //                 if (comparedSpecModelInColumn.deviceSpec() != null)
  //                 //Since the comparedSpecModelInColumn is not empty and the specModelInColumn is empty,
  //                 //we set the specModelInColumn deviceSpec string to an empty string.
  //                 //(Take a look at method setSpecItem(Context context, ViewHolderSpecItem holder, SpecsModel.Spec spec) in DeviceCompareAdapter)
  //                 //Do this so that the specModelInColumn deviceSpec string will show up in final layout.
  //                 //(Take a look at layout/device_spec_subitem_start and layout/device_spec_subitem_end)
  //                     specModelInColumn.deviceSpec(EMPTY_MODEL_JSON_STRING)

  //                 specModelInColumn.comparedDeviceSpec(comparedSpecModelInColumn.deviceSpec())
  //                 if (comparedSpecModelInColumn.deviceSpecFont() != null)
  //                     specModelInColumn.comparedDeviceSpecFont(comparedSpecModelInColumn.deviceSpecFont())
  //                 if (comparedSpecModelInColumn.deviceSpecTextSize() != null)
  //                     specModelInColumn.comparedDeviceSpecTextSize(comparedSpecModelInColumn.deviceSpecTextSize())
  //                 if (comparedSpecModelInColumn.deviceSpecTextColor() != null)
  //                     specModelInColumn.comparedDeviceSpecTextColor(comparedSpecModelInColumn.deviceSpecTextColor())
  //                 if (comparedSpecModelInColumn.colors() != null)
  //                     specModelInColumn.comparedColors(comparedSpecModelInColumn.colors())

  //                 spec.column1().add(specModelInColumn)

  //             } else if (specModelInColumn != null && comparedSpecModelInColumn == null) {
  //                 //The current spec model column has more entries than compared spec column. This case also takes care of the normal case
  //                 //where both the specModelInColumn and comparedSpecModelInColumn have equal number of entries.
  //                 //For example, in these 2 model json files, the spec's colunn1 has 2 entries whereas the comparedSpecs's column1 has only 1 entry.
  //                 //spec                                                                                 comparedSpecs
  //                 //{                                                                             {
  //                 //    "layout": "@layout/device_spec_item_extra_text",                              "layout": "@layout/device_spec_item_extra_text",
  //                 //    "deviceSpecItem": "@string/specs_compare4",                                   "deviceSpecItem": "@string/specs_compare4",
  //                 //    "deviceSpecItemFont": "@string/specs_compare4_font",                          "deviceSpecItemFont": "@string/specs_compare4_font",
  //                 //    "deviceSpecItemTextSize": "@dimen/specs_compare4_font_size",                  "deviceSpecItemTextSize": "@dimen/specs_compare4_font_size",
  //                 //    "deviceSpecItemTextColor": "@color/specs_compare4_font_color",                "deviceSpecItemTextColor": "@color/specs_compare4_font_color",
  //                 //    "column1": [                                                                  "column1": [
  //                 //      {                                                                               {
  //                 //        "layout": "@layout/device_spec_subitem_start",                                    "layout": "@layout/device_spec_subitem_start",
  //                 //        "deviceSpec": "@string/specs_compare5",                                           "deviceSpec": "@string/specs_compare5",
  //                 //        "deviceSpecFont": "@string/specs_compare5_font",                                  "deviceSpecFont": "@string/specs_compare5_font",
  //                 //        "deviceSpecTextSize": "@dimen/specs_compare5_font_size",                          "deviceSpecTextSize": "@dimen/specs_compare5_font_size",
  //                 //        "deviceSpecTextColor": "@color/specs_compare5_font_color"                         "deviceSpecTextColor": "@color/specs_compare5_font_color"
  //                 //      },                                                                              }
  //                 //      {                                                                           ]
  //                 //        "layout": "@layout/device_spec_subitem_end",                          }
  //                 //        "deviceSpec": "@string/specs_compare7",
  //                 //        "deviceSpecFont": "@string/specs_compare7_font",
  //                 //        "deviceSpecTextSize": "@dimen/specs_compare7_font_size",
  //                 //        "deviceSpecTextColor": "@color/specs_compare7_font_color"
  //                 //      }
  //                 //    ]
  //                 //}
  //                 Log.d(TAG, "#####column index:  $columnIndex  - specModelColumn is not empty. comparedSpecModelInColumn is empty.")
  //             } else if (specModelInColumn != null && comparedSpecModelInColumn != null) {
  //                 if (comparedSpecModelInColumn.deviceImage() != null)
  //                     specModelInColumn.comparedDeviceImage(comparedSpecModelInColumn.deviceImage())
  //                 if (comparedSpecModelInColumn.comparedDeviceName() != null)
  //                     specModelInColumn.comparedDeviceName(comparedSpecModelInColumn.comparedDeviceName())
  //                 if (comparedSpecModelInColumn.comparedDeviceNameFont() != null)
  //                     specModelInColumn.comparedDeviceNameFont(comparedSpecModelInColumn.comparedDeviceNameFont())
  //                 if (comparedSpecModelInColumn.comparedDeviceNameFontSize() != null)
  //                     specModelInColumn.comparedDeviceNameFontSize(comparedSpecModelInColumn.comparedDeviceNameFontSize())
  //                 if (comparedSpecModelInColumn.comparedDeviceNameFontColor() != null)
  //                     specModelInColumn.comparedDeviceNameFontColor(comparedSpecModelInColumn.comparedDeviceNameFontColor())
  //                 if (comparedSpecModelInColumn.deviceSpec() != null)
  //                     specModelInColumn.comparedDeviceSpec(comparedSpecModelInColumn.deviceSpec())
  //                 if (comparedSpecModelInColumn.deviceSpecFont() != null)
  //                     specModelInColumn.comparedDeviceSpecFont(comparedSpecModelInColumn.deviceSpecFont())
  //                 if (comparedSpecModelInColumn.deviceSpecTextSize() != null)
  //                     specModelInColumn.comparedDeviceSpecTextSize(comparedSpecModelInColumn.deviceSpecTextSize())
  //                 if (comparedSpecModelInColumn.deviceSpecTextColor() != null)
  //                     specModelInColumn.comparedDeviceSpecTextColor(comparedSpecModelInColumn.deviceSpecTextColor())
  //                 if (comparedSpecModelInColumn.colors() != null)
  //                     specModelInColumn.comparedColors(comparedSpecModelInColumn.colors())

  //             }
  //         }
  //     }
  // }

  function max(a, b) {
    if (a > b) {
      return a;
    } else {
      return b;
    }
  }

  /*
   Merge two spec models together for device comparison.
   Spec1 is from device 1.
   Spec2 is from device 2.
  */
  function mergeSpecs(spec1, spec2) {
    if (spec2.deviceImage) {
      spec1.comparedDeviceImage = spec2.deviceImage;
    }
    if (spec2.comparedDeviceName) {
      spec1.comparedDeviceName = spec2.comparedDeviceName;
    }
    if (spec2.deviceSpec) {
      spec1.comparedDeviceSpec = spec2.deviceSpec;
    }

    if (
      spec1.column1 &&
      spec2.column1 &&
      spec1.column1.length > 0 &&
      spec2.column1.length > 0
    ) {
      const columnSize = max(spec1.column1.length, spec2.column1.length);
      for (let columnIndex = 0; columnIndex < columnSize; columnIndex++) {
        let specModelInColumn = spec1.column1[columnIndex];
        let compareSpecModelInColumn = spec2.column1[columnIndex];
        if (!specModelInColumn && compareSpecModelInColumn) {
          // console.log(TAG + ' mergeSpecs case 1');
          //First case: device 1's feature has less texts than device 2's feature.
          specModelInColumn = JSON.parse('[]');
          specModelInColumn.layout = compareSpecModelInColumn.layout;
          if (compareSpecModelInColumn.deviceImage) {
            specModelInColumn.commpareDeviceImage =
              compareSpecModelInColumn.deviceImage;
          }
          if (compareSpecModelInColumn.comparedDeviceName) {
            specModelInColumn.comparedDeviceName =
              compareSpecModelInColumn.comparedDeviceName;
          }
          if (compareSpecModelInColumn.deviceSpec) {
            specModelInColumn.deviceSpec = '';
          }
          specModelInColumn.comparedDeviceSpec =
            compareSpecModelInColumn.deviceSpec;

          spec1.column1[columnIndex] = specModelInColumn;
        } else if (specModelInColumn && !compareSpecModelInColumn) {
          // console.log(TAG + ' mergeSpecs case 2');
          //Second case: device 1's feature has equal or more texts than device 2's feature.
          //Don't need to do anything since we just take spec1
          return specModelInColumn;
        } else if (specModelInColumn && compareSpecModelInColumn) {
          // console.log(TAG + ' mergeSpecs case 3');
          if (compareSpecModelInColumn.deviceImage) {
            specModelInColumn.comparedDeviceImage =
              compareSpecModelInColumn.deviceImage;
          }
          if (compareSpecModelInColumn.comparedDeviceName) {
            specModelInColumn.comparedDeviceName =
              compareSpecModelInColumn.comparedDeviceName;
          }
          if (compareSpecModelInColumn.deviceSpec) {
            specModelInColumn.comparedDeviceSpec =
              compareSpecModelInColumn.deviceSpec;
          }
          spec1.column1[columnIndex] = specModelInColumn;
        }
      }
    }
    // console.log(TAG + ' mergeSpecs newSpec ' + spec1.layout);
    // console.log(TAG + ' mergeSpecs newSpec ' + spec1.deviceImage);
    // console.log(TAG + ' mergeSpecs newSpec ' + spec1.deviceName);
    // console.log(TAG + ' mergeSpecs newSpec ' + spec1.comparedDeviceName);
    if (spec1.column1) {
      for (let j = 0; j < spec1.column1.length; j++) {
        // console.log(TAG + ' mergeSpecs newSpec column ' + j + ': ' + spec1.column1[j].layout);
        // console.log(TAG + ' mergeSpecs newSpec column ' + j + ': ' + spec1.column1[j].deviceSpec);
        // console.log(TAG + ' mergeSpecs newSpec column ' + j + ': ' + spec1.column1[j].comparedDeviceSpec);
      }
    }
    return spec1;
  }

  function mergeComparedSpecs(specsModel, comparedSpecsModel) {
    let newSpec = [];
    for (let i = 0; i < comparedSpecsModel.length; i++) {
      let result = mergeSpecs(specsModel[i], comparedSpecsModel[i]);
      newSpec.push(result);
    }
    for (let i = 0; i < newSpec.length; i++) {
      // console.log(TAG + ' mergeComparedSpecs result ' + i + ' layout ' + newSpec[i].layout);
      // console.log(TAG + ' mergeComparedSpecs result ' + i + ' deviceImage ' + newSpec[i].deviceImage);
      // console.log(TAG + ' mergeComparedSpecs result ' + i + ' deviceName ' + newSpec[i].deviceName);
      // console.log(TAG + ' mergeComparedSpecs result ' + i + ' comparedDeviceImage ' + newSpec[i].comparedDeviceImage);
      // console.log(TAG + ' mergeComparedSpecs result ' + i + ' comparedDeviceName ' + newSpec[i].comparedDeviceName);
      if (newSpec[i].column1) {
        for (let j = 0; j < newSpec[i].column1.length; j++) {
          // console.log(TAG + ' mergeComparedSpecs result column ' + j + ' layout: ' + newSpec[i].column1[j].layout);
          // console.log(TAG + ' mergeComparedSpecs result column ' + j + ' deviceSpec: ' + newSpec[i].column1[j].deviceSpec);
          // console.log(TAG + ' mergeComparedSpecs result column ' + j + ' comparedDeviceSpec: ' + newSpec[i].column1[j].comparedDeviceSpec);
        }
      }
    }
    return newSpec;
  }

  const backAction = () => {
    AnalyticsModule.sendScreenChangeEvent(
      AnalyticsModule.BACK_KEY_PRESS,
      SCREEN_NAMES.DEVICE_COMPARISON /* current screen name */,
      SCREEN_NAMES.CHOOSE_DEVICE /* next screen name */,
    );
    navigation.navigate(SCREEN_NAMES.CHOOSE_DEVICE, {
      prevScreen: SCREEN_NAMES.DEVICE_COMPARISON,
    });
    return true;
  };

  //Listen for native back button press - Android
  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);
  //Listen for 'X' icon click event from native Android module
  const onCloseIconPress = () => {
    // Send Analytics ScreenEvent
    AnalyticsModule.sendScreenChangeEvent(
      AnalyticsModule.BACK_ICON_PRESS,
      SCREEN_NAMES.DEVICE_COMPARISON /* current screen name */,
      SCREEN_NAMES.CHOOSE_DEVICE /* next screen name */,
    );
    navigation.navigate(SCREEN_NAMES.CHOOSE_DEVICE, {
      prevScreen: SCREEN_NAMES.DEVICE_COMPARISON,
    });
  };

  const onPopularSpecPress = () => {
    // Send Analytics ScreenEvent
    AnalyticsModule.sendScreenChangeEvent(
      AnalyticsModule.TAP,
      SCREEN_NAMES.DEVICE_COMPARISON /* current screen name */,
      SCREEN_NAMES.HOME /* next screen name */,
    );
    navigation.navigate(SCREEN_NAMES.HOME, {
      prevScreen: SCREEN_NAMES.DEVICE_COMPARISON,
    });
  };

  const onBackPress = () => {
    // Report Analytics ScreenEvent
    AnalyticsModule.sendScreenChangeEvent(
      AnalyticsModule.BACK_ICON_PRESS,
      SCREEN_NAMES.DEVICE_COMPARISON /* current screen name */,
      SCREEN_NAMES.CHOOSE_DEVICE /* next screen name */,
    );
    navigation.navigate(SCREEN_NAMES.CHOOSE_DEVICE, {
      prevScreen: SCREEN_NAMES.DEVICE_COMPARISON,
    });
  };

  const onLegalPress = () => {
    // Send Analytics ScreenEvent
    AnalyticsModule.sendScreenChangeEvent(
      AnalyticsModule.TAP,
      SCREEN_NAMES.DEVICE_COMPARISON /* current screen name */,
      SCREEN_NAMES.DEVICE_COMPARISON +
        '_' +
        SCREEN_NAMES.LEGAL /* next screen name */,
    );
    // Go to Legal pop-up
    legalRef.current?.open();
  };

  const loadModelFunc = function(deviceModel, comparableDevice) {
    compareListItems = mergeComparedSpecs(deviceModel, comparableDevice);
    for (let i = 0; i < compareListItems.length; i++) {
      // console.log(TAG + ' rendering merge specs ' + i + ' layout: ' + compareListItems[i].layout);
      // console.log(TAG + ' rendering merge specs ' + i + ' deviceImage: ' + compareListItems[i].deviceImage);
      // console.log(TAG + ' rendering merge specs ' + i + ' deviceName: ' + compareListItems[i].deviceName);
      // console.log(TAG + ' rendering merge specs ' + i + ' comparedDeviceImage: ' + compareListItems[i].comparedDeviceImage);
      // console.log(TAG + ' rendering merge specs ' + i + ' comparedDeviceName: ' + compareListItems[i].comparedDeviceName);
      if (compareListItems[i].column1) {
        for (let j = 0; j < compareListItems[i].column1.length; j++) {
          // console.log(TAG + ' rendering merge specs column ' + j + ' layout: ' + compareListItems[i].column1[j].layout);
          // console.log(TAG + ' rendering merge specs column ' + j + ' deviceSpec: ' + compareListItems[i].column1[j].deviceSpec);
          // console.log(TAG + ' rendering merge specs column ' + j + ' comparedDeviceSpec: ' + compareListItems[i].column1[j].comparedDeviceSpec);
        }
      }
    }
  };

  // Expand all function
  const expandAllChevronExpand =
    theme === 'dark'
      ? 'expand_all_plus_icon_white'
      : 'expand_all_plus_icon_blue';
  const expandAllChevronCollapse =
    theme === 'dark'
      ? 'expand_all_minus_icon_white'
      : 'expand_all_minus_icon_blue';
  const [expandIconURI, setExpandIconURI] = useState(expandAllChevronExpand);
  const [expandAllText, setExpandAllText] = useState(
    'screen_fullspec_expand_all',
  );
  const expandAllPressed = () => {
    for (var i = 0; i < compareListItems.length; i++) {
      if (expandIconURI === expandAllChevronExpand) {
        isCollapse[i] = false;
        iconURI[i] = listItemCollapseIcon;
      } else {
        isCollapse[i] = true;
        iconURI[i] = listItemExpandIcon;
      }
    }
    if (expandIconURI === expandAllChevronExpand) {
      // Send Analytics User Event
      AnalyticsModule.sendUserEvent(
        AnalyticsModule.TAP,
        'Expand_All_Spec_Content',
      );
      setExpandAllText('screen_fullspec_collapse_all');
    } else {
      // Send Analytics User Event
      AnalyticsModule.sendUserEvent(
        AnalyticsModule.TAP,
        'Collapse_All_Spec_Content',
      );
      setExpandAllText('screen_fullspec_expand_all');
    }

    setExpandIconURI(state => {
      return state === expandAllChevronExpand
        ? expandAllChevronCollapse
        : expandAllChevronExpand;
    });
  };
  const [isCollapse] = useState([]);
  const [iconURI] = useState([]);
  const [isFirstRun, setIsFirstRun] = useState(true);
  const [onRefresh, setOnRefresh] = useState(false);

  const listItemExpandIcon =
    theme === 'dark' ? 'white_plus_icon' : 'blue_plus_icon';
  const listItemCollapseIcon =
    theme === 'dark' ? 'white_minus_icon' : 'blue_minus_icon';
  const backButtonIcon = theme === 'dark' ? 'back_arrow_white' : 'back_arrow';
  const legalButtonIcon =
    theme === 'dark' ? 'legal_white_plus_icon' : 'black_plus_icon';
  const popularSpecIcon =
    theme === 'dark'
      ? 'arrow_chevron_right_white'
      : 'arrow_chevron_right_black';
  const xIconURI =
    theme === 'dark' ? '@drawable/white_x_circle' : '@drawable/blue_x_circle';

  const onSectionPress = id => {
    isCollapse[id] = !isCollapse[id];
    if (iconURI[id] === listItemExpandIcon) {
      iconURI[id] = listItemCollapseIcon;
      // Send Analytics User Event
      AnalyticsModule.sendUserEvent(
        AnalyticsModule.TAP,
        'Expand_' + id + '_Spec_Content',
      );
    } else {
      iconURI[id] = listItemExpandIcon;
      // Send Analytics User Even
      AnalyticsModule.sendUserEvent(
        AnalyticsModule.TAP,
        'Collapse_' + id + '_Spec_Content',
      );
    }
    setOnRefresh(!onRefresh);
  };

  // Start timer to automatically collapse first section in FlatList
  useEffect(() => {
    const timer = setTimeout(() => {
      if (isCollapse[0] == false) {
        isCollapse[0] = !isCollapse[0];
        iconURI[0] = listItemExpandIcon;
        setOnRefresh(!onRefresh);
      }
    }, 5000);
    return () => clearTimeout(timer);
  }, []);

  const renderItem = function({item}) {
    // Expand the first section in FlatList only at first enter the screen
    if (isFirstRun && item.id == 0) {
      for (var i = 0; i < compareListItems.length; i++) {
        if (i == 0) {
          isCollapse[i] = false;
          iconURI[i] = listItemCollapseIcon;
        } else {
          isCollapse[i] = true;
          iconURI[i] = listItemExpandIcon;
        }
      }
      setIsFirstRun(false);
    }
    return (
      <DeviceComparisonItem
        data={item}
        onPressItem={onSectionPress}
        isCollapse={isCollapse[item.id]}
        iconURI={iconURI[item.id]}
      />
    );
  };

  const _loadDataIntoList = () => {
    const context = useContext(DeviceComparisonContext);

    return (
      <>
        {context.isLoading && context.isLoading2 ? (
          <>
            {console.log(TAG + ': loading...')}
            <LoadingIndicator color="black" />
          </>
        ) : (
          <>
            {console.log(TAG + ': rendering specs content...')}
            {loadModelFunc(context.deviceModel, context.comparableDevice)}
            <FlatList
              data={compareListItems}
              keyExtractor={item => item.id}
              renderItem={renderItem}
              extraData={onRefresh}
              showsVerticalScrollIndicator={false}
            />
          </>
        )}
      </>
    );
  };

  const renderLegalPopup = function() {
    return (
      <LegalScreen prevScreen={SCREEN_NAMES.DEVICE_COMPARISON} ref={legalRef} />
    );
  };

  const renderDevices = function() {
    return (
      <View
        style={{flex: 1, flexDirection: 'row', justifyContent: 'space-around'}}>
        <View style={portStyles.leftDeviceContainer}>
          <DeviceComparisonView
            style={{flex: 1}}
            leftDeviceImg={'@drawable/temp_1_device'}
            rightDeviceImg={'@drawable/apple_iphone_xr'}
            xIconURI={null}
            deviceNameColor={DefaultTheme[theme].fontColor}
            onClick={onCloseIconPress}
          />
        </View>

        <View style={portStyles.rightDeviceContainer}>
          <DeviceComparisonView
            style={{flex: 1}}
            leftDeviceImg={'@drawable/temp_1_device'}
            rightDeviceImg={'@drawable/apple_iphone_xr'}
            xIconURI={xIconURI}
            deviceNameColor={DefaultTheme[theme].fontColor}
            onClick={onCloseIconPress}
          />
        </View>
      </View>
    );
  };

  const renderPopularSpec = function() {
    if (orientation === 'PORTRAIT') {
      return (
        <TouchableWithoutFeedback onPress={onPopularSpecPress}>
          <ShadowView
            style={[
              portStyles.popularSpecView,
              {backgroundColor: DefaultTheme[theme].cardColor},
            ]}>
            <FastImage
              style={portStyles.popularSpecImg}
              source={{uri: 'popular_spec_img'}}
              resizeMode={FastImage.resizeMode.contain}
            />
            <View style={portStyles.subFeaturesView}>
              <View style={portStyles.titleContainer}>
                <CustomText
                  data={getString('device_comparison_popular_feature_title')}
                  fontColor={DefaultTheme[theme].fontColor}
                  fontSize={isTablet() ? 18 : 14}
                />
                <FastImage
                  style={portStyles.newFeaturesIcon}
                  source={{uri: popularSpecIcon}}
                  resizeMode={FastImage.resizeMode.contain}
                />
              </View>
              <CustomText
                data={getString('screen_home_compare_devices_desc')}
                fontColor={DefaultTheme[theme].fontColor}
                fontSize={isTablet() ? 15 : 12}
              />
            </View>
          </ShadowView>
        </TouchableWithoutFeedback>
      );
    } else {
      return (
        <TouchableWithoutFeedback onPress={onPopularSpecPress}>
          <ShadowView
            style={[
              landStyles.popularSpecView,
              {backgroundColor: DefaultTheme[theme].cardColor},
            ]}>
            <FastImage
              style={landStyles.popularSpecImg}
              source={{uri: 'popular_spec_img'}}
              resizeMode={FastImage.resizeMode.contain}
            />
            <View style={landStyles.subFeaturesView}>
              <View style={landStyles.titleContainer}>
                <CustomText
                  data={getString('device_comparison_popular_feature_title')}
                  fontColor={DefaultTheme[theme].fontColor}
                  fontSize={isTablet() ? 18 : 14}
                />
                <FastImage
                  style={landStyles.newFeaturesIcon}
                  source={{uri: popularSpecIcon}}
                  resizeMode={FastImage.resizeMode.contain}
                />
              </View>
              <CustomText
                data={getString('screen_home_compare_devices_desc')}
                fontColor={DefaultTheme[theme].fontColor}
                fontSize={isTablet() ? 15 : 12}
              />
            </View>
          </ShadowView>
        </TouchableWithoutFeedback>
      );
    }
  };

  const renderScreenFooter = () => {
    return (
      <>
        <TouchableNativeFeedback onPress={onBackPress}>
          <View style={portStyles.backChevronView}>
            <FastImage
              style={portStyles.backChevron}
              source={{uri: backButtonIcon}}
              resizeMode={FastImage.resizeMode.contain}
            />
            <CustomText
              data={getString('screen_fullspec_back')}
              fontColor={DefaultTheme[theme].fontColor}
              fontSize={isTablet() ? 15 : 12}
            />
          </View>
        </TouchableNativeFeedback>

        <TouchableNativeFeedback onPress={onLegalPress}>
          <View style={portStyles.viewLegalView}>
            <CustomText
              data={getString('screen_fullspec_legal')}
              fontColor={DefaultTheme[theme].fontColor}
              fontSize={isTablet() ? 15 : 12}
            />
            <FastImage
              style={portStyles.viewLegalChevron}
              source={{uri: legalButtonIcon}}
              resizeMode={FastImage.resizeMode.contain}
            />
          </View>
        </TouchableNativeFeedback>
      </>
    );
  };

  const renderLandscapeTitle = function() {
    return (
      <>
        <CustomText
          data={getString('specs_y_devicekeyfeatures2')}
          fontColor={DefaultTheme[theme].fontColor}
          fontSize={isTablet() ? 30 : 23}
        />
        <CustomText
          data={getString('specs_y_devicekeyfeatures2_1')}
          fontColor={DefaultTheme[theme].fontColor}
          fontSize={isTablet() ? 30 : 23}
        />
      </>
    );
  };

  const renderLayout = function() {
    if (orientation === 'PORTRAIT') {
      return (
        <View
          style={[
            {flex: 1},
            {backgroundColor: DefaultTheme[theme].backgroundColor},
          ]}>
          {/* Device images View */}
          <View style={portStyles.deviceImagesRootView}>{renderDevices()}</View>
          {/* View Legal and Back button */}
          <View style={portStyles.footerContainer}>{renderScreenFooter()}</View>

          <View style={{flex: isTablet() ? 0.78 : 0.7}}>
            <ScrollView showsVerticalScrollIndicator={false}>
              {/* Section List View */}
              <ShadowView
                style={[
                  portStyles.listViewContainer,
                  {backgroundColor: DefaultTheme[theme].cardColor},
                ]}>
                <_loadDataIntoList />
                {/* Expand all button */}
                <View style={portStyles.expandAllContainer}>
                  <TouchableWithoutFeedback onPress={expandAllPressed}>
                    <View style={portStyles.expandAllIconView}>
                      <CustomText
                        data={getString(expandAllText)}
                        fontColor={DefaultTheme[theme].expandAllTextFontColor}
                        fontSize={isTablet() ? 11 : 10}
                      />
                      <FastImage
                        style={portStyles.expandAllIcon}
                        source={{uri: expandIconURI}}
                        resizeMode={FastImage.resizeMode.contain}
                      />
                    </View>
                  </TouchableWithoutFeedback>
                </View>
              </ShadowView>

              {/* Popular Specification CardView */}
              {renderPopularSpec()}
            </ScrollView>
          </View>
          {renderLegalPopup()}
        </View>
      );
    } else {
      return (
        <View
          style={[
            {flex: 1},
            {backgroundColor: DefaultTheme[theme].backgroundColor},
          ]}>
          {/* View Legal and Back button */}
          <View style={landStyles.footerContainer}>{renderScreenFooter()}</View>
          {/* Device Name */}
          <View style={landStyles.deviceNameContainer}>
            {renderLandscapeTitle()}
          </View>

          <View style={landStyles.landSpecsContent}>
            {/* Section List View */}
            <ShadowView
              style={[
                landStyles.listViewContainer,
                {backgroundColor: DefaultTheme[theme].cardColor},
              ]}>
              <_loadDataIntoList />
              {/* Expand all button */}
              <View style={landStyles.expandAllContainer}>
                <TouchableWithoutFeedback onPress={expandAllPressed}>
                  <View style={portStyles.expandAllIconView}>
                    <FastImage
                      style={landStyles.expandAllIcon}
                      source={{uri: expandIconURI}}
                      resizeMode={FastImage.resizeMode.contain}
                    />
                    <CustomText
                      data={getString(expandAllText)}
                      fontColor={DefaultTheme[theme].expandAllTextFontColor}
                      fontSize={isTablet() ? 12 : 11}
                    />
                  </View>
                </TouchableWithoutFeedback>
              </View>
              {/* Device images View */}
              <View style={landStyles.leftDeviceContainer}>
                <View style={{flex: 1}}>
                  <DeviceComparisonView
                    style={{flex: 1}}
                    leftDeviceImg={'@drawable/temp_1_device'}
                    rightDeviceImg={'@drawable/apple_iphone_xr'}
                    xIconURI={null}
                    deviceNameColor={DefaultTheme[theme].fontColor}
                    onClick={onCloseIconPress}
                  />
                </View>
              </View>
              <View style={landStyles.rightDeviceContainer}>
                <View style={{flex: 1}}>
                  <DeviceComparisonView
                    style={{flex: 1}}
                    leftDeviceImg={'@drawable/temp_1_device'}
                    rightDeviceImg={'@drawable/apple_iphone_xr'}
                    xIconURI={xIconURI}
                    deviceNameColor={DefaultTheme[theme].fontColor}
                    onClick={onCloseIconPress}
                  />
                </View>
              </View>
            </ShadowView>
            {/* Popular Specification CardView */}
            <View style={landStyles.popularSpecContainer}>
              {renderPopularSpec()}
            </View>
          </View>
          {renderLegalPopup()}
        </View>
      );
    }
  };

  if (isFocused) {
    return (
      //Wrap UI into the contextAPI in oreder to update UI accordingly
      <DeviceComparisonProvider>{renderLayout()}</DeviceComparisonProvider>
    );
  } else {
    return <></>;
  }
}

//////////////// PORTRAIT ////////////////
const portStyles = StyleSheet.create({
  // Device images View
  deviceImagesRootView: {
    flex: isTablet() ? 0.22 : 0.3,
    paddingTop: '2%',
  },

  // Section List View
  listViewContainer: {
    marginStart: isTablet() ? '8%' : '6.5%',
    marginEnd: isTablet() ? '8%' : '6.5%',
    marginTop: isTablet() ? '4%' : '8%',
    paddingTop: '5%',
    paddingBottom: '5%',
    shadowColor: '#4D000000',
    shadowOpacity: 1,
    shadowRadius: 6,
    shadowOffset: {width: 0, height: 0},
    borderRadius: 12,
  },

  expandAllContainer: {
    position: 'absolute',
    alignSelf: 'flex-end',
    marginTop: '8%',
    marginEnd: '8%',
  },

  expandAllIconView: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  expandAllIcon: {
    width: isTablet() ? 15 : 10,
    height: isTablet() ? 15 : 10,
    marginStart: '6%',
  },

  leftDeviceContainer: {
    width: isTablet() ? 120 : 120,
    height: isTablet() ? 240 : 240,
    marginStart: isTablet() ? '5%' : '5%',
  },

  rightDeviceContainer: {
    width: isTablet() ? 120 : 120,
    height: isTablet() ? 240 : 240,
    marginEnd: isTablet() ? '5%' : '5%',
  },

  // Popular Specification CardView
  popularSpecView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginStart: isTablet() ? '8%' : '6.5%',
    marginEnd: isTablet() ? '8%' : '6.5%',
    marginTop: isTablet() ? '5%' : '7%',
    marginBottom: '5%',
    shadowColor: '#4D000000',
    shadowOpacity: 1,
    shadowRadius: 6,
    shadowOffset: {width: 0, height: 0},
    borderRadius: 12,
  },

  popularSpecImg: {
    flex: 0.2,
    width: isTablet() ? 70 : 43,
    height: isTablet() ? 75 : 55,
    marginTop: '0%',
    marginStart: '4%',
  },

  subFeaturesView: {
    flex: 0.8,
    marginTop: isTablet() ? '5%' : '8%',
    marginBottom: isTablet() ? '5%' : '8%',
    marginStart: '2%',
    marginEnd: '2%',
  },

  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  newFeaturesIcon: {
    width: isTablet() ? 17 : 12,
    height: isTablet() ? 17 : 12,
    marginStart: isTablet() ? '4%' : '6%',
  },

  // View Legal and Back button
  footerContainer: {
    position: 'absolute',
    bottom: 0,
    start: 0,
    end: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: isTablet() ? '3%' : '5%',
    marginStart: '4%',
    marginEnd: '4%',
  },

  backChevronView: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  backChevron: {
    width: isTablet() ? 16 : 12,
    height: isTablet() ? 16 : 12,
    marginEnd: isTablet() ? '9%' : '6%',
  },

  viewLegalView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },

  viewLegalChevron: {
    width: isTablet() ? 16 : 12,
    height: isTablet() ? 16 : 12,
    marginStart: isTablet() ? '9%' : '6%',
  },
});
//////////////// LANDSCAPE ////////////////
const landStyles = StyleSheet.create({
  // Device Name
  deviceNameContainer: {
    marginTop: '1%',
    marginEnd: isTablet() ? '25%' : '20%',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },

  landSpecsContent: {
    flexDirection: 'row',
    marginStart: isTablet() ? '4.5%' : '4%',
    marginEnd: isTablet() ? '4.8%' : '3%',
    marginTop: isTablet() ? '2.2%' : '1%',
  },

  // Device images View / Section List View
  listViewContainer: {
    flex: 0.65,
    paddingStart: '15%',
    paddingEnd: isTablet() ? '18%' : '15%',
    paddingBottom: '5%',
    shadowColor: '#4D000000',
    shadowOpacity: 1,
    shadowRadius: 6,
    shadowOffset: {width: 0, height: 0},
    borderRadius: 12,
  },

  expandAllContainer: {
    position: 'absolute',
    alignSelf: 'flex-start',
    marginTop: isTablet() ? '8%' : '5%',
    marginStart: isTablet() ? '8%' : '7%',
  },

  expandAllIcon: {
    width: isTablet() ? 15 : 10,
    height: isTablet() ? 15 : 10,
    marginEnd: '6%',
  },

  leftDeviceContainer: {
    position: 'absolute',
    width: isTablet() ? 140 : 100,
    height: isTablet() ? 240 : 190,
    top: 0,
    start: isTablet() ? '3.5%' : '5%',
    marginTop: isTablet() ? '16%' : '9%',
  },

  rightDeviceContainer: {
    position: 'absolute',
    width: isTablet() ? 140 : 100,
    height: isTablet() ? 240 : 190,
    top: 0,
    end: isTablet() ? '3.5%' : '5%',
    marginTop: isTablet() ? '17%' : '8%',
  },
  // Popular Specification CardView
  popularSpecContainer: {
    flex: 0.35,
    height: isTablet() ? 520 : 270,
    justifyContent: 'flex-start',
    marginStart: isTablet() ? '3.5%' : '2%',
  },

  popularSpecView: {
    flex: 1,
    alignItems: 'center',
    shadowColor: '#4D000000',
    shadowOpacity: 1,
    shadowRadius: 6,
    shadowOffset: {width: 0, height: 0},
    borderRadius: 12,
  },

  popularSpecImg: {
    flex: 0.35,
    width: isTablet() ? 70 : 43,
    height: isTablet() ? 75 : 55,
    marginTop: '2%',
  },

  subFeaturesView: {
    flex: 0.65,
    marginStart: isTablet() ? '12%' : '14%',
    marginEnd: isTablet() ? '12%' : '14%',
    marginTop: '2%',
  },

  titleContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginBottom: '10%',
  },

  newFeaturesIcon: {
    position: 'absolute',
    alignSelf: 'flex-end',
    bottom: isTablet() ? '10%' : '7%',
    width: isTablet() ? 17 : 12,
    height: isTablet() ? 17 : 12,
  },

  // View Legal and Back button
  footerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    position: 'absolute',
    bottom: 0,
    start: 0,
    end: 0,
    marginStart: '2%',
    marginEnd: '2%',
    marginTop: '1%',
    marginBottom: '2%',
  },
});
