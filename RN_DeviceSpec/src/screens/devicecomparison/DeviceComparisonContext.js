import React, {createContext, useState, useEffect, useContext} from 'react';
import JsonModule from '@modules/JsonModule';

export const DeviceComparisonContext = createContext();

export const DeviceComparisonProvider = ({children}) => {
  const [deviceModel, setDeviceModel] = useState([]);
  const [comparableDevice, setComparableDevice] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isLoading2, setIsLoading2] = useState(true);
  /* Load device model json  */
  async function _fetchDeviceModelJson() {
    setIsLoading(true);
    try {
      const jsonModel = await JsonModule.getJsonModelTextAsync(
        'model/devicecomparison/galaxy_hubble_x_spec_full_feature.json'
      );
      setDeviceModel(JSON.parse(jsonModel));
    } catch (err) {
      console.log(err);
    }
    setIsLoading(false);
  }

  useEffect(() => {
    _fetchDeviceModelJson();
  }, []);
  /* Load comparable device model json */
  async function _fetchcomparableDeviceJson() {
    setIsLoading2(true);
    try {
      const jsonModel = await JsonModule.getJsonModelTextAsync(
        'model/devicecomparison/galaxy_hubble_y_spec_full_feature.json'
      );
      setComparableDevice(JSON.parse(jsonModel));
    } catch (err) {
      console.log(err);
    }
    setIsLoading2(false);
  }

  useEffect(() => {
    _fetchcomparableDeviceJson();
  }, []);

  return (
    <DeviceComparisonContext.Provider value={{deviceModel, comparableDevice, isLoading, isLoading2}}>
      {children}
    </DeviceComparisonContext.Provider>
  );
};
