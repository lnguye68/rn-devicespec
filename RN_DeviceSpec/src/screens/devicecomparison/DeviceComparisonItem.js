import React, { useContext, useState, useEffect } from 'react';
import {
  View,
  TouchableNativeFeedback,
  StyleSheet,
  ToastAndroid
} from 'react-native';
import FastImage from 'react-native-fast-image';
import CustomText from '@components/CustomText';
import { isTablet } from 'react-native-device-info';
import { StringsContext } from '@contexts/StringsContext';
import { OrientationContext } from '@contexts/OrientationContext';
import { ThemeContext } from '@contexts/ThemeContext';


const DeviceComparisonItem = ({data, isCollapse, onPressItem, iconURI}) => {
  const { getString } = useContext(StringsContext);
  const { orientation } = useContext(OrientationContext);
  const { theme, DefaultTheme } = useContext(ThemeContext);
  const [ collapse, setCollapse ] = useState(true);
  const {
    id,
    deviceSpecItem,
    deviceSpec
  } = data;

  const onPress = () => {
    onPressItem(id);
  }


  const renderSection = function(columnModel) {
    if (!isCollapse) {
      let sectionViewArray = []
      if (columnModel) {
        for (var i = 0; i < columnModel.length; i+=2) {
          //TODO: Customize layout.
          switch (columnModel[i].layout) {
            case 'activity_device_compare_subitem':
              sectionViewArray.push(
                <View style={portStyles.subContainer}>
                  <View style={portStyles.leftCol}>
                    <CustomText
                      data={getString(columnModel[i].deviceSpec)}
                      fontColor={DefaultTheme[theme].fontColor}
                      fontSize={isTablet() ? 16 : 13} />
                    <CustomText
                      data={getString(columnModel[i+1].deviceSpec)}
                      fontColor={DefaultTheme[theme].fontColor}
                      fontSize={isTablet() ? 15 : 12} />
                  </View>
                  <View style={portStyles.rightCol}>
                    <CustomText
                      data={''}
                      fontColor={DefaultTheme[theme].fontColor}
                      fontSize={isTablet() ? 16 : 13} />
                    <CustomText
                      data={getString(columnModel[i+1].comparedDeviceSpec)}
                      fontColor={DefaultTheme[theme].fontColor}
                      fontSize={isTablet() ? 15 : 12} />
                  </View>
                </View>
              );

              break;
          }
        }
      }
      return <>{sectionViewArray}</>
    }
  }

  if (orientation === 'PORTRAIT') {
    return (
       <View style={portStyles.container}>
        <TouchableNativeFeedback onPress={onPress}>
          {/* Section Header */}
          <View style={portStyles.headerContainer}>
            <FastImage
              style={portStyles.iconStyle}
              source={{uri: iconURI}}
              resizeMode={FastImage.resizeMode.contain}>
            </FastImage>
            <CustomText
              data={getString(deviceSpecItem)}
              fontColor={DefaultTheme[theme].fontColor}
              fontSize={isTablet() ? 18: 14} />
          </View>
        </TouchableNativeFeedback>
        {/* Section View */}
        {renderSection(data.column1)}
       </View>
    );
  } else {
    let extraStyle = ''
    if (id == 0) {
      extraStyle = landStyles.extraTopMargin;
    }
    return (
       <View style={[landStyles.container, extraStyle]}>
        <TouchableNativeFeedback onPress={onPress}>
          {/* Section Header */}
          <View style={portStyles.headerContainer}>
            <FastImage
              style={portStyles.iconStyle}
              source={{uri: iconURI}}
              resizeMode={FastImage.resizeMode.contain}>
            </FastImage>
            <CustomText
              data={getString(deviceSpecItem)}
              fontColor={DefaultTheme[theme].fontColor}
              fontSize={isTablet() ? 18: 14} />
          </View>
        </TouchableNativeFeedback>
        {/* Section View */}
        {renderSection(data.column1)}
       </View>
    );
  }
};

//////////////// PORTRAIT ////////////////
const portStyles = StyleSheet.create({
  container: {
    alignItems: 'flex-start',
    marginStart: isTablet() ? '6%' : '8%',
    marginTop: isTablet() ? '3%' : '3%',
    marginBottom: isTablet() ? '3%' : '3%'
  },

  headerContainer: {
    flexDirection: 'row'
  },

  iconStyle: {
    width: isTablet() ? 15: 10,
    height: isTablet() ? 15: 10,
    marginEnd: isTablet() ? '2%': '3%',
    alignSelf: 'center'
  },

  subContainer: {
    flexDirection: 'row',
    marginTop: '2%',
    justifyContent: 'space-between'
  },

  leftCol: {
    flex: 1,
    marginStart: isTablet() ? '5%' : '7%',
    marginEnd: '2%'
  },

  rightCol: {
    flex: 1,
    marginStart: isTablet() ? '20%': '5%',
    marginEnd: '2%'
  }
});

//////////////// LANDSCAPE ////////////////
const landStyles = StyleSheet.create({
  container: {
    alignItems: 'flex-start',
    marginStart: '8%',
    marginTop: isTablet() ? '2%' : '1%',
    marginBottom: isTablet() ? '2%' : '1%'
  },

  extraTopMargin: {
    marginTop: isTablet() ? '24%' : '12%'
  }
});

export default DeviceComparisonItem;
