import React from 'react';
import {requireNativeComponent} from 'react-native';
import PropTypes from 'prop-types';

class DeviceComparisonView extends React.Component {
  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }

  onClick(event) {
    this.props.onClick();
  }

  render() {
    return (
      <NativeDeviceComparisonView
        {...this.props}
        leftDeviceImg={this.props.leftDeviceImg}
        rightDeviceImg={this.props.rightDeviceImg}
        xIconURI={this.props.xIconURI}
        deviceNameColor={this.props.deviceNameColor}
        onClick={this.onClick}
      />
    );
  }
}

DeviceComparisonView.propTypes = {
  leftDeviceImg: PropTypes.string,
  rightDeviceImg: PropTypes.string,
  xIconURI: PropTypes.string,
  deviceNameColor: PropTypes.string,
  onClick: PropTypes.func,
};

var NativeDeviceComparisonView = requireNativeComponent(
  'DeviceComparisonView',
  DeviceComparisonView,
);

export default DeviceComparisonView;
