import React, {useEffect, useContext, useState} from 'react';
import {View, Text, StyleSheet, TouchableNativeFeedback} from 'react-native';
import JsonModule from '../../modules/JsonModule';
import {FlatList} from 'react-native-gesture-handler';
import FullSpecItem from './FullSpecItem';
import {StringsContext} from '../../contexts/StringsContext';
import {OrientationContext} from '../../contexts/OrientationContext';
import CustomText from '../../components/CustomText';
import FastImage from 'react-native-fast-image';

export default function FullSpec({navigation}) {
  const {getString} = useContext(StringsContext);
  const {orientation} = useContext(OrientationContext);
  const [mSpecModel, setSpecModel] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const numbColumns = orientation === 'PORTRAIT' ? 1 : 2;

  useEffect(() => {
    JsonModule.getJsonModelText(
      'model/fullspec/galaxy_hubble_z_full_spec.json',
      value => {
        const jsonObject = JSON.parse(value);
        setSpecModel(jsonObject);
        setIsLoading(false);
      },
      err => {
        setIsLoading(false);
        console.log(err);
      },
    );
  }, [isLoading]);

  const renderLoadingText = function() {
    return (
      <View style={styles.loadingContainer}>
        <Text>Loading...</Text>
      </View>
    );
  };

  const renderSeeKeySpecText = function() {
    if (orientation === 'PORTRAIT') {
      return (
        <View>
          <TouchableNativeFeedback onPress={() => navigation.goBack()}>
            <View style={portStyles.seeKeySpec}>
              <CustomText data={getString('specs_z_device_1')} />
              <FastImage
                style={portStyles.seeKeySpecChevron}
                source={{uri: 'arrow_chevron_up'}}
                resizeMode={FastImage.resizeMode.contain}
              />
            </View>
          </TouchableNativeFeedback>
        </View>
      );
    } else {
      return (
        <View>
          <TouchableNativeFeedback onPress={() => navigation.goBack()}>
            <View style={landStyles.seeKeySpec}>
              <CustomText data={getString('specs_z_device_1')} />
              <FastImage
                style={landStyles.seeKeySpecChevron}
                source={{uri: 'arrow_chevron_up'}}
                resizeMode={FastImage.resizeMode.contain}
              />
            </View>
          </TouchableNativeFeedback>
        </View>
      );
    }
  };

  if (orientation === 'PORTRAIT') {
    return (
      <View style={portStyles.container}>
        {!isLoading ? (
          <View>
            {renderSeeKeySpecText()}
            <View>
              <FlatList
                data={mSpecModel}
                keyExtractor={item => item.deviceSpecItem}
                renderItem={({item, index}) => (
                  <FullSpecItem
                    data={item}
                    index={index}
                    dataSize={mSpecModel.length}
                  />
                )}
              />
              <View style={portStyles.viewLegalContainer_port}>
                <CustomText data={getString('specs_z_device_9')} />
              </View>
            </View>
          </View>
        ) : (
          <View>{renderLoadingText()}</View>
        )}
      </View>
    );
  } else {
    return (
      <View style={landStyles.container}>
        {!isLoading ? (
          <View>
            {renderSeeKeySpecText()}
            <FlatList
              style={landStyles.list_land}
              numColumns={numbColumns}
              key={numbColumns}
              data={mSpecModel}
              keyExtractor={item => item.deviceSpecItem}
              renderItem={({item, index}) => (
                <FullSpecItem
                  data={item}
                  index={index}
                  dataSize={mSpecModel.length}
                />
              )}
            />
            <View style={landStyles.viewLegalContainer_land}>
              <CustomText data={getString('specs_z_device_9')} />
            </View>
          </View>
        ) : (
          <View>{renderLoadingText()}</View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  listItemSeparator: {
    width: 20,
    backgroundColor: 'grey',
  },
  loadingContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const portStyles = StyleSheet.create({
  container: {
    flex: 1,
  },
  seeKeySpec: {
    marginTop: 30,
    justifyContent: 'center',
    alignContent: 'center',
    marginHorizontal: 120,
    flexDirection: 'row',
  },
  seeKeySpecChevron: {
    width: 20,
    height: 9,
    marginTop: 5,
    marginLeft: 5,
  },
  list_land: {
    height: '80%',
  },
  item: {
    flexDirection: 'row',
  },
  title: {
    fontSize: 32,
  },
  viewLegalContainer_port: {
    marginTop: 30,
    alignSelf: 'center',
    borderBottomColor: 'black',
    borderBottomWidth: 1,
  },
  viewLegalContainer_land: {
    marginTop: 10,
    alignSelf: 'center',
    borderBottomColor: 'black',
    borderBottomWidth: 1,
  },
});

const landStyles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 60,
  },
  seeKeySpec: {
    marginTop: 15,
    justifyContent: 'center',
    alignContent: 'center',
    marginHorizontal: 120,
    flexDirection: 'row',
  },
  seeKeySpecChevron: {
    width: 20,
    height: 9,
    marginTop: 5,
    marginLeft: 5,
  },
  list_land: {
    height: '80%',
  },
  item: {
    flexDirection: 'row',
  },
  title: {
    fontSize: 32,
  },
  viewLegalContainer_port: {
    marginTop: 30,
    alignSelf: 'center',
    borderBottomColor: 'black',
    borderBottomWidth: 1,
  },
  viewLegalContainer_land: {
    marginTop: 10,
    alignSelf: 'center',
    borderBottomColor: 'black',
    borderBottomWidth: 1,
  },
  loadingContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});