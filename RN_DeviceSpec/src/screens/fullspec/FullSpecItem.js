import React, {useContext, useState} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {StringsContext} from '@contexts/StringsContext';
import {OrientationContext} from '@contexts/OrientationContext';
import {FlatList, TouchableWithoutFeedback} from 'react-native-gesture-handler';
import CustomText from '@components/CustomText';
import FastImage from 'react-native-fast-image';
import {ThemeContext} from '@contexts/ThemeContext';
import {isTablet} from 'react-native-device-info';
import AnalyticsModule from '@modules/AnalyticsModule';

export default function FullSpecItem({
  model,
  index,
  isExpanded,
  firstSpecExpanded,
}) {
  const TAG = 'FullSpecItem';
  const {getString} = useContext(StringsContext);
  const {orientation} = useContext(OrientationContext);
  const {theme, DefaultTheme} = useContext(ThemeContext);
  const [mIsExpanded, setIsExpanded] = useState(isExpanded);

  const {
    deviceSpecItem,
    deviceSpecFullItemLeft,
    deviceSpecFullItemLeftTitle,
    deviceSpecFullItemRight,
    deviceSpecFullItemRightTitle,
    data,
  } = model;

  const _toggleExpandedState = () => {
    if (mIsExpanded) {
      AnalyticsModule.sendUserEvent(
        AnalyticsModule.TAP,
        'Collapse feature ' + deviceSpecItem,
      );
    } else {
      AnalyticsModule.sendUserEvent(
        AnalyticsModule.TAP,
        'Expand feature ' + deviceSpecItem,
      );
    }
    setIsExpanded(!mIsExpanded);
  };

  const _getLeftIconBaseOnTheme = () => {
    if (mIsExpanded) {
      if (theme === 'light') {
        return 'blue_minus_icon';
      } else {
        return 'white_minus_icon';
      }
    } else {
      if (theme === 'light') {
        return 'blue_plus_icon';
      } else {
        return 'white_plus_icon';
      }
    }
  };

  const _renderCameraSpecsDetail = () => {
    if (mIsExpanded) {
      if (orientation === 'PORTRAIT') {
        return (
          <View style={styles.two_columns_container}>
            <View style={styles.leftColumn}>
              <CustomText
                data={getString(deviceSpecFullItemLeftTitle)}
                fontColor={DefaultTheme[theme].fontColor}
                fontSize={isTablet() ? 16 : null}
              />
              <CustomText
                data={getString(deviceSpecFullItemLeft)}
                fontColor={DefaultTheme[theme].fontColor}
                fontSize={isTablet() ? 15 : null}
              />
            </View>
            <View style={styles.rightColumn}>
              <CustomText
                data={getString(deviceSpecFullItemRightTitle)}
                fontColor={DefaultTheme[theme].fontColor}
                fontSize={isTablet() ? 15 : null}
              />
              <CustomText
                data={getString(deviceSpecFullItemRight)}
                fontColor={DefaultTheme[theme].fontColor}
              />
            </View>
          </View>
        );
      } else {
        if (isTablet()) {
          return (
            <View style={styles.two_columns_container}>
              <View style={styles.leftColumn}>
                <View style={tabletStyles.topText}>
                  <CustomText
                    data={getString(deviceSpecFullItemLeftTitle)}
                    fontColor={DefaultTheme[theme].fontColor}
                    fontSize={isTablet() ? 16 : null}
                  />
                </View>
                <View style={tabletStyles.bottomText}>
                  <CustomText
                    data={getString(deviceSpecFullItemLeft)}
                    fontColor={DefaultTheme[theme].fontColor}
                    fontSize={isTablet() ? 15 : null}
                  />
                </View>
              </View>
              <View style={styles.rightColumn}>
                <View style={tabletStyles.topText}>
                  <CustomText
                    data={getString(deviceSpecFullItemRightTitle)}
                    fontColor={DefaultTheme[theme].fontColor}
                    fontSize={isTablet() ? 15 : null}
                  />
                </View>
                <View style={tabletStyles.bottomText}>
                  <CustomText
                    data={getString(deviceSpecFullItemRight)}
                    fontColor={DefaultTheme[theme].fontColor}
                  />
                </View>
              </View>
            </View>
          );
        } else {
          return (
            <View style={styles.one_columns_container}>
              <View style={styles.topText}>
                <CustomText
                  style={styles.topText}
                  data={getString(deviceSpecFullItemLeftTitle)}
                  fontColor={DefaultTheme[theme].fontColor}
                  fontSize={isTablet() ? 16 : null}
                />
              </View>
              <View style={styles.bottomText}>
                <CustomText
                  data={getString(deviceSpecFullItemLeft)}
                  fontColor={DefaultTheme[theme].fontColor}
                  fontSize={isTablet() ? 15 : null}
                />
              </View>

              <View style={styles.topText}>
                <CustomText
                  data={getString(deviceSpecFullItemRightTitle)}
                  fontColor={DefaultTheme[theme].fontColor}
                  fontSize={isTablet() ? 15 : null}
                />
              </View>
              <View style={styles.bottomText}>
                <CustomText
                  data={getString(deviceSpecFullItemRight)}
                  fontColor={DefaultTheme[theme].fontColor}
                />
              </View>
            </View>
          );
        }
      }
    } else {
      if (firstSpecExpanded) {
        setIsExpanded(true);
        if (isTablet) {
          return (
            <View style={styles.two_columns_container}>
              <View style={styles.leftColumn}>
                <View style={tabletStyles.topText}>
                  <CustomText
                    data={getString(deviceSpecFullItemLeftTitle)}
                    fontColor={DefaultTheme[theme].fontColor}
                    fontSize={isTablet() ? 16 : null}
                  />
                </View>
                <View style={tabletStyles.bottomText}>
                  <CustomText
                    data={getString(deviceSpecFullItemLeft)}
                    fontColor={DefaultTheme[theme].fontColor}
                    fontSize={isTablet() ? 15 : null}
                  />
                </View>
              </View>
              <View style={styles.rightColumn}>
                <View style={tabletStyles.topText}>
                  <CustomText
                    data={getString(deviceSpecFullItemRightTitle)}
                    fontColor={DefaultTheme[theme].fontColor}
                    fontSize={isTablet() ? 15 : null}
                  />
                </View>
                <View style={tabletStyles.bottomText}>
                  <CustomText
                    data={getString(deviceSpecFullItemRight)}
                    fontColor={DefaultTheme[theme].fontColor}
                  />
                </View>
              </View>
            </View>
          );
        } else {
          return (
            <View style={styles.two_columns_container}>
              <View style={styles.leftColumn}>
                <CustomText
                  data={getString(deviceSpecFullItemLeftTitle)}
                  fontColor={DefaultTheme[theme].fontColor}
                  fontSize={isTablet() ? 16 : null}
                />
                <CustomText
                  data={getString(deviceSpecFullItemLeft)}
                  fontColor={DefaultTheme[theme].fontColor}
                  fontSize={isTablet() ? 15 : null}
                />
              </View>
              <View style={styles.rightColumn}>
                <CustomText
                  data={getString(deviceSpecFullItemRightTitle)}
                  fontColor={DefaultTheme[theme].fontColor}
                  fontSize={isTablet() ? 16 : null}
                />
                <CustomText
                  data={getString(deviceSpecFullItemRight)}
                  fontColor={DefaultTheme[theme].fontColor}
                  fontSize={isTablet() ? 15 : null}
                />
              </View>
            </View>
          );
        }
      } else {
        setIsExpanded(false);
        return <View />;
      }
    }
  };

  const _renderSpecsDetail = () => {
    if (mIsExpanded) {
      if (orientation === 'PORTRAIT') {
        return (
          <FlatList
            data={data}
            listKey={item => item.deviceSpecFullItemLeft}
            renderItem={({item, itemIndex}) => (
              <View
                key={item.deviceSpecFullItemLeft}
                style={styles.two_columns_container}>
                <View style={styles.leftColumn}>
                  <CustomText
                    data={getString(item.deviceSpecFullItemLeft)}
                    fontColor={DefaultTheme[theme].fontColor}
                  />
                </View>
                <View style={styles.rightColumn}>
                  <CustomText
                    data={getString(item.deviceSpecFullItemRight)}
                    fontColor={DefaultTheme[theme].fontColor}
                  />
                </View>
              </View>
            )}
          />
        );
      } else {
        return (
          <FlatList
            data={data}
            listKey={item => item.deviceSpecFullItemLeft}
            renderItem={({item, itemIndex}) => (
              <View
                key={item.deviceSpecFullItemLeft}
                style={styles.one_columns_container}>
                <View style={styles.topText}>
                  <CustomText
                    data={getString(item.deviceSpecFullItemLeft)}
                    fontColor={DefaultTheme[theme].fontColor}
                  />
                </View>
                <View style={styles.bottomText}>
                  <CustomText
                    data={getString(item.deviceSpecFullItemRight)}
                    fontColor={DefaultTheme[theme].fontColor}
                  />
                </View>
              </View>
            )}
          />
        );
      }
    } else {
      return <View />;
    }
  };

  const _renderContentPort = () => {
    if (index === 0) {
      //Camera specs. We need left and right column.
      return (
        <View
          style={[
            styles.spec_item_container,
            mIsExpanded ? styles.expandedMarginBottom : styles.hasBottomMargin,
          ]}>
          <View style={styles.touchable_container}>
            <TouchableWithoutFeedback
              style={styles.textWithLeftIcon}
              onPress={_toggleExpandedState}>
              <FastImage
                style={styles.leftIcon}
                source={{uri: _getLeftIconBaseOnTheme()}}
                resizeMode={FastImage.resizeMode.contain}
              />
              <CustomText
                data={getString(deviceSpecItem)}
                fontColor={DefaultTheme[theme].fontColor}
                fontSize={isTablet() ? 18 : null}
              />
            </TouchableWithoutFeedback>
          </View>
          <_renderCameraSpecsDetail />
        </View>
      );
    } else {
      return (
        <View
          style={[
            styles.spec_item_container,
            mIsExpanded ? styles.expandedMarginBottom : styles.hasBottomMargin,
          ]}>
          <View style={styles.touchable_container}>
            <TouchableWithoutFeedback
              style={styles.textWithLeftIcon}
              onPress={_toggleExpandedState}>
              <FastImage
                style={styles.leftIcon}
                source={{uri: _getLeftIconBaseOnTheme()}}
                resizeMode={FastImage.resizeMode.contain}
              />
              <CustomText
                data={getString(deviceSpecItem)}
                fontColor={DefaultTheme[theme].fontColor}
                fontSize={isTablet() ? 18 : null}
              />
            </TouchableWithoutFeedback>
          </View>
          <_renderSpecsDetail />
        </View>
      );
    }
  };

  const _renderContentLand = () => {
    if (index === 0) {
      //Camera specs. We need left and right column.
      return (
        <View
          style={[
            landStyles.spec_item_container,
            mIsExpanded
              ? landStyles.expandedMarginBottom
              : landStyles.marginBottom,
          ]}>
          <View style={styles.touchable_container}>
            <TouchableWithoutFeedback
              style={styles.textWithLeftIcon}
              onPress={_toggleExpandedState}>
              <FastImage
                style={styles.leftIcon}
                source={{uri: _getLeftIconBaseOnTheme()}}
                resizeMode={FastImage.resizeMode.contain}
              />
              <CustomText
                data={getString(deviceSpecItem)}
                fontColor={DefaultTheme[theme].fontColor}
                fontSize={isTablet() ? 18 : null}
              />
            </TouchableWithoutFeedback>
          </View>
          <_renderCameraSpecsDetail />
        </View>
      );
    } else {
      return (
        <View
          style={[
            landStyles.spec_item_container,
            mIsExpanded
              ? landStyles.expandedMarginBottom
              : landStyles.marginBottom,
          ]}>
          <View style={styles.touchable_container}>
            <TouchableWithoutFeedback
              style={styles.textWithLeftIcon}
              onPress={_toggleExpandedState}>
              <FastImage
                style={styles.leftIcon}
                source={{uri: _getLeftIconBaseOnTheme()}}
                resizeMode={FastImage.resizeMode.contain}
              />
              <CustomText
                data={getString(deviceSpecItem)}
                fontColor={DefaultTheme[theme].fontColor}
                fontSize={isTablet() ? 18 : null}
              />
            </TouchableWithoutFeedback>
          </View>
          <_renderSpecsDetail />
        </View>
      );
    }
  };

  if (orientation === 'PORTRAIT') {
    return <>{_renderContentPort()}</>;
  } else {
    return <>{_renderContentLand()}</>;
  }
}

const styles = StyleSheet.create({
  container_port: {
    flex: 1,
  },
  container_land: {
    flex: 1,
  },
  content: {
    flex: 1,
    flexDirection: 'row',
  },
  touchable_container: {
    alignSelf: 'flex-start',
  },
  hasBottomMargin: {
    marginBottom: isTablet() ? '1%' : 0,
  },
  expandedMarginBottom: {
    marginBottom: isTablet() ? '2%' : '2%',
  },
  //Specs detail
  spec_item_container: {
    flex: 1,
    marginStart: isTablet() ? '6%' : '5%',
    marginEnd: isTablet() ? '6%' : '5%',
  },
  //Text with left icon
  textWithLeftIcon: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: isTablet() ? '2%' : '4%',
    marginTop: isTablet() ? '2%' : '4%',
    // borderColor: 'red',
    // borderWidth: 1,
  },
  leftIcon: {
    width: isTablet() ? 18 : 10,
    height: isTablet() ? 18 : 10,
    marginEnd: isTablet() ? 21 : '5%',
  },
  //One column
  one_columns_container: {
    flex: 1,
    flexWrap: 'wrap',
    // borderWidth: 1,
    // borderColor: 'red',
  },
  topText: {
    marginStart: isTablet() ? '8.8%' : '8%',
  },
  bottomText: {
    marginStart: isTablet() ? '16%' : '32%',
  },
  //Two columns
  two_columns_container: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    // borderWidth: 1,
  },
  leftColumn: {
    flex: 0.5,
    marginStart: '8%',
    alignContent: 'center',
    // borderWidth: 1,
  },
  rightColumn: {
    flex: 0.5,
    marginStart: '8%',
    alignContent: 'center',
    // borderWidth: 1,
    // borderColor: 'red',
  },
});

const landStyles = StyleSheet.create({
  marginBottom: {
    marginBottom: isTablet() ? '3%' : '6%',
  },
  expandedMarginBottom: {
    marginBottom: isTablet() ? '3%' : '5%',
  },
  //Specs detail
  spec_item_container: {
    flex: 1,
    marginEnd: '5%',
    // borderWidth: 1,
    // borderColor: 'blue',
  },
});

const tabletStyles = StyleSheet.create({
  topText: {
    marginStart: '12%',
  },
  bottomText: {
    marginStart: '32%',
  },
});
