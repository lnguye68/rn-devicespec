/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  FlatList,
  BackHandler,
} from 'react-native';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {OrientationContext} from '@contexts/OrientationContext';
import {StringsContext} from '@contexts/StringsContext';
import {ThemeContext} from '@contexts/ThemeContext';
import {
  FullSpecScreenContext,
  FullSpecScreenProvider,
} from './FullSpecScreenContext';
import LoadingIndicator from '@components/LoadingIndicator';
import CustomText from '@components/CustomText';
import FastImage from 'react-native-fast-image';
import {isTablet} from 'react-native-device-info';
import {SCREEN_NAMES} from '@utils/Constants';
import FullSpecItem from './FullSpecItem';
import ShadowView from 'react-native-simple-shadow-view';
import {timer} from 'rxjs';
import LegalScreen from '@screens/legal/LegalScreen';
import AnalyticsModule from '@modules/AnalyticsModule';
import {useIsFocused} from '@react-navigation/native';

export default function FullSpecScreen({route, navigation}) {
  const TAG = 'FullSpecScreen';
  const CARDVIEW_CORNER_RADIUS = 10;
  const isFocused = useIsFocused();

  const {prevScreen} = route.params;

  const {orientation} = useContext(OrientationContext);
  const {getString} = useContext(StringsContext);
  const {theme, DefaultTheme} = useContext(ThemeContext);
  const [mIsAllSpecExpanded, setAllSpecExpanded] = useState(false);
  const [mIsFirstSpecExpanded, setFirstSpecExpanded] = useState(true);
  const legalRef = useRef(null);

  useEffect(() => {
    timer(5000).subscribe(() => setFirstSpecExpanded(false));
  }, []);

  useEffect(() => {
    const _backAction = () => {
      _goBack();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      _backAction,
    );
    return () => backHandler.remove();
  }, []);

  const _goBack = () => {
    AnalyticsModule.sendUserEvent(AnalyticsModule.TAP, 'Tap Back button');
    AnalyticsModule.sendScreenChangeEvent(
      AnalyticsModule.TAP,
      SCREEN_NAMES.FULL_SPEC,
      prevScreen,
    );
    navigation.navigate(SCREEN_NAMES.HOME);
  };

  const _goToLegalScreen = () => {
    legalRef.current?.open();
    AnalyticsModule.sendScreenChangeEvent(
      AnalyticsModule.TAP,
      SCREEN_NAMES.FULL_SPEC /* current screen name */,
      SCREEN_NAMES.FULL_SPEC + '_' + SCREEN_NAMES.LEGAL /* next screen name */,
    );
    // navigation.navigate(SCREEN_NAMES.HOME_TEST);
  };

  const _getExpandAllTextIcon = () => {
    if (mIsAllSpecExpanded) {
      if (theme === 'light') {
        return 'expand_all_minus_icon_blue';
      } else {
        return 'expand_all_minus_icon_white';
      }
    } else {
      if (theme === 'light') {
        return 'blue_plus_icon';
      } else {
        return 'white_plus_icon';
      }
    }
  };

  const _getCardViewTitleTextSize = () => {
    if (isTablet()) {
      return 18;
    } else {
      return null;
    }
  };

  const _getCardViewSubtitleTextSize = () => {
    if (isTablet()) {
      return 15;
    } else {
      return null;
    }
  };

  const _renderTextWithRightIcon = ({
    containerStyle,
    textUri,
    iconUri,
    iconStyle,
    fontSize,
    fontColor,
  }) => {
    return (
      <View style={[styles.textWithRightIcon, containerStyle]}>
        <CustomText
          fontColor={fontColor}
          data={getString(textUri)}
          fontSize={fontSize}
        />
        <FastImage
          style={iconStyle}
          source={{uri: iconUri}}
          resizeMode={FastImage.resizeMode.contain}
        />
      </View>
    );
  };

  const _renderTextWithLeftIcon = ({
    containerStyle,
    textUri,
    iconUri,
    iconStyle,
    fontSize,
  }) => {
    return (
      <View style={[styles.textWithLeftIcon, containerStyle]}>
        <FastImage
          style={iconStyle}
          source={{uri: iconUri}}
          resizeMode={FastImage.resizeMode.contain}
        />
        <CustomText
          data={getString(textUri)}
          fontColor={DefaultTheme[theme].fontColor}
          fontSize={fontSize}
        />
      </View>
    );
  };

  const _renderFullSpec = () => {
    const context = useContext(FullSpecScreenContext);

    if (orientation === 'PORTRAIT') {
      return (
        <>
          {context.isLoading ? (
            <>
              {console.log(TAG + ': loading...')}
              <LoadingIndicator color="black" />
            </>
          ) : (
            <>
              {console.log(TAG + ': rendering list...')}
              <FlatList
                data={context.fullSpecModel}
                listKey={item => item.deviceSpecItem}
                renderItem={({item, index}) => (
                  <FullSpecItem
                    model={item}
                    index={index}
                    isExpanded={mIsAllSpecExpanded}
                    firstSpecExpanded={mIsFirstSpecExpanded}
                  />
                )}
              />
            </>
          )}
        </>
      );
    } else {
      return (
        <>
          {context.isLoading ? (
            <>
              {console.log(TAG + ': loading...')}
              <LoadingIndicator color="black" />
            </>
          ) : (
            <>
              {console.log(TAG + ': rendering list...')}
              <FlatList
                numColumns={isTablet() ? 1 : 2}
                data={context.fullSpecModel}
                listKey={item => item.deviceSpecItem}
                keyExtractor={item => item.deviceSpecItem}
                renderItem={({item, index}) => (
                  <FullSpecItem
                    model={item}
                    index={index}
                    isExpanded={mIsAllSpecExpanded}
                    firstSpecExpanded={mIsFirstSpecExpanded}
                  />
                )}
              />
            </>
          )}
        </>
      );
    }
  };

  const _renderSectionLegal = () => {
    return (
      <>
        <TouchableWithoutFeedback style={styles.backButton} onPress={_goBack}>
          <_renderTextWithLeftIcon
            textUri={'screen_fullspec_back'}
            fontSize={isTablet() ? 15 : null}
            fontColor={DefaultTheme[theme].fontColor}
            iconUri={theme === 'light' ? 'back_arrow' : 'back_arrow_white'}
            iconStyle={{
              width: isTablet() ? 16 : 12,
              height: isTablet() ? 16 : 12,
              marginEnd: isTablet() ? '9%' : '5%',
            }}
          />
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={_goToLegalScreen}>
          <_renderTextWithRightIcon
            containerStyle={styles.legalButton}
            textUri={'screen_fullspec_legal'}
            fontSize={isTablet() ? 15 : null}
            fontColor={DefaultTheme[theme].fontColor}
            iconUri={theme === 'light' ? 'black_plus_icon' : 'white_plus_icon'}
            iconStyle={{
              width: isTablet() ? 16 : 12,
              height: isTablet() ? 16 : 12,
              marginStart: isTablet() ? '9%' : '5%',
            }}
          />
        </TouchableWithoutFeedback>
      </>
    );
  };

  const _onViewPopularFeaturesPress = () => {
    AnalyticsModule.sendScreenChangeEvent(
      AnalyticsModule.TAP,
      SCREEN_NAMES.FULL_SPEC,
      SCREEN_NAMES.HOME,
    );
    navigation.navigate(SCREEN_NAMES.HOME);
  };

  const _onCompareDevicesPress = () => {
    AnalyticsModule.sendScreenChangeEvent(
      AnalyticsModule.TAP,
      SCREEN_NAMES.FULL_SPEC,
      SCREEN_NAMES.CHOOSE_DEVICE,
    );
    navigation.navigate(SCREEN_NAMES.CHOOSE_DEVICE, {
      prevScreen: SCREEN_NAMES.FULL_SPEC,
    });
  };

  const _onExpandCollapsePress = () => {
    if (mIsAllSpecExpanded) {
      AnalyticsModule.sendUserEvent(
        AnalyticsModule.TAP,
        'Tap Collapse all button.',
      );
    } else {
      AnalyticsModule.sendUserEvent(
        AnalyticsModule.TAP,
        'Tap Expand all button.',
      );
    }
    setAllSpecExpanded(!mIsAllSpecExpanded);
  };

  const _renderRightThumbnailCardview = ({title, subtitle, onPress}) => {
    if (orientation === 'PORTRAIT') {
      return (
        <TouchableWithoutFeedback onPress={onPress}>
          <View
            style={styles.rightThumbnailCardView}
            cornerRadius={CARDVIEW_CORNER_RADIUS}>
            <View style={styles.rightThumbnailCardViewText}>
              <_renderTextWithRightIcon
                textUri={title}
                fontSize={_getCardViewTitleTextSize()}
                fontColor={DefaultTheme[theme].fontColor}
                iconUri={
                  theme === 'light'
                    ? 'arrow_chevron_right_black'
                    : 'arrow_chevron_right_white'
                }
                iconStyle={{
                  width: isTablet() ? 18 : 12,
                  height: isTablet() ? 18 : 12,
                  marginStart: isTablet() ? '5%' : '4%',
                }}
              />
              <CustomText
                data={getString(subtitle)}
                fontColor={DefaultTheme[theme].fontColor}
                fontSize={_getCardViewSubtitleTextSize()}
              />
            </View>
            <TouchableWithoutFeedback
              style={styles.cardButton}
              onPress={onPress}
            />
            <FastImage
              style={styles.cardviewRightImage}
              source={{uri: 'compare_devices_image'}}
              resizeMode={FastImage.resizeMode.contain}
            />
          </View>
        </TouchableWithoutFeedback>
      );
    } else {
      return (
        <TouchableWithoutFeedback onPress={onPress}>
          <View
            style={landStyles.rightThumbnailCardView}
            cornerRadius={CARDVIEW_CORNER_RADIUS}>
            <View style={landStyles.rightThumbnailCardViewText}>
              <_renderTextWithRightIcon
                containerStyle={landStyles.cardViewTitleContainer}
                textUri={title}
                fontSize={_getCardViewTitleTextSize()}
                fontColor={DefaultTheme[theme].fontColor}
                iconUri={
                  theme === 'light'
                    ? 'arrow_chevron_right_black'
                    : 'arrow_chevron_right_white'
                }
                iconStyle={{
                  width: isTablet() ? 18 : 12,
                  height: isTablet() ? 18 : 12,
                  marginStart: isTablet() ? '3%' : '4%',
                }}
              />
              <CustomText
                data={getString(subtitle)}
                fontColor={DefaultTheme[theme].fontColor}
                fontSize={_getCardViewSubtitleTextSize()}
                lineHeight={isTablet() ? 22 : null}
              />
            </View>
            <TouchableWithoutFeedback
              style={styles.cardButton}
              onPress={onPress}
            />
            <FastImage
              style={landStyles.cardviewRightImage}
              source={{uri: 'compare_devices_image'}}
              resizeMode={FastImage.resizeMode.contain}
            />
          </View>
        </TouchableWithoutFeedback>
      );
    }
  };

  const _renderLeftThumbnailCardview = ({title, subtitle, onPress}) => {
    if (orientation === 'PORTRAIT') {
      return (
        <TouchableWithoutFeedback onPress={onPress}>
          <View
            style={[styles.leftThumbnailCardView]}
            cornerRadius={CARDVIEW_CORNER_RADIUS}>
            <FastImage
              style={styles.cardviewLeftImage}
              source={{uri: 'popular_specs_image'}}
              resizeMode={FastImage.resizeMode.contain}
            />
            <View style={styles.leftThumbnailCardViewText}>
              <_renderTextWithRightIcon
                textUri={title}
                fontSize={_getCardViewTitleTextSize()}
                fontColor={DefaultTheme[theme].fontColor}
                iconUri={
                  theme === 'light'
                    ? 'arrow_chevron_right_black'
                    : 'arrow_chevron_right_white'
                }
                iconStyle={{
                  width: isTablet() ? 15 : 12,
                  height: isTablet() ? 15 : 12,
                  marginStart: isTablet() ? '5%' : '4%',
                }}
              />
              <CustomText
                data={getString(subtitle)}
                fontColor={DefaultTheme[theme].fontColor}
                fontSize={_getCardViewSubtitleTextSize()}
              />
            </View>
          </View>
        </TouchableWithoutFeedback>
      );
    } else {
      return (
        <TouchableWithoutFeedback onPress={onPress}>
          <View
            style={landStyles.leftThumbnailCardView}
            cornerRadius={CARDVIEW_CORNER_RADIUS}>
            <FastImage
              style={landStyles.cardviewLeftImage}
              source={{uri: 'popular_specs_image'}}
              resizeMode={FastImage.resizeMode.contain}
            />
            <View style={landStyles.leftThumbnailCardViewText}>
              <_renderTextWithRightIcon
                textUri={title}
                fontSize={_getCardViewTitleTextSize()}
                fontColor={DefaultTheme[theme].fontColor}
                iconUri={
                  theme === 'light'
                    ? 'arrow_chevron_right_black'
                    : 'arrow_chevron_right_white'
                }
                iconStyle={{
                  width: isTablet() ? 18 : 12,
                  height: isTablet() ? 18 : 12,
                  marginStart: isTablet() ? '3%' : '5%',
                }}
              />
              <CustomText
                data={getString(subtitle)}
                fontColor={DefaultTheme[theme].fontColor}
                fontSize={_getCardViewSubtitleTextSize()}
              />
            </View>
            <TouchableWithoutFeedback
              style={styles.cardButton}
              onPress={onPress}
            />
          </View>
        </TouchableWithoutFeedback>
      );
    }
  };

  const _renderLandscapeLayout = function() {
    return (
      <View
        style={[
          landStyles.rootContainer,
          {backgroundColor: DefaultTheme[theme].backgroundColor},
        ]}>
        <View
          style={[
            landStyles.sectionLegalContainer,
            {backgroundColor: DefaultTheme[theme].backgroundColor},
          ]}>
          <_renderSectionLegal />
        </View>
        <View style={landStyles.title}>
          <CustomText
            data={getString('screen_fullspec_all_specifications')}
            fontColor={DefaultTheme[theme].fontColor}
            fontSize={isTablet() ? 30 : null}
          />
        </View>
        <View style={landStyles.contentLand}>
          <ShadowView
            style={[
              landStyles.full_spec_shadow_view,
              {backgroundColor: DefaultTheme[theme].cardColor},
            ]}>
            <View
              style={[
                landStyles.full_spec_container,
                mIsAllSpecExpanded
                  ? landStyles.noStyle
                  : styles.hasPaddingBottom,
                {backgroundColor: DefaultTheme[theme].cardColor},
              ]}>
              <View
                style={[
                  landStyles.full_spec_content,
                  mIsAllSpecExpanded
                  ? landStyles.expandedPaddingBottom
                  : landStyles.hasPaddingBottom,
                  {backgroundColor: DefaultTheme[theme].cardColor},
                ]}>
                <_renderFullSpec />
              </View>
            </View>
            <View style={landStyles.expandAllText}>
              <TouchableWithoutFeedback onPress={_onExpandCollapsePress}>
                <_renderTextWithRightIcon
                  textUri={
                    mIsAllSpecExpanded
                      ? 'screen_fullspec_collapse_all'
                      : 'screen_fullspec_expand_all'
                  }
                  fontSize={isTablet() ? 12 : 11}
                  fontColor={DefaultTheme[theme].fontColor}
                  iconUri={_getExpandAllTextIcon()}
                  iconStyle={{
                    width: isTablet() ? 15 : 8,
                    height: isTablet() ? 15 : 8,
                    marginStart: isTablet() ? '5%' : '4%',
                  }}
                />
              </TouchableWithoutFeedback>
            </View>
          </ShadowView>

          <View style={landStyles.rightColumn}>
            <ShadowView
              style={[
                landStyles.shadow_view,
                landStyles.hasBottomMargin,
                {backgroundColor: DefaultTheme[theme].cardColor},
              ]}>
              <View style={landStyles.cardViewContainer}>
                <_renderRightThumbnailCardview
                  title={'screen_fullspec_compare_devices'}
                  subtitle={
                    isTablet()
                      ? 'screen_fullspec_compare_devices_subtitle_tablet'
                      : 'screen_fullspec_compare_devices_subtitle'
                  }
                  onPress={_onCompareDevicesPress}
                />
              </View>
            </ShadowView>
            <ShadowView
              style={[
                landStyles.shadow_view,
                {backgroundColor: DefaultTheme[theme].cardColor},
              ]}>
              <View style={landStyles.cardViewContainer}>
                <_renderLeftThumbnailCardview
                  title={'screen_fullspec_view_popular_features'}
                  subtitle={'screen_fullspec_common_subtitle'}
                  onPress={_onViewPopularFeaturesPress}
                />
              </View>
            </ShadowView>
          </View>
        </View>
      </View>
    );
  };

  const _renderPortraitLayout = function() {
    return (
      <View
        style={[
          styles.rootContainer,
          {backgroundColor: DefaultTheme[theme].backgroundColor},
        ]}>
        <View
          style={[
            styles.sectionLegalContainer,
            {backgroundColor: DefaultTheme[theme].backgroundColor},
          ]}>
          <_renderSectionLegal />
        </View>
        <ScrollView>
          <View style={styles.title}>
            <CustomText
              data={getString('screen_fullspec_all_specifications')}
              fontColor={DefaultTheme[theme].fontColor}
              fontSize={isTablet() ? 30 : null}
            />
          </View>

          <View style={styles.full_spec_container}>
            <ShadowView
              style={[
                styles.shadow_view,
                {backgroundColor: DefaultTheme[theme].cardColor},
              ]}>
              <View
                style={[
                  styles.full_spec_content,
                  {backgroundColor: DefaultTheme[theme].cardColor},
                ]}>
                <_renderFullSpec />
              </View>
              <View style={styles.expandAllText}>
                <TouchableWithoutFeedback onPress={_onExpandCollapsePress}>
                  <_renderTextWithRightIcon
                    textUri={
                      mIsAllSpecExpanded
                        ? 'screen_fullspec_collapse_all'
                        : 'screen_fullspec_expand_all'
                    }
                    iconUri={_getExpandAllTextIcon()}
                    iconStyle={{
                      width: isTablet() ? 15 : 8,
                      height: isTablet() ? 15 : 8,
                      marginStart: isTablet() ? '6%' : '5%',
                    }}
                    fontColor={DefaultTheme[theme].expandAllTextFontColor}
                    fontSize={isTablet() ? 11 : null}
                  />
                </TouchableWithoutFeedback>
              </View>
            </ShadowView>
          </View>

          <View style={styles.cardViewSectionContainer}>
            <ShadowView
              style={[
                styles.shadow_view,
                {backgroundColor: DefaultTheme[theme].cardColor},
              ]}>
              <View style={styles.cardViewContainer}>
                <_renderRightThumbnailCardview
                  title={'screen_fullspec_compare_devices'}
                  subtitle={
                    isTablet()
                      ? 'screen_fullspec_compare_devices_subtitle_tablet'
                      : 'screen_fullspec_compare_devices_subtitle'
                  }
                  onPress={_onCompareDevicesPress}
                />
              </View>
            </ShadowView>

            <ShadowView
              style={[
                styles.shadow_view,
                {backgroundColor: DefaultTheme[theme].cardColor},
              ]}>
              <View style={styles.cardViewContainer}>
                <_renderLeftThumbnailCardview
                  title={'screen_fullspec_view_popular_features'}
                  subtitle={
                    isTablet()
                      ? 'screen_fullspec_common_subtitle_2'
                      : 'screen_fullspec_common_subtitle'
                  }
                  onPress={_onViewPopularFeaturesPress}
                />
              </View>
            </ShadowView>
          </View>
        </ScrollView>
      </View>
    );
  };
  if (isFocused) {
    if (orientation === 'PORTRAIT') {
      return (
        <FullSpecScreenProvider>
          <_renderPortraitLayout />
          <LegalScreen prevScreen={SCREEN_NAMES.FULL_SPEC} ref={legalRef} />
        </FullSpecScreenProvider>
      );
    } else {
      return (
        <FullSpecScreenProvider>
          <_renderLandscapeLayout />
          <LegalScreen prevScreen={SCREEN_NAMES.FULL_SPEC} ref={legalRef} />
        </FullSpecScreenProvider>
      );
    }
  } else {
    return <></>;
  }
}

const styles = StyleSheet.create({
  noStyle: {},
  rootContainer: {
    flex: 1,
  },
  textWithRightIcon: {
    // borderWidth: 1,
    // borderColor: 'green',
    flexDirection: 'row',
    alignItems: 'center',
  },
  textWithLeftIcon: {
    // borderWidth: 1,
    // borderColor: 'blue',
    flexDirection: 'row',
    alignItems: 'center',
  },

  //Portrait shadow
  shadow: {
    borderRadius: 15,
    flex: 1,
  },
  shadow_view: {
    borderRadius: 12,
    shadowColor: '#4D000000',
    shadowOpacity: 1,
    shadowRadius: 6,
    shadowOffset: {width: 0, height: 0},
    marginBottom: isTablet() ? '5%' : '7%',
  },
  //Screen title section
  title: {
    marginTop: isTablet() ? '6%' : '4%',
    marginStart: '5%',
  },
  //Full spec section
  full_spec_container: {
    flex: 1,
    marginTop: isTablet() ? '4%' : '5%',
    marginBottom: isTablet() ? '2%' : '1%',
    marginStart: isTablet() ? '8%' : '7%',
    marginEnd: isTablet() ? '8%' : '7%',
    borderRadius: 15,
    // borderWidth: 1,
    // borderColor: 'red',
  },
  full_spec_content: {
    marginStart: isTablet() ? '3%' : '4%',
    marginEnd: '3%',
    marginTop: isTablet() ? '7%' : '7%',
    marginBottom: isTablet() ? '7%' : '5%',
    // borderWidth: 1,
    // borderColor: 'blue',
    borderRadius: 15,
  },
  full_spec_cardview: {
    flex: 1,
  },
  expandAllText: {
    position: 'absolute',
    alignSelf: 'flex-end',
    marginTop: '8%',
    marginEnd: '8%',
  },
  //Cardview section
  cardViewSectionContainer: {
    marginStart: isTablet() ? '8%' : '7%',
    marginEnd: isTablet() ? '8%' : '7%',
  },
  cardViewContainer: {
    width: '100%',
    alignSelf: 'baseline',
    justifyContent: 'center',
    paddingVertical: '4%',
  },
  //Right thumbnail cardview
  rightThumbnailCardView: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingVertical: isTablet() ? '2%' : 0,
  },
  cardviewRightImage: {
    width: isTablet() ? 80 : 70,
    height: isTablet() ? 105 : 95,
    flex: 0.35,
    marginEnd: isTablet() ? '8%' : '8.5%',
    alignSelf: 'center',
  },
  rightThumbnailCardViewText: {
    flex: 0.65,
    marginTop: '4%',
    marginBottom: '4%',
    marginStart: isTablet() ? '9%' : '8%',
    marginEnd: isTablet() ? '6%' : '2%',
    alignSelf: 'center',
  },
  //Left thumbnail cardview
  leftThumbnailCardView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  cardviewLeftImage: {
    width: isTablet() ? 70 : 43,
    height: isTablet() ? 70 : 46,
    flex: 0.1,
    marginStart: isTablet() ? '8%' : '7%',
  },
  leftThumbnailCardViewText: {
    flex: 0.9,
    marginTop: '4%',
    marginBottom: '4%',
    marginStart: '6%',
    marginEnd: isTablet() ? '6%' : '4%',
  },
  //Legal text
  sectionLegalContainer: {
    position: 'absolute',
    bottom: 0,
    start: 0,
    end: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    // borderWidth: 1,
    // borderColor: 'red',
    marginBottom: isTablet() ? '5%' : '6%',
    marginStart: isTablet() ? '4%' : '4%',
    marginEnd: isTablet() ? '4%' : '4%',
  },
  legalButton: {
    justifyContent: 'flex-end',
  },
});

const landStyles = StyleSheet.create({
  rootContainer: {
    flex: 1,
  },
  expandedPaddingBottom: {
    paddingBottom: isTablet() ? '15%' : '12%',
  },
  hasPaddingBottom: {
    marginBottom: isTablet() ? '0%' : '0%',
  },
  hasBottomMargin: {
    marginBottom: isTablet() ? '6%' : '5%',
  },
  //Screen title section
  title: {
    marginTop: isTablet() ? '3.7%' : '1.6%',
    marginStart: isTablet() ? '4.5%' : '3%',
  },
  contentLand: {
    // borderWidth: 1,
    flexDirection: 'row',
    marginStart: isTablet() ? '4.5%' : '3%',
    marginEnd: isTablet() ? '4.8%' : '3%',
    marginTop: isTablet() ? '2.2%' : '2%',
  },
  rightColumn: {
    flex: isTablet() ? 0.87 : 0.8,
    flexDirection: 'column',
    // borderColor: 'red',
    // borderWidth: 1,
  },

  shadow_view: {
    borderRadius: 12,
    shadowColor: '#4D000000',
    shadowOpacity: 1,
    shadowRadius: 6,
    shadowOffset: {width: 0, height: 0},
    marginStart: isTablet() ? '8%' : '6%',
  },
  full_spec_shadow_view: {
    borderRadius: 12,
    shadowColor: '#4D000000',
    shadowOpacity: 1,
    shadowRadius: 6,
    shadowOffset: {width: 0, height: 0},
    flex: 1,
  },
  //Section full spec
  full_spec_container: {
    borderRadius: 12,
    backgroundColor: 'white',
    // borderWidth: 1,
    // borderColor: 'red',
    // marginBottom: isTablet() ? '10%' : '8%',
  },
  full_spec_content: {
    marginStart: isTablet() ? '9.5%' : '6.5%',
    marginEnd: '5%',
    marginTop: isTablet() ? '8%' : '7%',
    backgroundColor: 'white',
    borderRadius: 12,
    // borderWidth: 1,
    // borderColor: 'green',
  },
  expandAllText: {
    position: 'absolute',
    alignSelf: 'flex-end',
    marginTop: isTablet() ? '5%' : '4%',
    marginEnd: isTablet() ? '8%' : 0,
  },
  //Section card view
  cardViewContainer: {
    width: '100%',
    alignSelf: 'baseline',
    justifyContent: 'center',
    paddingVertical: isTablet() ? '10%' : '4%',
  },
  rightThumbnailCardView: {
    flexDirection: 'row',
    paddingVertical: isTablet() ? '4%' : '5%',
  },
  cardviewRightImage: {
    flex: 0.35,
    width: isTablet() ? 130 : 70,
    height: isTablet() ? 155 : 95,
    marginEnd: isTablet() ? '10%' : '7%',
    alignSelf: 'center',
  },
  rightThumbnailCardViewText: {
    flex: 0.65,
    marginEnd: isTablet() ? '3%' : '4%',
    marginStart: isTablet() ? '13%' : '7%',
    alignSelf: 'center',
  },
  leftThumbnailCardView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  cardviewLeftImage: {
    flex: 0.2,
    width: isTablet() ? 80 : 43,
    height: isTablet() ? 101 : 46,
    alignSelf: 'center',
    marginStart: isTablet() ? '13%' : '7%',
  },
  leftThumbnailCardViewText: {
    flex: 0.8,
    marginStart: '4%',
    marginEnd: isTablet() ? '9%' : '7%',
    alignSelf: 'center',
  },
  cardViewTitleContainer: {
    marginBottom: isTablet() ? '2%' : 0,
  },
  //Legal section
  legalTextContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginTop: '9%',
    marginBottom: '4%',
    marginEnd: '4%',
  },
  sectionLegalContainer: {
    position: 'absolute',
    bottom: 0,
    start: 0,
    end: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    // borderWidth: 1,
    marginTop: '1%',
    marginBottom: isTablet() ? '4%' : '2%',
    marginStart: '2%',
    marginEnd: '2%',
  },
  legalTextTabletLand: {
    position: 'absolute',
    bottom: '5%',
    right: '3%',
  },
});
