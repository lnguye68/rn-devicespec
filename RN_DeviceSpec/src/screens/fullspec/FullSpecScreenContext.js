import React, {createContext, useState, useEffect, useContext} from 'react';
import JsonModule from '@modules/JsonModule';
import {OrientationContext} from '@contexts/OrientationContext';

export const FullSpecScreenContext = createContext();

export const FullSpecScreenProvider = ({children}) => {
  const [fullSpecModel, setFullSpecModel] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const {orientation} = useContext(OrientationContext);

  async function _fetchFullSpecModelJson() {
    setIsLoading(true);
    try {
      if (orientation === 'PORTRAIT') {
        const jsonModel = await JsonModule.getJsonModelTextAsync(
          'model/fullspec/full_spec_test.json',
        );
        setFullSpecModel(JSON.parse(jsonModel));
      } else {
        const jsonModel = await JsonModule.getJsonModelTextAsync(
          'model/fullspec/full_spec_test_land.json',
        );
        setFullSpecModel(JSON.parse(jsonModel));
      }
    } catch (err) {
      console.log(err);
    }
    setIsLoading(false);
  }

  useEffect(() => {
    _fetchFullSpecModelJson();
  }, []);

  return (
    <FullSpecScreenContext.Provider value={{fullSpecModel, isLoading}}>
      {children}
    </FullSpecScreenContext.Provider>
  );
};
