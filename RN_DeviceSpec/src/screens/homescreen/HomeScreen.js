/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useRef, useEffect, useState} from 'react';
import {
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  BackHandler,
} from 'react-native';
import {OrientationContext} from '@contexts/OrientationContext';
import {StringsContext} from '@contexts/StringsContext';
import {ThemeContext} from '@contexts/ThemeContext';
import {HomeScreenProvider, HomeScreenContext} from './HomeScreenContext';
import CustomText from '@components/CustomText';
import PopularFeaturesPager from './PopularFeaturesPager';
import FastImage from 'react-native-fast-image';
import LoadingIndicator from '@components/LoadingIndicator';
import {isTablet} from 'react-native-device-info';
import {useNavigation, useIsFocused} from '@react-navigation/native';
import {SCREEN_NAMES} from '@utils/Constants';
import ShadowView from 'react-native-simple-shadow-view';
import AnalyticsModule from '@modules/AnalyticsModule';
import LegalScreen from '@screens/legal/LegalScreen';
import LinearGradient from 'react-native-linear-gradient';

const _renderSectionCards = () => {
  const {theme, DefaultTheme} = useContext(ThemeContext);
  const {getString} = useContext(StringsContext);
  const navigation = useNavigation();
  const {orientation} = useContext(OrientationContext);

  const _onViewAllSpecPress = () => {
    AnalyticsModule.sendScreenChangeEvent(
      AnalyticsModule.TAP,
      SCREEN_NAMES.HOME /* current screen name */,
      SCREEN_NAMES.FULL_SPEC /* next screen name */,
    );

    navigation.navigate(SCREEN_NAMES.FULL_SPEC, {
      prevScreen: SCREEN_NAMES.HOME,
    });
  };

  const _onCompareDevicesPress = () => {
    AnalyticsModule.sendScreenChangeEvent(
      AnalyticsModule.TAP,
      SCREEN_NAMES.HOME /* current screen name */,
      SCREEN_NAMES.CHOOSE_DEVICE /* next screen name */,
    );

    navigation.navigate(SCREEN_NAMES.CHOOSE_DEVICE, {
      prevScreen: SCREEN_NAMES.HOME,
    });
  };

  return (
    <View style={styles.sectionCards}>
      <View
        style={
          orientation === 'LANDSCAPE'
            ? styles.viewAllSpecContainerLand
            : styles.viewAllSpecContainer
        }>
        <ShadowView
          style={[
            styles.cardViewAllSpec,
            {backgroundColor: DefaultTheme[theme].cardColor},
          ]}>
          <>
            {orientation === 'LANDSCAPE' ? (
              <TouchableWithoutFeedback onPress={_onViewAllSpecPress}>
                <View style={styles.cardViewAllSpecContentLand}>
                  <View style={styles.cardViewAllSpecLeftColumnLand}>
                    <View style={styles.viewAllSpecsThumbnailLand} />
                  </View>
                  <View style={styles.cardViewAllSpecRightColumnLand}>
                    <View style={styles.cardTitleContainer}>
                      <CustomText
                        data={getString('screen_home_view_all_spec_title')}
                        fontColor={DefaultTheme[theme].fontColor}
                        fontSize={isTablet() ? 18 : null}
                      />
                      <FastImage
                        style={styles.chevronIcon}
                        source={{
                          uri:
                            theme === 'dark'
                              ? 'arrow_chevron_right_white'
                              : 'arrow_chevron_right_black',
                        }}
                        resizeMode={FastImage.resizeMode.contain}
                      />
                    </View>
                    <View>
                      <CustomText
                        data={getString('screen_home_view_all_spec_desc')}
                        fontColor={DefaultTheme[theme].fontColor}
                        fontSize={isTablet() ? 15 : null}
                      />
                    </View>
                  </View>
                </View>
              </TouchableWithoutFeedback>
            ) : (
              <TouchableWithoutFeedback onPress={_onViewAllSpecPress}>
                <View style={styles.cardViewAllSpecContent}>
                  <View style={styles.cardTitleContainer}>
                    <CustomText
                      data={getString('screen_home_view_all_spec_title')}
                      fontColor={DefaultTheme[theme].fontColor}
                      fontSize={isTablet() ? 18 : null}
                    />
                    <FastImage
                      style={styles.chevronIcon}
                      source={{
                        uri:
                          theme === 'dark'
                            ? 'arrow_chevron_right_white'
                            : 'arrow_chevron_right_black',
                      }}
                      resizeMode={FastImage.resizeMode.contain}
                    />
                  </View>
                </View>
              </TouchableWithoutFeedback>
            )}
          </>
        </ShadowView>
      </View>
      <View
        style={
          orientation === 'LANDSCAPE'
            ? styles.compareDevicesContainerLand
            : styles.compareDevicesContainer
        }>
        <ShadowView
          style={[
            isTablet() && orientation === 'LANDSCAPE'
              ? styles.cardCompareDevicesLandTablet
              : styles.cardCompareDevices,
            {backgroundColor: DefaultTheme[theme].cardColor},
          ]}>
          <TouchableWithoutFeedback onPress={_onCompareDevicesPress}>
            <View style={styles.cardCompareDevicesContent}>
              <View style={styles.compareDeviceLeftColumn}>
                <View style={styles.cardTitleContainer}>
                  <CustomText
                    data={getString('screen_home_compare_devices_title')}
                    fontColor={DefaultTheme[theme].fontColor}
                    fontSize={isTablet() ? 18 : null}
                  />
                  <FastImage
                    style={styles.chevronIcon}
                    source={{
                      uri:
                        theme === 'dark'
                          ? 'arrow_chevron_right_white'
                          : 'arrow_chevron_right_black',
                    }}
                    resizeMode={FastImage.resizeMode.contain}
                  />
                </View>
                <View>
                  <CustomText
                    data={getString('screen_home_compare_devices_desc')}
                    fontColor={DefaultTheme[theme].fontColor}
                    fontSize={isTablet() ? 15 : null}
                  />
                </View>
              </View>
              <View style={styles.compareDeviceRightColumn}>
                <FastImage
                  style={
                    isTablet() && orientation === 'LANDSCAPE'
                      ? styles.cardCompareDevicesThumbnailTabletLand
                      : styles.cardCompareDevicesThumbnail
                  }
                  source={{uri: 'compare_devices_image'}}
                  resizeMode={FastImage.resizeMode.contain}
                />
              </View>
            </View>
          </TouchableWithoutFeedback>
        </ShadowView>
      </View>
    </View>
  );
};

const _renderViewPager = () => {
  const context = useContext(HomeScreenContext);
  const {theme} = useContext(ThemeContext);

  useEffect(() => {
    context.fetchPopularFeatures();
  }, []);

  function _onPageSelected(index) {
    index += 1;
    AnalyticsModule.sendUserEvent(
      AnalyticsModule.SWIPE,
      'popular_features_carousel_page_' + index + '_selected',
    );
  }

  return (
    <>
      {!context.isLoading && (
        <PopularFeaturesPager
          style={styles.viewPager}
          dataList={context.popularFeatures}
          onPageSelected={_onPageSelected}
        />
      )}
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        colors={
          theme === 'light'
            ? ['#FFFFFF', '#FFFFFF00']
            : ['#282727', '#28272700']
        }
        style={styles.leftGradient}
      />
      <LinearGradient
        start={{x: 1, y: 0}}
        end={{x: 0, y: 0}}
        colors={
          theme === 'light'
            ? ['#FFFFFF', '#FFFFFF00']
            : ['#282727', '#28272700']
        }
        style={styles.rightGradient}
      />
    </>
  );
};

const _renderSectionFooter = ({onLegalClick}) => {
  const {theme, DefaultTheme, toggleTheme} = useContext(ThemeContext);
  const {getString} = useContext(StringsContext);
  const {orientation} = useContext(OrientationContext);

  return (
    <View style={styles.sectionFooter}>
      <TouchableWithoutFeedback onPress={toggleTheme}>
        <View
          style={
            orientation === 'LANDSCAPE'
              ? styles.themeTextContainerLand
              : styles.themeTextContainer
          }>
          <FastImage
            style={styles.themePlusIcon}
            source={{
              uri: theme === 'dark' ? 'white_plus_icon' : 'black_plus_icon',
            }}
            resizeMode={FastImage.resizeMode.contain}
          />
          <View style={styles.legalText}>
            <CustomText
              data={
                theme === 'light'
                  ? getString('screen_home_dark_mode')
                  : getString('screen_home_light_mode')
              }
              fontColor={DefaultTheme[theme].fontColor}
              fontSize={isTablet() ? 16 : null}
            />
          </View>
        </View>
      </TouchableWithoutFeedback>

      <TouchableWithoutFeedback onPress={onLegalClick}>
        <View
          style={
            orientation === 'LANDSCAPE'
              ? styles.legalTextContainerLand
              : styles.legalTextContainer
          }>
          <View style={styles.legalText}>
            <CustomText
              data={getString('screen_home_legal')}
              fontColor={DefaultTheme[theme].fontColor}
              fontSize={isTablet() ? 16 : null}
            />
          </View>
          <FastImage
            style={styles.legalPlusIcon}
            source={{
              uri: theme === 'dark' ? 'white_plus_icon' : 'black_plus_icon',
            }}
            resizeMode={FastImage.resizeMode.contain}
          />
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
};

const _handleXIconClick = () => {
  AnalyticsModule.sendScreenChangeEvent(
    AnalyticsModule.BACK_ICON_PRESS,
    SCREEN_NAMES.HOME /* current screen name */,
    SCREEN_NAMES.NATIVE /* next screen name */,
  );

  BackHandler.exitApp();
};

const _onLegalClick = legalRef => {
  legalRef.current?.open();
  AnalyticsModule.sendScreenChangeEvent(
    AnalyticsModule.TAP,
    SCREEN_NAMES.HOME /* current screen name */,
    SCREEN_NAMES.HOME + '_' + SCREEN_NAMES.LEGAL /* next screen name */,
  );
};

////////////////// PORTRAIT LAYOUT
const _renderPortrait = ({legalRef}) => {
  const {theme, DefaultTheme} = useContext(ThemeContext);
  const {getString} = useContext(StringsContext);

  return (
    <View
      style={[
        styles.rootContainer,
        {backgroundColor: DefaultTheme[theme].backgroundColor},
      ]}>
      {/* section page title */}
      <View style={styles.sectionTitle}>
        <View style={styles.titleContainer}>
          <CustomText
            data={getString('screen_home_title')}
            fontColor={DefaultTheme[theme].fontColor}
            fontSize={isTablet() ? 30 : null}
          />
        </View>
        <TouchableWithoutFeedback onPress={_handleXIconClick}>
          <FastImage
            style={styles.xIcon}
            source={{
              uri: theme === 'dark' ? 'white_x_icon' : 'black_x_icon',
            }}
            resizeMode={FastImage.resizeMode.contain}
          />
        </TouchableWithoutFeedback>
      </View>

      {/* section view pager */}
      <View style={styles.sectionPager}>
        <_renderViewPager />
      </View>

      {/* section cards */}
      <_renderSectionCards />

      {/* section footer */}
      <_renderSectionFooter
        onLegalClick={() => {
          _onLegalClick(legalRef);
        }}
      />
    </View>
  );
};

////////////////// LANDSCAPE LAYOUT
const _renderLandscape = ({legalRef}) => {
  const {theme, DefaultTheme} = useContext(ThemeContext);
  const {getString} = useContext(StringsContext);

  return (
    <View
      style={[
        styles.rootContainerLand,
        {backgroundColor: DefaultTheme[theme].backgroundColor},
      ]}>
      {/* LAYOUT HEADER */}
      <View style={styles.layoutHeaderLand}>
        <View style={styles.titleContainerLand}>
          <CustomText
            data={getString('screen_home_title')}
            fontColor={DefaultTheme[theme].fontColor}
            fontSize={isTablet() ? 30 : null}
          />
        </View>
        <TouchableWithoutFeedback onPress={_handleXIconClick}>
          <FastImage
            style={styles.xIconLand}
            source={{
              uri: theme === 'dark' ? 'white_x_icon' : 'black_x_icon',
            }}
            resizeMode={FastImage.resizeMode.contain}
          />
        </TouchableWithoutFeedback>
      </View>

      {/* LAYOUT MAIN */}
      <View style={styles.layoutMainLand}>
        {/* LAYOUT MAIN LEFT */}
        <View style={styles.layoutLeftLand}>
          {/* section view pager */}
          <View style={styles.sectionPagerLand}>
            <_renderViewPager />
          </View>
        </View>

        {/* LAYOUT MAIN RIGHT */}
        <View style={styles.layoutRightLand}>
          {/* section cards */}
          <_renderSectionCards />
        </View>
      </View>

      {/* LAYOUT FOOTER */}
      <View style={styles.layoutFooterLand}>
        <_renderSectionFooter
          onLegalClick={() => {
            _onLegalClick(legalRef);
          }}
        />
      </View>
    </View>
  );
};

const HomeScreen = () => {
  const {orientation} = useContext(OrientationContext);
  const isFocused = useIsFocused();
  const legalRef = useRef(null);

  const _backAction = () => {
    AnalyticsModule.sendScreenChangeEvent(
      AnalyticsModule.BACK_KEY_PRESS,
      SCREEN_NAMES.HOME,
      SCREEN_NAMES.NATIVE,
    );
    BackHandler.exitApp();

    return true;
  };

  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      _backAction,
    );
    return () => backHandler.remove();
  }, []);

  if (isFocused) {
    if (orientation === 'PORTRAIT') {
      return (
        <>
          <HomeScreenProvider>
            <_renderPortrait legalRef={legalRef} />
          </HomeScreenProvider>
          <LegalScreen prevScreen={SCREEN_NAMES.HOME} ref={legalRef} />
        </>
      );
    } else {
      return (
        <>
          <HomeScreenProvider>
            <_renderLandscape legalRef={legalRef} />
          </HomeScreenProvider>
          <LegalScreen prevScreen={SCREEN_NAMES.HOME} ref={legalRef} />
        </>
      );
    }
  } else {
    return <></>;
  }
};

const styles = StyleSheet.create({
  // DEFAULT/PORTRAIT STYLES
  rootContainer: {
    flex: 1,
    backgroundColor: 'white',
  },
  sectionTitle: {
    flex: isTablet() ? 0.8 : 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: isTablet() ? '2%' : '0%',
  },
  sectionPager: {
    flex: isTablet() ? 9 : 8.5,
  },
  sectionCards: {
    flex: isTablet() ? 4 : 4.5,
  },
  sectionFooter: {
    flex: 1.5,
    flexDirection: 'row',
    alignItems: 'center',
  },
  titleContainer: {
    marginStart: '7.5%',
  },
  xIcon: {
    width: isTablet() ? 20 : 17,
    height: isTablet() ? 22 : 19,
    position: 'absolute',
    end: '8%',
  },
  viewPager: {
    flex: 1,
  },
  viewAllSpecContainer: {
    flex: 0.8,
    justifyContent: 'center',
  },
  compareDevicesContainer: {
    flex: 1,
    alignItems: 'center',
  },
  cardViewAllSpec: {
    width: '88%',
    alignSelf: 'baseline',
    marginEnd: '6%',
    marginStart: '6%',
    justifyContent: 'center',
    shadowColor: '#4D000000',
    shadowOpacity: 1,
    shadowRadius: 6,
    shadowOffset: {width: 0, height: 0},
    borderRadius: 12,
  },
  cardViewAllSpecContent: {
    paddingStart: '8%',
    paddingTop: isTablet() ? '2%' : '3%',
    paddingBottom: isTablet() ? '1%' : '3%',
  },
  cardCompareDevices: {
    width: '88%',
    paddingVertical: '7%',
    alignSelf: 'baseline',
    marginEnd: '6%',
    marginStart: '6%',
    justifyContent: 'center',
    shadowColor: '#4D000000',
    shadowOpacity: 1,
    shadowRadius: 6,
    shadowOffset: {width: 0, height: 0},
    borderRadius: 12,
  },
  compareDeviceLeftColumn: {
    flex: 0.65,
    paddingStart: '8%',
    paddingEnd: '2%',
  },
  compareDeviceRightColumn: {
    flex: 0.35,
    paddingEnd: '3%',
  },
  cardCompareDevicesContent: {
    flexDirection: 'row',
  },
  cardCompareDevicesThumbnail: {
    width: 85,
    height: 90,
    alignSelf: 'center',
  },
  cardBtn: {
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  cardTitleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: '2%',
  },
  chevronIcon: {
    marginStart: '2%',
    width: isTablet() ? 15 : 11,
    height: isTablet() ? 15 : 11,
  },
  themeTextContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginStart: '4%',
    marginTop: '3%',
  },
  legalTextContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginEnd: '4%',
    marginTop: '3%',
  },
  themePlusIcon: {
    width: 12,
    height: 12,
    marginEnd: '3%',
  },
  legalPlusIcon: {
    width: 12,
    height: 12,
    marginStart: '3%',
  },
  leftGradient: {position: 'absolute', width: '7%', height: '100%'},
  rightGradient: {position: 'absolute', end: 0, width: '7%', height: '100%'},

  // LAND STYLES
  rootContainerLand: {
    flex: 1,
  },
  layoutHeaderLand: {
    flex: isTablet() ? 0.8 : 1,
    justifyContent: 'center',
    marginTop: isTablet() ? '2%' : '0%',
  },
  layoutMainLand: {
    flex: 7.5,
    flexDirection: 'row',
  },
  layoutLeftLand: {
    flex: 1,
    height: '100%',
  },
  layoutRightLand: {
    flex: 1,
    paddingBottom: '0%',
    marginEnd: isTablet() ? '1.3%' : '0%',
  },
  sectionPagerLand: {
    flex: 1,
    marginStart: isTablet() ? '8%' : '6%',
  },
  layoutFooterLand: {
    flex: 1.5,
  },
  themeTextContainerLand: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginStart: '4%',
  },
  legalTextContainerLand: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginEnd: '4%',
  },
  titleContainerLand: {
    marginStart: isTablet() ? '4%' : '3%',
  },
  xIconLand: {
    width: isTablet() ? 20 : 17,
    height: isTablet() ? 22 : 19,
    position: 'absolute',
    end: isTablet() ? '4.5%' : '3.5%',
  },
  viewAllSpecContainerLand: {
    flex: 0.4,
    justifyContent: 'center',
    marginTop: '3%',
    marginBottom: isTablet ? '6%' : '3%',
  },
  compareDevicesContainerLand: {
    flex: 0.6,
    alignItems: 'center',
  },
  cardViewAllSpecContentLand: {
    flexDirection: 'row',
    paddingStart: '8%',
    paddingVertical: isTablet() ? '12%' : '4%',
  },
  viewAllSpecsThumbnailLand: {
    width: 45,
    height: isTablet() ? 90 : 60,
    backgroundColor: 'blue',
  },
  cardViewAllSpecLeftColumnLand: {
    flex: 0.2,
    justifyContent: 'center',
  },
  cardViewAllSpecRightColumnLand: {
    flex: 0.7,
  },
  cardCompareDevicesLandTablet: {
    width: '88%',
    paddingVertical: '12%',
    alignSelf: 'baseline',
    marginEnd: '6%',
    marginStart: '6%',
    justifyContent: 'center',
    shadowColor: '#4D000000',
    shadowOpacity: 1,
    shadowRadius: 6,
    shadowOffset: {width: 0, height: 0},
    borderRadius: 12,
  },
  cardCompareDevicesThumbnailTabletLand: {
    width: 120,
    height: 125,
    alignSelf: 'center',
  },
});

export default HomeScreen;
