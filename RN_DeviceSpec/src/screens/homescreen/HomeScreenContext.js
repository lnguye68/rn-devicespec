import React, {createContext, useState, useEffect} from 'react';
import FileUtil from '@utils/FileUtil';
import RNFS from 'react-native-fs';

export const HomeScreenContext = createContext();

export const HomeScreenProvider = ({children}) => {
  const [popularFeatures, setPopularFeatures] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const _getPopularFeatureItem = (title, item) => {
    const image = `${RNFS.ExternalDirectoryPath}/${item.src}/img.png`;
    const subtitle =
      item.additionalProperties.devicePayload.selections.subtitle['en-us'];
    const desc =
      item.additionalProperties.devicePayload.selections.content['en-us'];

    return {
      image: image,
      title: title,
      subtitle: subtitle,
      description: desc,
    };
  };

  async function fetchPopularFeatures() {
    setIsLoading(true);
    try {
      const data = await FileUtil.readExternalFile('spec.json');
      const parsedData = JSON.parse(data);
      const convertedFeatureList = [];

      parsedData.features.map(item => {
        if (item.featureId === 'phoneCamera') {
          convertedFeatureList.push(_getPopularFeatureItem('Camera', item));
        } else if (item.featureId === 'phoneDisplay') {
          convertedFeatureList.push(_getPopularFeatureItem('Display', item));
        } else if (item.featureId === 'phoneBattery') {
          convertedFeatureList.push(_getPopularFeatureItem('Battery', item));
        } else if (item.featureId === 'phoneMemoryStorage') {
          convertedFeatureList.push(_getPopularFeatureItem('Storage', item));
        }
      });

      setPopularFeatures(convertedFeatureList);
    } catch (e) {
      console.log(e);
    }
    setTimeout(() => {
      setIsLoading(false);
    }, 100);
    // setIsLoading(false);
  }

  return (
    <HomeScreenContext.Provider
      value={{popularFeatures, isLoading, fetchPopularFeatures}}>
      {children}
    </HomeScreenContext.Provider>
  );
};
