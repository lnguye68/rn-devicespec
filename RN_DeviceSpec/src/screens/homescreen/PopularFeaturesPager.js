import React from 'react';
import {requireNativeComponent} from 'react-native';
import PropTypes from 'prop-types';

class PopularFeaturesPager extends React.Component {
  constructor(props) {
    super(props);
    this._onPageSelected = this._onPageSelected.bind(this);
  }
  _onPageSelected(event) {
    if (!this.props.onPageSelected) {
      return;
    }
    const position = event.nativeEvent.position;
    this.props.onPageSelected(position);
  }
  render() {
    return (
      <NativePopularFeaturesPager
        {...this.props}
        dataList={this.props.dataList}
        initialPage={this.props.initialPage}
        onPageSelected={this._onPageSelected}
      />
    );
  }
}

PopularFeaturesPager.propTypes = {
  initialPage: PropTypes.number,
  onPageSelected: PropTypes.func,
  dataList: PropTypes.array,
};

var NativePopularFeaturesPager = requireNativeComponent(
  'PopularFeaturesPager',
  PopularFeaturesPager,
);

export default PopularFeaturesPager;
