import React, {
  useState,
  useEffect,
  useRef,
  useContext,
  forwardRef,
} from 'react';
import {
  StyleSheet,
  View,
  TouchableWithoutFeedback,
  BackHandler,
} from 'react-native';

import {OrientationContext} from '@contexts/OrientationContext';
import {ThemeContext} from '@contexts/ThemeContext';
import {StringsContext} from '@contexts/StringsContext';
import {isTablet} from 'react-native-device-info';
import {useCombinedRefs} from '@utils/CombinedRefs';
import AnalyticsModule from '@modules/AnalyticsModule';
import {SCREEN_NAMES} from '@utils/Constants';

import {Modalize} from 'react-native-modalize';
import CustomText from '@components/CustomText';
import FastImage from 'react-native-fast-image';

const LegalScreen = forwardRef((props, ref) => {
  const TAG = 'LegalScreen';
  const {theme, DefaultTheme} = useContext(ThemeContext);
  const {getString} = useContext(StringsContext);
  const {orientation} = useContext(OrientationContext);
  const modalizeRef = useRef(null);
  const combinedRef = useCombinedRefs(ref, modalizeRef);

  // ************ Start portrait content ************
  const _renderHeader = () => (
    <View
      style={[
        styles.paddingTitle,
        {backgroundColor: DefaultTheme[theme].backgroundColor},
      ]}>
      <CustomText
        data={getString('screen_legal_popup_title')}
        fontColor={DefaultTheme[theme].fontColor}
        fontSize={_getLegalTitleSize()}
      />
      <TouchableWithoutFeedback
        onPress={() => {
          modalizeRef.current?.close();
        }}>
        <FastImage
          style={styles.xIcon}
          source={{
            uri: theme === 'dark' ? 'white_x_icon' : 'black_x_icon',
          }}
          resizeMode={FastImage.resizeMode.contain}
        />
      </TouchableWithoutFeedback>
    </View>
  );

  const _renderContent = () => (
    <View
      style={[
        styles.paddingText,
        {backgroundColor: DefaultTheme[theme].backgroundColor},
      ]}>
      <CustomText
        data={getString('screen_legal_popup_subtitle')}
        fontColor={DefaultTheme[theme].fontColor}
        fontSize={_getLegalTextSize()}
      />
    </View>
  );
  // ************ End portrait content ************

  // ************ Start landscape content ************
  const _renderHeaderLand = () => (
    <View
      style={[
        styles.paddingTitleLand,
        {backgroundColor: DefaultTheme[theme].backgroundColor},
      ]}>
      <CustomText
        data={getString('screen_legal_popup_title')}
        fontColor={DefaultTheme[theme].fontColor}
        fontSize={_getLegalTitleSize()}
      />
      <TouchableWithoutFeedback onPress={() => modalizeRef.current?.close()}>
        <FastImage
          style={styles.xIconLand}
          source={{
            uri: theme === 'dark' ? 'white_x_icon' : 'black_x_icon',
          }}
          resizeMode={FastImage.resizeMode.contain}
        />
      </TouchableWithoutFeedback>
    </View>
  );

  const _renderContentLand = () => (
    <View
      style={[
        styles.paddingTextLand,
        {backgroundColor: DefaultTheme[theme].backgroundColor},
      ]}>
      <CustomText
        data={getString('screen_legal_popup_subtitle')}
        fontColor={DefaultTheme[theme].fontColor}
        fontSize={_getLegalTextSize()}
      />
    </View>
  );
  // ************ End landscape content ************

  // ********** Start font size methods **********
  const _getLegalTitleSize = () => {
    if (isTablet()) {
      return 21;
    } else {
      return null;
    }
  };

  const _getLegalTextSize = () => {
    if (isTablet()) {
      return 15;
    } else {
      return null;
    }
  };
  // ********** End font size methods **********

  // ************ Start render ************
  if (orientation === 'PORTRAIT') {
    return (
      <Modalize
        onClosed={() => {
          AnalyticsModule.sendScreenChangeEvent(
            AnalyticsModule.TAP,
            props.prevScreen +
              '_' +
              SCREEN_NAMES.LEGAL /* current screen name */,
            props.prevScreen /* next screen name */,
          );
        }}
        ref={combinedRef}
        modalStyle={[
          styles.modal,
          {backgroundColor: DefaultTheme[theme].backgroundColor},
        ]}
        rootStyle={{backgroundColor: 'rgba(50,50,50,0.8)'}}
        modalTopOffset={isTablet() ? -10 : (20, 0, -60)}
        withHandle={false}
        HeaderComponent={_renderHeader()}>
        {_renderContent()}
      </Modalize>
    );
  } else {
    return (
      <Modalize
        onClosed={() => {
          AnalyticsModule.sendScreenChangeEvent(
            AnalyticsModule.TAP,
            props.prevScreen +
              '_' +
              SCREEN_NAMES.LEGAL /* current screen name */,
            props.prevScreen /* next screen name */,
          );
        }}
        ref={combinedRef}
        modalStyle={[
          styles.modalLand,
          {backgroundColor: DefaultTheme[theme].backgroundColor},
        ]}
        rootStyle={{backgroundColor: 'rgba(50,50,50,0.8)'}}
        modalTopOffset={isTablet() ? -10 : 15}
        withHandle={false}
        HeaderComponent={_renderHeaderLand()}>
        {_renderContentLand()}
      </Modalize>
    );
  }
});
// ************ End render ************

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  modal: {
    width: isTablet() ? '88%' : '85%',
    alignSelf: 'center',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    paddingBottom: '5%',
    marginBottom: '5%',
  },
  paddingTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    paddingTop: isTablet() ? '7%' : '8%',
    paddingBottom: '5%',
    paddingLeft: isTablet() ? '8%' : '10%',
    paddingRight: isTablet() ? '8%' : '10%',
  },
  paddingText: {
    alignSelf: 'center',
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    paddingLeft: isTablet() ? '8%' : '10%',
    paddingRight: isTablet() ? '8%' : '10%',
  },
  xIcon: {
    width: isTablet() ? 18 : 15,
    height: isTablet() ? 20 : 17,
  },

  //////////////////// LANDSCAPE STYLES ////////////////////

  modalLand: {
    width: isTablet() ? '90%' : '95%',
    alignSelf: 'center',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    paddingBottom: '5%',
    marginBottom: '3%',
  },
  paddingTitleLand: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    paddingTop: isTablet() ? '4%' : '3%',
    paddingBottom: '3%',
    paddingLeft: isTablet() ? '5%' : '3%',
    paddingRight: isTablet() ? '5%' : '3%',
  },
  paddingTextLand: {
    flexGrow: 1,
    alignSelf: 'center',
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    paddingBottom: '5%',
    paddingLeft: isTablet() ? '5%' : '3%',
    paddingRight: isTablet() ? '5%' : '3%',
  },
  xIconLand: {
    width: isTablet() ? 18 : 15,
    height: isTablet() ? 20 : 17,
  },
});

export default LegalScreen;
