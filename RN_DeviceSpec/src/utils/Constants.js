export const STRING_FILE_NAME = 'strings.json';
export const STRING_FILE_NAME_LAND = 'land/strings.json';
export const MSG_LOADING_STRINGS = 'Loading strings...';

// screen names for analytics/react navigation
export const SCREEN_NAMES = {
  HOME: 'NATIVE_Popular Specifications',
  CHOOSE_DEVICE: 'NATIVE_Popular Specifications_Choose a Device to Compare',
  DEVICE_COMPARISON: 'NATIVE_Popular Specifications_Choose a Device to Compare_Device Comparison',
  FULL_SPEC: 'NATIVE_Popular Specifications_All Specifications',
  LEGAL: 'Legal',
  NATIVE: 'NATIVE',
};

export const CHAPTER_INTERACTION = {
  Super: 'Super',
  Disclaimer: 'Disclaimer',
  Custom: 'Custom',
  Tap: 'Tap',
  Swipe_Left: 'Swipe_Left',
  Swipe_Up: 'Swipe_Up',
  Swipe_Right: 'Swipe_Right',
  Swipe_Down: 'Swipe_Down',
  Swipe_Left_Right: 'Swipe_Left_Right',
  Swipe_Up_Down: 'Swipe_Up_Down',
  Unknown: 'Unknown',
};

export const CHAPTER_ACTION = {
  Custom: 'Custom',
  Unknown: 'Unknown',
};
