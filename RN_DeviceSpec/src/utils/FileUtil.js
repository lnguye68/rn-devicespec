/**
 * FileUtil v1.0.0
 *
 * OLD STRUCTURE
 * 1. Language-Country/video/orientation/subsidiary/channel/demo.mp4
 * 2. Language-Country/video/orientation/subsidiary/demo.mp4
 * 3. Language/video/orientation/subsidiary/channel/demo.mp4
 * 4. Language/video/orientation/subsidiary/demo.mp4
 * 5. /video/orientation/subsidiary/channel/demo.mp4
 * 6. /video/orientation/subsidiary/demo.mp4
 * 7. Language-Country/video/orientation/demo.mp4
 * 8. Language/video/orientation/demo.mp4
 * 9. /video/orientation/demo.mp4
 * 10. Language-Country/video/subsidiary/channel/demo.mp4
 * 11. Language-Country/video/subsidiary/demo.mp4
 * 12. Language/video/subsidiary/channel/demo.mp4
 * 13. Language/video/subsidiary/demo.mp4
 * 14. /video/subsidiary/channel/mp4
 * 15. /video/subsidiary/mp4
 * 16. Language-Country/video/demo.mp4
 * 17. Language/video/demo.mp4
 * 18. /video/demo.mp4
 */

/**
 * NEW STRUCTURE
 *
 * language-country => en-rUS, es-rEs, etc.
 * language => en, ko, etc.
 * channel => att, best buy, etc.
 * subsidiary => sea, seau, etc.
 *
 *  1. subsidiary/channel/language-country/land/<file_name>
 *  2. subsidiary/channel/language-country/<file_name>
 *  3. subsidiary/channel/language/land/<file_name>
 *  4. subsidiary/channel/language/<file_name>
 *  5. subsidiary/channel/land/<file_name>
 *  6. subsidiary/channel/<file_name>
 *  7. subsidiary/language-country/land/<file_name>
 *  8. subsidiary/language-country/<file_name>
 *  9. subsidiary/language/land/<file_name>
 * 10. subsidiary/language/<file_name>
 * 11. subsidiary/land/<file_name>
 * 12. subsidiary/<file_name>
 * 13. language-country/land/<file_name>
 * 14. language-country/<file_name>
 * 15. language/land/<file_name>
 * 16. language/<file_name>
 * 17. land/<file_name>
 * 18. <file_name>
 */

import RNFS from 'react-native-fs';
import AnalyticsModule from '@modules/AnalyticsModule';
import ScreenModule from '@modules/ScreenModule';
import LocalizeModule from '@modules/LocalizeModule';

/**
 * Read the default file  (without locale, usually in the root folder
 * or root/land/). If the file is not found in
 * the external folder, it will try to read the file inside the
 * native assets folder, if that file is still not found, it will
 * return an empty string.
 *
 * @param filePath
 * @returns {Promise<any>}
 */
const readFile = async filePath => {
  const externalFilePath = _getFullExternalFilePath(filePath);
  const isExist = await RNFS.exists(externalFilePath);
  return isExist
    ? await RNFS.readFile(externalFilePath)
    : (await RNFS.existsAssets(filePath))
    ? await RNFS.readFileAssets(filePath)
    : '';
};

/**
 * Read the default external file  (without locale, usually in the root folder
 * or root/land/). If the file is not found in
 * the external folder, it will try to read the file inside the
 * native assets folder, if that file is still not found, it will
 * return an empty string.
 *
 * @param filePath
 * @returns {Promise<string>}
 */
const readExternalFile = async filePath => {
  const externalFilePath = _getFullExternalFilePath(filePath);
  const isExist = await RNFS.exists(externalFilePath);
  return isExist ? await RNFS.readFile(externalFilePath) : '';
};

/**
 * Read the default assets file  (without locale, usually in the root assets folder
 * or root/land/). If the file is not found in
 * the external folder, it will try to read the file inside the
 * native assets folder, if that file is still not found, it will
 * return an empty string.
 *
 * @param filePath
 * @returns {Promise<string>}
 */
const readAssetsFile = async filePath => {
  const isExist = await RNFS.existsAssets(filePath);
  return isExist ? await RNFS.readFileAssets(filePath) : '';
};

/**
 * Read the file with locale. If the file is not found in
 * the external folder, it will try to read the file inside the
 * native assets folder, if that file is still not found, it will
 * return an empty string.
 *
 * @param filePath
 * @param ignoreOrientation
 * @returns {Promise<any>}
 */
const readFileWithLocale = async (filePath, ignoreOrientation = false) => {
  const externalContent = await readExternalFileWithLocale(filePath, ignoreOrientation);
  if (externalContent) {
    return externalContent;
  } else {
    const assetContent = await readAssetsFileWithLocale(filePath, ignoreOrientation);
    return assetContent ? assetContent : '';
  }
};

/**
 *
 * Read the external file with locale. If the file is not found in
 * the external folder,  it will return an empty string.
 *
 * @param filePath
 * @param ignoreOrientation
 * @returns {Promise<any>}
 */
const readExternalFileWithLocale = async (filePath, ignoreOrientation = false) => {
  const externalFilePath = await getExternalFilePathWithLocale(filePath);
  let modifiedPath = externalFilePath;
  if (ignoreOrientation) {
    modifiedPath = externalFilePath.replace('/land', '');
  }
  const isExist = await RNFS.exists(modifiedPath);
  return isExist ? await RNFS.readFile(modifiedPath) : '';
};

/**
 *
 * Read the asset file with locale. If the file is not found in
 * the asset folder,  it will return an empty string.
 *
 * @param filePath
 * @param ignoreOrientation
 * @returns {Promise<any>}
 */
const readAssetsFileWithLocale = async (filePath, ignoreOrientation = false) => {
  const assetFilePath = await getAssetsFilePathWithLocale(filePath);
  let modifiedPath = assetFilePath;
  if (ignoreOrientation) {
    modifiedPath = assetFilePath.replace('/land', '');
  }
  const isExist = await RNFS.existsAssets(modifiedPath);
  return isExist ? await RNFS.readFileAssets(modifiedPath) : '';
};

/**
 * Return the default file path (without locale, usually
 * in the root folder or root/land/). If the file is not
 * found in the external folder, it will look into the
 * native assets folder, if the file is still not found,
 * it will return and empty string.
 *
 * @param fileName
 * @returns {Promise<string|string>}
 */
const getFilePath = async fileName => {
  const externalFilePath = await getExternalFilePath(fileName);
  if (externalFilePath) {
    return externalFilePath;
  } else {
    const assetFilePath = await getAssetsFilePath(fileName);
    return assetFilePath ? assetFilePath : '';
  }
};

/**
 * Return the default external file path (without locale, usually
 * in the root folder or root/land/). If the file is not
 * found in the external folder, it will return an empty string.
 *
 * @param fileName
 * @returns {Promise<string|string>}
 */
const getExternalFilePath = async fileName => {
  const filePathList = await _createFilePaths(fileName);
  return await _traverseExternalFilePaths(filePathList);
};

/**
 * Return the default asset file path (without locale, usually
 * in the assets root folder or root/land/). If the file is not
 * found in the external folder, it will return an empty string.
 *
 * @param fileName
 * @returns {Promise<string>}
 */
const getAssetsFilePath = async fileName => {
  const filePathList = await _createFilePaths(fileName);
  return await _traverseAssetsFilePaths(filePathList);
};

/**
 * Get file path with locale. This method will look into external storage
 * first. If the file in external storage is not found, it will look into
 * the native assets folder. If that file is still not found, it will return
 * an empty string.
 *
 * @param fileName
 * @returns {Promise<string>}
 */
const getFilePathWithLocale = async fileName => {
  const filePathList = await _createFilePathsWithLocale(fileName);
  const externalFilePath = await _traverseExternalFilePaths(filePathList);
  return externalFilePath != null && externalFilePath !== ''
    ? externalFilePath
    : await _traverseAssetsFilePaths(filePathList);
};

/**
 * Get external file path with locale. If the file in the external folder
 * is not found, it will return an empty string.
 *
 * @param fileName
 * @returns {Promise<void>}
 */
const getExternalFilePathWithLocale = async fileName => {
  const filePathList = await _createFilePathsWithLocale(fileName);
  return await _traverseExternalFilePaths(filePathList);
};

/**
 * Get asset file path with locale. If the file in the native assets folder
 * is not found, it will return an empty string.
 *
 * @param fileName
 * @returns {Promise<void>}
 */
const getAssetsFilePathWithLocale = async fileName => {
  const filePathList = await _createFilePathsWithLocale(fileName);
  return await _traverseAssetsFilePaths(filePathList);
};

const _traverseExternalFilePaths = async filePathList => {
  for (let i = 0; i < filePathList.length; i++) {
    const externalFilePath = _getFullExternalFilePath(filePathList[i]);
    const isExist = await RNFS.exists(externalFilePath);
    if (isExist) {
      return externalFilePath;
    }
  }
  return '';
};

const _traverseAssetsFilePaths = async filePathList => {
  for (let i = 0; i < filePathList.length; i++) {
    const assetFilePath = filePathList[i];
    const isExist = await RNFS.existsAssets(assetFilePath);
    if (isExist) {
      return assetFilePath;
    }
  }
  return '';
};

const _createFilePaths = async fileName => {
  const orientation = await _getOrientation();

  return [/* .1 */ `${orientation}/${fileName}`, /* .2 */ `${fileName}`];
};

const _createFilePathsWithLocale = async fileName => {
  const subsidiary = await _getSubsidiary();
  const channel = await _getChannel();
  const orientation = await _getOrientation();
  const languageCountry = await _getLanguageCountry();
  const language = await _getLanguage();

  return [
    /*  .1 */ `${subsidiary}/${channel}/${languageCountry}/${orientation}/${fileName}`,
    /*  .2 */ `${subsidiary}/${channel}/${languageCountry}/${fileName}`,
    /*  .3 */ `${subsidiary}/${channel}/${language}/${orientation}/${fileName}`,
    /*  .4 */ `${subsidiary}/${channel}/${language}/${fileName}`,
    /*  .5 */ `${subsidiary}/${channel}/${orientation}/${fileName}`,
    /*  .6 */ `${subsidiary}/${channel}/${fileName}`,
    /*  .7 */ `${subsidiary}/${languageCountry}/${orientation}/${fileName}`,
    /*  .8 */ `${subsidiary}/${languageCountry}/${fileName}`,
    /*  .9 */ `${subsidiary}/${language}/${orientation}/${fileName}`,
    /* .10 */ `${subsidiary}/${language}/${fileName}`,
    /* .11 */ `${subsidiary}/${orientation}/${fileName}`,
    /* .12 */ `${subsidiary}/${fileName}`,
    /* .13 */ `${languageCountry}/${orientation}/${fileName}`,
    /* .14 */ `${languageCountry}/${fileName}`,
    /* .15 */ `${language}/${orientation}/${fileName}`,
    /* .16 */ `${language}/${fileName}`,
    /* .17 */ `${orientation}/${fileName}`,
    /* .18 */ `${fileName}`,
  ];
};

const _getFullExternalFilePath = filePath =>
  `${RNFS.ExternalDirectoryPath}/${filePath}`;

const _getSubsidiary = async () => {
  try {
    const subsidiary = await AnalyticsModule.getSubsidiary();
    return subsidiary ? subsidiary : 'default';
  } catch (e) {
    console.log(e);
    return 'default';
  }
};

const _getChannel = async () => {
  try {
    const channel = await AnalyticsModule.getChannel();
    return channel ? channel : 'default';
  } catch (e) {
    console.log(e);
    return 'default';
  }
};

const _getOrientation = async () => {
  try {
    return await ScreenModule.getOrientation();
  } catch (e) {
    console.log(e);
    return 'default';
  }
};

const _getLanguageCountry = async () => {
  try {
    const languageCountry = await LocalizeModule.getLanguageCountry();
    return languageCountry ? languageCountry : 'default';
  } catch (e) {
    console.log(e);
    return 'default';
  }
};

const _getLanguage = async () => {
  try {
    const language = await LocalizeModule.getLanguage();
    return language ? language : 'default';
  } catch (e) {
    console.log(e);
    return 'default';
  }
};

export default {
  readFile,
  readExternalFile,
  readAssetsFile,
  readFileWithLocale,
  readExternalFileWithLocale,
  readAssetsFileWithLocale,
  getFilePath,
  getExternalFilePath,
  getAssetsFilePath,
  getFilePathWithLocale,
  getExternalFilePathWithLocale,
  getAssetsFilePathWithLocale,
};
